-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-02-2018 a las 18:08:50
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `crm_gestar`
--
CREATE DATABASE IF NOT EXISTS `crm_gestar` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `crm_gestar`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id_activity` int(11) NOT NULL,
  `name_activity` varchar(255) NOT NULL,
  `description_activity` text NOT NULL,
  `date_activity` date NOT NULL,
  `hour_activity` time NOT NULL,
  `address_activity` text NOT NULL,
  `type_activity` varchar(255) NOT NULL,
  `image_activity` varchar(255) NOT NULL,
  `age_activity` varchar(255) NOT NULL,
  `genre_activity` int(11) NOT NULL,
  `category_activity` int(11) NOT NULL,
  `commune_activity` int(11) NOT NULL,
  `status_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activities_affiliates`
--

CREATE TABLE IF NOT EXISTS `activities_affiliates` (
  `id_activity_affiliate` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrators`
--

CREATE TABLE IF NOT EXISTS `administrators` (
  `id_admin` int(11) NOT NULL,
  `name_admin` varchar(255) NOT NULL,
  `phone_admin` varchar(255) NOT NULL,
  `direction_admin` text NOT NULL,
  `email_admin` varchar(255) NOT NULL,
  `sex_admin` varchar(200) NOT NULL,
  `check_user_admin` varchar(255) NOT NULL,
  `date_register_admin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrators`
--

INSERT INTO `administrators` (`id_admin`, `name_admin`, `phone_admin`, `direction_admin`, `email_admin`, `sex_admin`, `check_user_admin`, `date_register_admin`) VALUES
(1, 'Julian Bagilet', '+543415198035', 'El rosario Argentina', 'sales@jbosolutions.com', 'Masculino', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow==', '2018-02-25 16:16:14'),
(2, 'Jhon Perez', '+573055633212', 'Bogota Colombia', 'jhon.perez@gmail.com', 'Masculino', 'dFi0Afa8xGBsRtp3nDCqjYm2A9uP7rglC9CDhqhruTTATrmKS7kEKaUdv5cXnHcs6XPQGgpYPiFWEMgaCvh3Yg==', '2018-02-25 16:16:14'),
(3, 'Maritza Perex', '+584248559520', 'caracas venezuela', 'mari.perex@outlook.com', 'Femenino', '7+r+SPuX6n7qpljGML1CRx70o9teDzA7xkdpJ97DodoAu/ARPCHND6S8RuF2+74PcIjlaSYps2cJ0V/AfObMEQ==', '2018-02-25 16:16:14'),
(4, 'tyrone', '+5834348834', 'la pica', 'tyrone@gnail.com', 'Masculino', 'Op/fNcLLtNutCU5aKP4JrX6+gW7huomfyCarYh/kyR1PX1/8y5crRDVm81/lBaXMQyuKGaapHBMlSnv8JNU/Cg==', '2018-02-25 16:16:14'),
(5, 'Henry', '878787', 'hojkj', 'hgmanriquezm@gmail.com', 'Femenino', 'ewl9kahzYZm90UBwaM0HTvh9fFrQrkkJgTqG7+Ck9yAX3MI6YFTniA/K2ZTJ9gLaJDYnBcJSBVitqdFDS/87cA==', '2018-02-25 16:16:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `affiliates`
--

CREATE TABLE IF NOT EXISTS `affiliates` (
  `id_affiliate` int(11) NOT NULL,
  `names_affiliate` varchar(255) NOT NULL,
  `last_names_affiliate` varchar(255) NOT NULL,
  `dni_affiliate` bigint(20) NOT NULL,
  `street_affiliate` varchar(255) NOT NULL,
  `number_affiliate` int(11) NOT NULL,
  `dtto_affiliate` varchar(255) NOT NULL,
  `postal_code_affiliate` int(11) NOT NULL,
  `job_affiliate` varchar(255) NOT NULL,
  `studies_affiliate` int(11) NOT NULL,
  `denomination_affiliate` varchar(255) NOT NULL,
  `circuit_affiliate` int(11) NOT NULL,
  `commune_affiliate` int(11) NOT NULL,
  `gener_affiliate` varchar(255) NOT NULL,
  `age_affiliate` int(11) NOT NULL,
  `facebook_affiliate` varchar(255) NOT NULL,
  `twitter_affiliate` varchar(255) NOT NULL,
  `instagram_affiliate` varchar(255) NOT NULL,
  `email_affiliate` varchar(255) NOT NULL,
  `phone_1_affiliate` bigint(20) NOT NULL,
  `phone_2_affiliate` bigint(20) NOT NULL,
  `checked_phone` int(11) NOT NULL DEFAULT '0',
  `mobile_1_affiliate` bigint(20) NOT NULL,
  `mobile_2_affiliate` bigint(20) NOT NULL,
  `mobile_3_affiliate` bigint(20) NOT NULL,
  `mobile_4_affiliate` bigint(20) NOT NULL,
  `mobile_5_affiliate` bigint(20) NOT NULL,
  `mobile_6_affiliate` bigint(20) NOT NULL,
  `comments_affiliate` text NOT NULL,
  `status_affiliate` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `affiliates`
--

INSERT INTO `affiliates` (`id_affiliate`, `names_affiliate`, `last_names_affiliate`, `dni_affiliate`, `street_affiliate`, `number_affiliate`, `dtto_affiliate`, `postal_code_affiliate`, `job_affiliate`, `studies_affiliate`, `denomination_affiliate`, `circuit_affiliate`, `commune_affiliate`, `gener_affiliate`, `age_affiliate`, `facebook_affiliate`, `twitter_affiliate`, `instagram_affiliate`, `email_affiliate`, `phone_1_affiliate`, `phone_2_affiliate`, `checked_phone`, `mobile_1_affiliate`, `mobile_2_affiliate`, `mobile_3_affiliate`, `mobile_4_affiliate`, `mobile_5_affiliate`, `mobile_6_affiliate`, `comments_affiliate`, `status_affiliate`) VALUES
(1, 'Henry Eduardo', 'Martinez Marmole', 20823231, 'Primera calle', 123, 'Dtto. Capital', 1012, 'Desarrollador Web', 1, '', 12, 12, 'Masculino', 25, 'hemm', 'hemm_18_', 'hemm18', 'martinezhenry18@hotmail.com', 4248559520, 0, 0, 0, 0, 0, 0, 0, 0, '', 1),
(2, 'asdsdas', 'jhjkk', 0, 'h', 0, 'h', 0, 'hjh', 0, 'kh', 0, 0, 'jkh', 0, 'khj', 'jkh', 'hkhk', 'hkh', 0, 0, 0, 0, 0, 0, 0, 0, 0, 'sdfdf', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `affiliate_vendor`
--

CREATE TABLE IF NOT EXISTS `affiliate_vendor` (
  `id_affiliate_vendor` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `date_affiliate_vendor` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audio`
--

CREATE TABLE IF NOT EXISTS `audio` (
  `id_audio` int(11) NOT NULL,
  `title_audio` varchar(255) NOT NULL,
  `description_audio` varchar(255) NOT NULL,
  `folder_audio` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `check_users`
--

CREATE TABLE IF NOT EXISTS `check_users` (
  `id_check_users` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code_user` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `check_users`
--

INSERT INTO `check_users` (`id_check_users`, `user_id`, `code_user`) VALUES
(1, 1, 'RYoUDslnvg6yjiC5fOLywCGC04+7j+ZXHYkA+EnuH7+ZxDHyEH4f0oQ4MpmWv38DeaVwrmwLRfClITYyGTL+nA=='),
(2, 2, 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(3, 3, 'dFi0Afa8xGBsRtp3nDCqjYm2A9uP7rglC9CDhqhruTTATrmKS7kEKaUdv5cXnHcs6XPQGgpYPiFWEMgaCvh3Yg=='),
(4, 4, '7+r+SPuX6n7qpljGML1CRx70o9teDzA7xkdpJ97DodoAu/ARPCHND6S8RuF2+74PcIjlaSYps2cJ0V/AfObMEQ=='),
(5, 5, 'Op/fNcLLtNutCU5aKP4JrX6+gW7huomfyCarYh/kyR1PX1/8y5crRDVm81/lBaXMQyuKGaapHBMlSnv8JNU/Cg=='),
(6, 6, 'ewl9kahzYZm90UBwaM0HTvh9fFrQrkkJgTqG7+Ck9yAX3MI6YFTniA/K2ZTJ9gLaJDYnBcJSBVitqdFDS/87cA=='),
(7, 7, 'U4fOgjKOKOVcyPjiv7SN6jpWX8WxPx41Memq/XIGf/ieA9zwZoOW3JOme4pdlX5HRYu/JKzryOmqNUQao2aorQ==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts_affiliates`
--

CREATE TABLE IF NOT EXISTS `contacts_affiliates` (
  `id_contact_affliate` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `date_contact_affiliate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts_types`
--

CREATE TABLE IF NOT EXISTS `contacts_types` (
  `id_contacts_type` int(11) NOT NULL,
  `name_contacts_type` int(11) NOT NULL,
  `description_contacts_type` int(11) NOT NULL,
  `status_contacts_tye` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `id_faq` int(11) NOT NULL,
  `title_faq` varchar(255) NOT NULL,
  `answer_faq` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `general_rules`
--

CREATE TABLE IF NOT EXISTS `general_rules` (
  `id_general_rule` int(11) NOT NULL,
  `title_general_rule` varchar(255) NOT NULL,
  `description_general_rule` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master`
--

CREATE TABLE IF NOT EXISTS `master` (
  `id_master` int(11) NOT NULL,
  `name_master` varchar(255) NOT NULL,
  `cargo_master` varchar(255) NOT NULL,
  `date_register_master` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `check_user` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `master`
--

INSERT INTO `master` (`id_master`, `name_master`, `cargo_master`, `date_register_master`, `check_user`) VALUES
(1, 'Henry Martinez', 'Presidente', '2018-02-22 13:20:54', 'RYoUDslnvg6yjiC5fOLywCGC04+7j+ZXHYkA+EnuH7+ZxDHyEH4f0oQ4MpmWv38DeaVwrmwLRfClITYyGTL+nA==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_messages`
--

CREATE TABLE IF NOT EXISTS `model_messages` (
  `id_model_message` int(11) NOT NULL,
  `title_model_message` varchar(255) NOT NULL,
  `content_model_message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id_session` int(11) NOT NULL,
  `in_session` datetime NOT NULL,
  `out_session` datetime NOT NULL,
  `session_check_user` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `specialcheck`
--

CREATE TABLE IF NOT EXISTS `specialcheck` (
  `id_special_check` int(11) NOT NULL,
  `pass_special_check` varchar(255) NOT NULL,
  `created_special_check` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `specialcheck`
--

INSERT INTO `specialcheck` (`id_special_check`, `pass_special_check`, `created_special_check`, `status_id`) VALUES
(1, 'yOsu5Msvyr9QhnjAiN8Leq2bjMduPxllUtEEk02zKBOow2CNlDwwsxMtFFKUJ38oNt462h3QllOh/msqAA3aiQ==', '2018-02-22 11:43:12', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id_status` int(11) NOT NULL,
  `name_status` varchar(255) NOT NULL,
  `description_status` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id_status`, `name_status`, `description_status`) VALUES
(1, 'Activo', 'Status Activo para Todos los casos'),
(2, 'Inactivo', 'Inactivo para todos los casos'),
(3, 'Espera de Confirmacion', 'Espera de confirmacion a las solicitudes de vendedores'),
(4, 'Posible Voto', 'cliente que posiblemente registre');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `types_users`
--

CREATE TABLE IF NOT EXISTS `types_users` (
  `id_type_user` int(11) NOT NULL,
  `name_type` varchar(255) NOT NULL,
  `description_type` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `types_users`
--

INSERT INTO `types_users` (`id_type_user`, `name_type`, `description_type`) VALUES
(1, 'Master', 'Usuario Principal del Sistema'),
(2, 'Administrador', 'Administrador encargado por la empresa.'),
(3, 'Vendedor', 'Usuario admitido o registrado por un administrador o el master.'),
(4, 'Comun', 'Se le asignan los modulos a los que puede entrar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL,
  `name_user` varchar(255) NOT NULL,
  `pass_user` varchar(255) NOT NULL,
  `type_user` int(11) NOT NULL,
  `status_user` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user`, `name_user`, `pass_user`, `type_user`, `status_user`) VALUES
(1, 'hemm18', 'WW40Qtqr//sy30c5viGrsLHNSPNTj1vRSoDqGMOIb7PlYvR4KiUcxIb4hlSLSlNFfl0Q7C2/opI6hR9D6M+H9g==', 1, 1),
(2, 'admin', '23/tCAY1wDpOkk0fRu/mviJ1/DdqIhC5Z1OQ1VJjxs1wlgMbJyuY1hYo+bXZjc8cADv4BoiBGcLjYiJlR/U8Pw==', 2, 1),
(3, 'jperez', '23/tCAY1wDpOkk0fRu/mviJ1/DdqIhC5Z1OQ1VJjxs1wlgMbJyuY1hYo+bXZjc8cADv4BoiBGcLjYiJlR/U8Pw==', 2, 1),
(4, 'maper', '23/tCAY1wDpOkk0fRu/mviJ1/DdqIhC5Z1OQ1VJjxs1wlgMbJyuY1hYo+bXZjc8cADv4BoiBGcLjYiJlR/U8Pw==', 2, 1),
(5, 'tyrone', 'MuW4v12sBKulMzNNLCAwVi5ohu6H7lcUvdGyl10m0H/I+pNygVao1ucYpxjavRfwskm9py+W6qTcItUPykgWKg==', 2, 1),
(6, 'hola', 'D13w4qK+VHv6sg0S4o/s76zdkvFLrDp7lyyFphhbC0EgxO4hZuI+gUmVYghE6G0Uc3wRunPV51KrMz55sgGdfw==', 2, 1),
(7, 'vendor@aout.com', 'uc/d0gUMrdgqrbt50ckji0Zg2wCBvqBJzGMMq+VcGLM66fpFFRN5VqGvynpimWCIVyg+TLqDRaUr6dO5KgqzBA==', 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendors`
--

CREATE TABLE IF NOT EXISTS `vendors` (
  `id_vendor` int(11) NOT NULL,
  `name_vendor` varchar(255) NOT NULL,
  `phone_vendor` varchar(255) NOT NULL,
  `direction_vendor` text NOT NULL,
  `email_vendor` varchar(255) NOT NULL,
  `sex_vendor` varchar(200) NOT NULL,
  `check_user_vendor` varchar(255) NOT NULL,
  `dni_vendor` varchar(50) NOT NULL,
  `age_vendor` int(11) NOT NULL,
  `date_register_vendor` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vendors`
--

INSERT INTO `vendors` (`id_vendor`, `name_vendor`, `phone_vendor`, `direction_vendor`, `email_vendor`, `sex_vendor`, `check_user_vendor`, `dni_vendor`, `age_vendor`, `date_register_vendor`, `status_id`) VALUES
(2, 'Henry Martinez', '', '', 'vendor@aout.com', 'Masculino', 'U4fOgjKOKOVcyPjiv7SN6jpWX8WxPx41Memq/XIGf/ieA9zwZoOW3JOme4pdlX5HRYu/JKzryOmqNUQao2aorQ==', '20823231', 25, '2018-02-27 13:01:07', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id_activity`);

--
-- Indices de la tabla `activities_affiliates`
--
ALTER TABLE `activities_affiliates`
  ADD PRIMARY KEY (`id_activity_affiliate`);

--
-- Indices de la tabla `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `affiliates`
--
ALTER TABLE `affiliates`
  ADD PRIMARY KEY (`id_affiliate`);

--
-- Indices de la tabla `affiliate_vendor`
--
ALTER TABLE `affiliate_vendor`
  ADD PRIMARY KEY (`id_affiliate_vendor`);

--
-- Indices de la tabla `audio`
--
ALTER TABLE `audio`
  ADD PRIMARY KEY (`id_audio`);

--
-- Indices de la tabla `check_users`
--
ALTER TABLE `check_users`
  ADD PRIMARY KEY (`id_check_users`);

--
-- Indices de la tabla `contacts_affiliates`
--
ALTER TABLE `contacts_affiliates`
  ADD PRIMARY KEY (`id_contact_affliate`);

--
-- Indices de la tabla `contacts_types`
--
ALTER TABLE `contacts_types`
  ADD PRIMARY KEY (`id_contacts_type`);

--
-- Indices de la tabla `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id_faq`);

--
-- Indices de la tabla `general_rules`
--
ALTER TABLE `general_rules`
  ADD PRIMARY KEY (`id_general_rule`);

--
-- Indices de la tabla `master`
--
ALTER TABLE `master`
  ADD PRIMARY KEY (`id_master`);

--
-- Indices de la tabla `model_messages`
--
ALTER TABLE `model_messages`
  ADD PRIMARY KEY (`id_model_message`);

--
-- Indices de la tabla `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id_session`), ADD UNIQUE KEY `session_check_user` (`session_check_user`);

--
-- Indices de la tabla `specialcheck`
--
ALTER TABLE `specialcheck`
  ADD PRIMARY KEY (`id_special_check`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indices de la tabla `types_users`
--
ALTER TABLE `types_users`
  ADD PRIMARY KEY (`id_type_user`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indices de la tabla `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id_vendor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activities`
--
ALTER TABLE `activities`
  MODIFY `id_activity` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `activities_affiliates`
--
ALTER TABLE `activities_affiliates`
  MODIFY `id_activity_affiliate` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `administrators`
--
ALTER TABLE `administrators`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `affiliates`
--
ALTER TABLE `affiliates`
  MODIFY `id_affiliate` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `affiliate_vendor`
--
ALTER TABLE `affiliate_vendor`
  MODIFY `id_affiliate_vendor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `audio`
--
ALTER TABLE `audio`
  MODIFY `id_audio` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `check_users`
--
ALTER TABLE `check_users`
  MODIFY `id_check_users` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `contacts_affiliates`
--
ALTER TABLE `contacts_affiliates`
  MODIFY `id_contact_affliate` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `contacts_types`
--
ALTER TABLE `contacts_types`
  MODIFY `id_contacts_type` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `faq`
--
ALTER TABLE `faq`
  MODIFY `id_faq` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `general_rules`
--
ALTER TABLE `general_rules`
  MODIFY `id_general_rule` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `master`
--
ALTER TABLE `master`
  MODIFY `id_master` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `model_messages`
--
ALTER TABLE `model_messages`
  MODIFY `id_model_message` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id_session` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `specialcheck`
--
ALTER TABLE `specialcheck`
  MODIFY `id_special_check` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `types_users`
--
ALTER TABLE `types_users`
  MODIFY `id_type_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id_vendor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
