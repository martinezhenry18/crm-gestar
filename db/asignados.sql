-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-02-2018 a las 22:12:08
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crm_gestar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `affiliate_vendor`
--

DROP TABLE IF EXISTS `affiliate_vendor`;
CREATE TABLE `affiliate_vendor` (
  `id_affiliate_vendor` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `date_affiliate_vendor` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `affiliate_vendor`
--

INSERT INTO `affiliate_vendor` (`id_affiliate_vendor`, `affiliate_id`, `vendor_id`, `date_affiliate_vendor`) VALUES
(1, 742, 5, '2018-02-28 20:52:36'),
(2, 358, 4, '2018-02-28 20:52:36'),
(3, 903, 6, '2018-02-28 20:52:36'),
(4, 917, 6, '2018-02-28 20:52:36'),
(5, 892, 4, '2018-02-28 20:52:36'),
(6, 72, 3, '2018-02-28 20:52:36'),
(7, 869, 3, '2018-02-28 20:52:36'),
(8, 45, 3, '2018-02-28 20:52:36'),
(9, 306, 6, '2018-02-28 20:52:36'),
(10, 725, 4, '2018-02-28 20:52:36'),
(11, 858, 7, '2018-02-28 20:52:36'),
(12, 590, 5, '2018-02-28 20:52:36'),
(13, 497, 6, '2018-02-28 20:52:36'),
(14, 90, 8, '2018-02-28 20:52:36'),
(15, 795, 3, '2018-02-28 20:52:36'),
(16, 43, 5, '2018-02-28 20:52:36'),
(17, 452, 7, '2018-02-28 20:52:36'),
(18, 802, 8, '2018-02-28 20:52:36'),
(19, 525, 4, '2018-02-28 20:52:36'),
(20, 597, 5, '2018-02-28 20:52:36'),
(21, 640, 8, '2018-02-28 20:52:36'),
(22, 879, 8, '2018-02-28 20:52:36'),
(23, 719, 7, '2018-02-28 20:52:36'),
(24, 91, 5, '2018-02-28 20:52:36'),
(25, 479, 8, '2018-02-28 20:52:36'),
(26, 457, 7, '2018-02-28 20:52:36'),
(27, 441, 3, '2018-02-28 20:52:36'),
(28, 780, 8, '2018-02-28 20:52:36'),
(29, 744, 8, '2018-02-28 20:52:36'),
(30, 467, 7, '2018-02-28 20:52:36'),
(31, 474, 3, '2018-02-28 20:52:36'),
(32, 589, 5, '2018-02-28 20:52:36'),
(33, 779, 6, '2018-02-28 20:52:36'),
(34, 154, 5, '2018-02-28 20:52:36'),
(35, 803, 3, '2018-02-28 20:52:36'),
(36, 254, 6, '2018-02-28 20:52:36'),
(37, 596, 8, '2018-02-28 20:52:36'),
(38, 519, 8, '2018-02-28 20:52:36'),
(39, 555, 8, '2018-02-28 20:52:36'),
(40, 361, 7, '2018-02-28 20:52:36'),
(41, 772, 7, '2018-02-28 20:52:36'),
(42, 199, 7, '2018-02-28 20:52:36'),
(43, 722, 8, '2018-02-28 20:52:36'),
(44, 554, 4, '2018-02-28 20:52:36'),
(45, 159, 8, '2018-02-28 20:52:36'),
(46, 270, 8, '2018-02-28 20:52:36'),
(47, 645, 8, '2018-02-28 20:52:36'),
(48, 590, 8, '2018-02-28 20:52:36'),
(49, 895, 7, '2018-02-28 20:52:36'),
(50, 706, 3, '2018-02-28 20:52:36'),
(51, 112, 4, '2018-02-28 20:52:36'),
(52, 817, 3, '2018-02-28 20:52:36'),
(53, 287, 4, '2018-02-28 20:52:36'),
(54, 753, 3, '2018-02-28 20:52:36'),
(55, 207, 8, '2018-02-28 20:52:36'),
(56, 286, 5, '2018-02-28 20:52:36'),
(57, 833, 4, '2018-02-28 20:52:36'),
(58, 277, 4, '2018-02-28 20:52:36'),
(59, 535, 3, '2018-02-28 20:52:36'),
(60, 740, 5, '2018-02-28 20:52:36'),
(61, 167, 8, '2018-02-28 20:52:36'),
(62, 58, 3, '2018-02-28 20:52:36'),
(63, 829, 7, '2018-02-28 20:52:36'),
(64, 474, 8, '2018-02-28 20:52:36');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendors`
--

DROP TABLE IF EXISTS `vendors`;
CREATE TABLE `vendors` (
  `id_vendor` int(11) NOT NULL,
  `name_vendor` varchar(255) NOT NULL,
  `phone_vendor` varchar(255) NOT NULL,
  `direction_vendor` text NOT NULL,
  `email_vendor` varchar(255) NOT NULL,
  `sex_vendor` varchar(200) NOT NULL,
  `check_user_vendor` varchar(255) NOT NULL,
  `dni_vendor` varchar(50) NOT NULL,
  `age_vendor` int(11) NOT NULL,
  `date_register_vendor` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vendors`
--

INSERT INTO `vendors` (`id_vendor`, `name_vendor`, `phone_vendor`, `direction_vendor`, `email_vendor`, `sex_vendor`, `check_user_vendor`, `dni_vendor`, `age_vendor`, `date_register_vendor`, `status_id`) VALUES
(3, 'Mercedes Marighetti', '', '', 'mercedesmarighetti@gmail.com', '	Femenino', '', '35678820', 26, '2018-02-28 20:34:54', 1),
(4, 'steven munoz', '', '', 'steven.cabo18@gmail.com', '	Masculino', '', '1452', 25, '2018-02-28 20:34:54', 1),
(5, 'Federico Treguer', '', '', 'fstreguer@gmail.com', '	Masculino', '', '39207552', 22, '2018-02-28 20:34:54', 1),
(6, 'Luis Gotfryd', '', '', 'Lggot@yahoo.com.ar', '	Masculino', '', '14727777', 56, '2018-02-28 20:34:54', 1),
(7, 'Marcelo Pascal', '', '', 'marcelopascal@yahoo.com.ar', '	Masculino', '', '20369251', 49, '2018-02-28 20:34:54', 1),
(8, 'Hernan Martini', '', '', 'rrcc100@hotmail.com', '	Masculino', '', '33563445', 30, '2018-02-28 20:34:54', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `affiliate_vendor`
--
ALTER TABLE `affiliate_vendor`
  ADD PRIMARY KEY (`id_affiliate_vendor`);

--
-- Indices de la tabla `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id_vendor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `affiliate_vendor`
--
ALTER TABLE `affiliate_vendor`
  MODIFY `id_affiliate_vendor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT de la tabla `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id_vendor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
