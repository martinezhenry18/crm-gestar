-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-02-2018 a las 14:21:50
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `crm_gestar`
--
CREATE DATABASE IF NOT EXISTS `crm_gestar` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `crm_gestar`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrators`
--

CREATE TABLE IF NOT EXISTS `administrators` (
  `id_admin` int(11) NOT NULL,
  `name_admin` varchar(255) NOT NULL,
  `phone_admin` varchar(255) NOT NULL,
  `direction_admin` text NOT NULL,
  `email_admin` varchar(255) NOT NULL,
  `sex_admin` int(11) NOT NULL,
  `check_user_admin` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `check_users`
--

CREATE TABLE IF NOT EXISTS `check_users` (
  `id_check_users` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code_user` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `check_users`
--

INSERT INTO `check_users` (`id_check_users`, `user_id`, `code_user`) VALUES
(1, 1, 'RYoUDslnvg6yjiC5fOLywCGC04+7j+ZXHYkA+EnuH7+ZxDHyEH4f0oQ4MpmWv38DeaVwrmwLRfClITYyGTL+nA==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master`
--

CREATE TABLE IF NOT EXISTS `master` (
  `id_master` int(11) NOT NULL,
  `name_master` varchar(255) NOT NULL,
  `cargo_master` varchar(255) NOT NULL,
  `date_register_master` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `check_user` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `master`
--

INSERT INTO `master` (`id_master`, `name_master`, `cargo_master`, `date_register_master`, `check_user`) VALUES
(1, 'Henry Martinez', 'Presidente', '2018-02-22 13:20:54', 'RYoUDslnvg6yjiC5fOLywCGC04+7j+ZXHYkA+EnuH7+ZxDHyEH4f0oQ4MpmWv38DeaVwrmwLRfClITYyGTL+nA==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `specialcheck`
--

CREATE TABLE IF NOT EXISTS `specialcheck` (
  `id_special_check` int(11) NOT NULL,
  `pass_special_check` varchar(255) NOT NULL,
  `created_special_check` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `specialcheck`
--

INSERT INTO `specialcheck` (`id_special_check`, `pass_special_check`, `created_special_check`, `status_id`) VALUES
(1, 'yOsu5Msvyr9QhnjAiN8Leq2bjMduPxllUtEEk02zKBOow2CNlDwwsxMtFFKUJ38oNt462h3QllOh/msqAA3aiQ==', '2018-02-22 11:43:12', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id_status` int(11) NOT NULL,
  `name_status` varchar(255) NOT NULL,
  `description_status` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id_status`, `name_status`, `description_status`) VALUES
(1, 'Activo', 'Status Activo para Todos los casos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `types_users`
--

CREATE TABLE IF NOT EXISTS `types_users` (
  `id_type_user` int(11) NOT NULL,
  `name_type` varchar(255) NOT NULL,
  `description_type` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `types_users`
--

INSERT INTO `types_users` (`id_type_user`, `name_type`, `description_type`) VALUES
(1, 'Master', 'Usuario Principal del Sistema');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL,
  `name_user` varchar(255) NOT NULL,
  `pass_user` varchar(255) NOT NULL,
  `type_user` int(11) NOT NULL,
  `status_user` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user`, `name_user`, `pass_user`, `type_user`, `status_user`) VALUES
(1, 'hemm18', 'WW40Qtqr//sy30c5viGrsLHNSPNTj1vRSoDqGMOIb7PlYvR4KiUcxIb4hlSLSlNFfl0Q7C2/opI6hR9D6M+H9g==', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendors`
--

CREATE TABLE IF NOT EXISTS `vendors` (
  `id_vendor` int(11) NOT NULL,
  `name_vendor` varchar(255) NOT NULL,
  `phone_vendor` varchar(255) NOT NULL,
  `direction_vendor` text NOT NULL,
  `email_vendor` varchar(255) NOT NULL,
  `sex_vendor` int(11) NOT NULL,
  `check_user_vendor` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `check_users`
--
ALTER TABLE `check_users`
  ADD PRIMARY KEY (`id_check_users`);

--
-- Indices de la tabla `master`
--
ALTER TABLE `master`
  ADD PRIMARY KEY (`id_master`);

--
-- Indices de la tabla `specialcheck`
--
ALTER TABLE `specialcheck`
  ADD PRIMARY KEY (`id_special_check`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indices de la tabla `types_users`
--
ALTER TABLE `types_users`
  ADD PRIMARY KEY (`id_type_user`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indices de la tabla `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id_vendor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrators`
--
ALTER TABLE `administrators`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `check_users`
--
ALTER TABLE `check_users`
  MODIFY `id_check_users` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `master`
--
ALTER TABLE `master`
  MODIFY `id_master` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `specialcheck`
--
ALTER TABLE `specialcheck`
  MODIFY `id_special_check` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `types_users`
--
ALTER TABLE `types_users`
  MODIFY `id_type_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id_vendor` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
