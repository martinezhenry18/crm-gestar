-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 05-03-2018 a las 18:58:01
-- Versión del servidor: 5.6.24
-- Versión de PHP: 5.5.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `crm_gestar`
--
CREATE DATABASE IF NOT EXISTS `crm_gestar` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `crm_gestar`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id_activity` int(11) NOT NULL,
  `name_activity` varchar(255) NOT NULL,
  `description_activity` text NOT NULL,
  `date_activity` date NOT NULL,
  `hour_activity` time NOT NULL,
  `address_activity` text NOT NULL,
  `type_activity` varchar(255) NOT NULL,
  `image_activity` varchar(255) NOT NULL,
  `age_activity` varchar(255) NOT NULL,
  `genre_activity` int(11) NOT NULL,
  `category_activity` int(11) NOT NULL,
  `commune_activity` int(11) NOT NULL,
  `status_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `activities_affiliates`
--

CREATE TABLE IF NOT EXISTS `activities_affiliates` (
  `id_activity_affiliate` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrators`
--

CREATE TABLE IF NOT EXISTS `administrators` (
  `id_admin` int(11) NOT NULL,
  `name_admin` varchar(255) NOT NULL,
  `phone_admin` varchar(255) NOT NULL,
  `direction_admin` text NOT NULL,
  `email_admin` varchar(255) NOT NULL,
  `sex_admin` varchar(200) NOT NULL,
  `check_user_admin` varchar(255) NOT NULL,
  `date_register_admin` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrators`
--

INSERT INTO `administrators` (`id_admin`, `name_admin`, `phone_admin`, `direction_admin`, `email_admin`, `sex_admin`, `check_user_admin`, `date_register_admin`, `status_id`) VALUES
(1, 'Julian Bagilet', '+543415198035', 'El rosario Argentina', 'sales@jbosolutions.com', 'Masculino', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow==', '2018-02-25 16:16:14', 1),
(2, 'Jhon Perez', '+573055633212', 'Bogota Colombia', 'jhon.perez@gmail.com', 'Masculino', 'dFi0Afa8xGBsRtp3nDCqjYm2A9uP7rglC9CDhqhruTTATrmKS7kEKaUdv5cXnHcs6XPQGgpYPiFWEMgaCvh3Yg==', '2018-02-25 16:16:14', 1),
(3, 'Maritza Perex', '+584248559520', 'caracas venezuela', 'mari.perex@outlook.com', 'Femenino', '7+r+SPuX6n7qpljGML1CRx70o9teDzA7xkdpJ97DodoAu/ARPCHND6S8RuF2+74PcIjlaSYps2cJ0V/AfObMEQ==', '2018-02-25 16:16:14', 1),
(4, 'tyrone', '+5834348834', 'la pica', 'tyrone@gnail.com', 'Masculino', 'Op/fNcLLtNutCU5aKP4JrX6+gW7huomfyCarYh/kyR1PX1/8y5crRDVm81/lBaXMQyuKGaapHBMlSnv8JNU/Cg==', '2018-02-25 16:16:14', 1),
(5, 'Henry E. Martinez', '4248559520', 'Caracas Venezuela', 'hgmanriquezm@gmail.com', 'Masculino', 'ewl9kahzYZm90UBwaM0HTvh9fFrQrkkJgTqG7+Ck9yAX3MI6YFTniA/K2ZTJ9gLaJDYnBcJSBVitqdFDS/87cA==', '2018-03-05 08:08:08', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `affiliate_vendor`
--

CREATE TABLE IF NOT EXISTS `affiliate_vendor` (
  `id_affiliate_vendor` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `date_affiliate_vendor` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `affiliate_vendor`
--

INSERT INTO `affiliate_vendor` (`id_affiliate_vendor`, `affiliate_id`, `vendor_id`, `date_affiliate_vendor`) VALUES
(3, 903, 6, '2018-02-28 20:52:36'),
(4, 917, 6, '2018-02-28 20:52:36'),
(5, 892, 4, '2018-02-28 20:52:36'),
(6, 72, 3, '2018-02-28 20:52:36'),
(7, 869, 3, '2018-02-28 20:52:36'),
(8, 45, 3, '2018-02-28 20:52:36'),
(9, 306, 6, '2018-02-28 20:52:36'),
(10, 725, 4, '2018-02-28 20:52:36'),
(11, 858, 7, '2018-02-28 20:52:36'),
(12, 590, 5, '2018-02-28 20:52:36'),
(13, 497, 6, '2018-02-28 20:52:36'),
(14, 90, 8, '2018-02-28 20:52:36'),
(15, 795, 3, '2018-02-28 20:52:36'),
(16, 43, 5, '2018-02-28 20:52:36'),
(17, 452, 7, '2018-02-28 20:52:36'),
(18, 802, 8, '2018-02-28 20:52:36'),
(19, 525, 4, '2018-02-28 20:52:36'),
(20, 597, 5, '2018-02-28 20:52:36'),
(21, 640, 8, '2018-02-28 20:52:36'),
(22, 879, 8, '2018-02-28 20:52:36'),
(23, 719, 7, '2018-02-28 20:52:36'),
(25, 479, 8, '2018-02-28 20:52:36'),
(26, 457, 7, '2018-02-28 20:52:36'),
(27, 441, 3, '2018-02-28 20:52:36'),
(29, 744, 8, '2018-02-28 20:52:36'),
(30, 467, 7, '2018-02-28 20:52:36'),
(31, 474, 3, '2018-02-28 20:52:36'),
(33, 779, 6, '2018-02-28 20:52:36'),
(35, 803, 3, '2018-02-28 20:52:36'),
(36, 254, 6, '2018-02-28 20:52:36'),
(37, 596, 8, '2018-02-28 20:52:36'),
(38, 519, 8, '2018-02-28 20:52:36'),
(39, 555, 8, '2018-02-28 20:52:36'),
(40, 361, 7, '2018-02-28 20:52:36'),
(41, 772, 7, '2018-02-28 20:52:36'),
(42, 199, 7, '2018-02-28 20:52:36'),
(43, 722, 8, '2018-02-28 20:52:36'),
(44, 554, 4, '2018-02-28 20:52:36'),
(45, 159, 8, '2018-02-28 20:52:36'),
(46, 270, 8, '2018-02-28 20:52:36'),
(47, 645, 8, '2018-02-28 20:52:36'),
(48, 590, 8, '2018-02-28 20:52:36'),
(49, 895, 7, '2018-02-28 20:52:36'),
(50, 706, 3, '2018-02-28 20:52:36'),
(51, 112, 4, '2018-02-28 20:52:36'),
(52, 817, 3, '2018-02-28 20:52:36'),
(53, 287, 4, '2018-02-28 20:52:36'),
(54, 753, 3, '2018-02-28 20:52:36'),
(55, 207, 8, '2018-02-28 20:52:36'),
(57, 833, 4, '2018-02-28 20:52:36'),
(58, 277, 4, '2018-02-28 20:52:36'),
(59, 535, 3, '2018-02-28 20:52:36'),
(60, 740, 5, '2018-02-28 20:52:36'),
(61, 167, 8, '2018-02-28 20:52:36'),
(62, 58, 3, '2018-02-28 20:52:36'),
(63, 829, 7, '2018-02-28 20:52:36'),
(64, 474, 8, '2018-02-28 20:52:36'),
(70, 154, 5, '2018-03-05 09:55:08'),
(71, 245, 5, '2018-03-05 09:55:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `affiliates`
--

CREATE TABLE IF NOT EXISTS `affiliates` (
  `id_affiliate` int(11) NOT NULL,
  `names_affiliate` varchar(255) NOT NULL,
  `last_names_affiliate` varchar(255) NOT NULL,
  `dni_affiliate` bigint(20) NOT NULL,
  `street_affiliate` varchar(255) NOT NULL,
  `number_affiliate` int(11) NOT NULL,
  `dtto_affiliate` varchar(255) NOT NULL,
  `postal_code_affiliate` int(11) NOT NULL,
  `job_affiliate` varchar(255) NOT NULL,
  `studies_affiliate` varchar(50) NOT NULL,
  `denomination_affiliate` varchar(255) NOT NULL,
  `circuit_affiliate` int(11) NOT NULL,
  `commune_affiliate` int(11) NOT NULL,
  `gener_affiliate` varchar(255) NOT NULL,
  `age_affiliate` int(11) NOT NULL,
  `facebook_affiliate` varchar(255) NOT NULL,
  `twitter_affiliate` varchar(255) NOT NULL,
  `instagram_affiliate` varchar(255) NOT NULL,
  `email_affiliate` varchar(255) NOT NULL,
  `phone_1_affiliate` bigint(20) NOT NULL,
  `phone_2_affiliate` bigint(20) NOT NULL,
  `checked_phone` int(11) NOT NULL DEFAULT '0',
  `mobile_1_affiliate` bigint(20) NOT NULL,
  `mobile_2_affiliate` bigint(20) NOT NULL,
  `mobile_3_affiliate` bigint(20) NOT NULL,
  `mobile_4_affiliate` bigint(20) NOT NULL,
  `mobile_5_affiliate` bigint(20) NOT NULL,
  `mobile_6_affiliate` bigint(20) NOT NULL,
  `comments_affiliate` text NOT NULL,
  `status_affiliate` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=933 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `affiliates`
--

INSERT INTO `affiliates` (`id_affiliate`, `names_affiliate`, `last_names_affiliate`, `dni_affiliate`, `street_affiliate`, `number_affiliate`, `dtto_affiliate`, `postal_code_affiliate`, `job_affiliate`, `studies_affiliate`, `denomination_affiliate`, `circuit_affiliate`, `commune_affiliate`, `gener_affiliate`, `age_affiliate`, `facebook_affiliate`, `twitter_affiliate`, `instagram_affiliate`, `email_affiliate`, `phone_1_affiliate`, `phone_2_affiliate`, `checked_phone`, `mobile_1_affiliate`, `mobile_2_affiliate`, `mobile_3_affiliate`, `mobile_4_affiliate`, `mobile_5_affiliate`, `mobile_6_affiliate`, `comments_affiliate`, `status_affiliate`) VALUES
(1, 'ELVIRA', 'ABAD', 4983807, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(2, 'ANDREA VERONICA', 'ABAD', 25538122, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(3, 'MATIAS', 'ABADIE', 26949105, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(4, 'ADRIANA SILVIA', 'ABAL', 5474515, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '<ul><li>El cliente respondio a los anuncios enviados al WhatsApp</li></ul>', 6),
(5, 'LILIANA LAURA', 'ABAL', 10468356, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(6, 'ROQUE JORGE', 'ABALEN', 7682215, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(7, 'CLAUDIO ENRIQUE', 'ABALEN', 24431543, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(8, 'LUISA ROSA', 'ABALLE', 2645780, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(9, 'NATALIA GIMENA', 'ABALLE', 30064098, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(10, 'CARLOS HORACIO', 'ABALO', 12419736, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(11, 'VICTORIA TERESA', 'ABALO', 14309102, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(12, 'DAMIAN GASTON', 'ABALO', 28361090, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(13, 'MARIA ELIDA', 'ABALSAMO', 12009939, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(14, 'TRINIDAD FRANCISCA', 'ABAN', 25494392, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(15, 'FABIANA DEL CARMEN', 'ABAN', 33543358, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(16, 'FABIANA ADELAIDA', 'ABAN', 33543359, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(17, 'OSVALDO', 'ABANCES', 4400730, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(18, 'HORACIO ANTONIO', 'ABARCA', 4405675, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(19, 'REGINA', 'ABAS', 60670, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(20, 'GASTON JAVIER', 'ABAS', 22992857, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(21, 'LILIANA', 'ABATE', 5251148, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(22, 'DALMIRO PABLO', 'ABBAS', 23643577, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(23, 'LUIS JUAN', 'ABBIATI', 4658481, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(24, 'JUAN COSME', 'ABBRUZZESE', 8264997, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(25, 'MARIA DEL CARMEN', 'ABDALA', 3580959, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(26, 'ALEJANDRO JORGE', 'ABDELNUR', 16559484, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(27, 'ROSA PAULA', 'ABENDA?O LOPEZ', 24663337, '', 0, '', 0, '', '0', '', 0, 13, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(28, 'HECTOR MARIO', 'ABERASTAIN', 16580565, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(29, 'FEDERICO LUIS', 'ABERASTURY', 4294000, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(30, 'SERGIO G', 'ABERASTURY', 16557695, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(31, 'MONICA GLADYS', 'ABIB', 14822083, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(32, 'BERTA', 'ABOVSKY', 3179012, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(33, 'RAQUEL MARIA', 'ABRAHAM', 4235930, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(34, 'KARINA PAULA', 'ABRAIRA', 23470246, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(35, 'ALFREDO ANIBAL', 'ABRALDES', 4418961, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(36, 'VIVIANA EDITH', 'ABRALDEZ', 14313907, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(37, 'CARLOS MARIA', 'ABRAM LUJAN', 16973431, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(38, 'REGINA', 'ABRAMSON', 137880, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(39, 'FACUNDO ARIEL', 'ABRANZON', 37378715, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(40, 'ESTELA MARIA CATALINA', 'ABRATE', 4820810, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(41, 'DOLY INES', 'ABREGU', 3323858, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(42, 'MARIA VIRGINIA', 'ABREGU', 17062007, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(43, 'ERICA ELENA', 'ABREGU', 23654119, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(44, 'MARTIN ALBERTO', 'ABREGU', 25630726, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(45, 'MARIANA ALEJANDRA', 'ABREHU', 23669486, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(46, 'CARLOS ANTONIO', 'ABRIOLA', 4140206, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(47, 'ANGELA BEATRIZ', 'ABRODOS', 3541252, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(48, 'ANA CRISTINA', 'ABRODOS', 10478094, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(49, 'MANUEL ANIBAL', 'ABRODOS', 22148921, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(50, 'JULIO CESAR OSCAR', 'ABUSTO', 22402277, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(51, 'MARIA VANINA', 'ABUSTO', 28109627, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(52, 'RODOLFO ANTONIO', 'ACCARI', 4477935, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(53, 'RICARDO', 'ACCETTA', 416649, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(54, 'MARIA ELENA', 'ACCIARDI', 5095459, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(55, 'LORENA DULCE JESUS', 'ACCIARDI', 26236705, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(56, 'MARIA CECILIA', 'ACCORINTI', 20922348, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(57, 'SUSANA NORMA', 'ACCORSI', 6418412, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(58, 'GERARDO DANIEL', 'ACEA', 8318019, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(59, 'OSCAR BENITO', 'ACEBEDO', 4120224, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(60, 'SERGIO GABRIEL', 'ACEBEDO', 14316967, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(61, 'GABRIEL NORBERTO', 'ACERBI', 14013118, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(62, 'ADRIAN MARIANO', 'ACETO', 25248765, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(63, 'JACINTO', 'ACEVEDO', 4419508, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(64, 'MARIA ZULMA', 'ACEVEDO', 4969371, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(65, 'MARIA BARTOLINA', 'ACEVEDO', 5571208, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(66, 'SARA DEL VALLE', 'ACEVEDO', 6492949, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(67, 'RAMON', 'ACEVEDO', 7698990, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(68, 'JUAN', 'ACEVEDO', 8404703, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(69, 'ALBERTO', 'ACEVEDO', 10889955, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(70, 'MARIA ANGELICA', 'ACEVEDO', 11687188, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(71, 'ADELA ESTHER', 'ACEVEDO', 12743183, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(72, 'ANTONIA', 'ACEVEDO', 13349464, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(73, 'JUAN CARLOS', 'ACEVEDO', 13624043, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(74, 'JUANA ELISA', 'ACEVEDO', 13982214, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(75, 'ANA TERESA', 'ACEVEDO', 14495527, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(76, 'JOSE ALBERTO', 'ACEVEDO', 16938676, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(77, 'ARACELI NANCY', 'ACEVEDO', 16974096, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(78, 'MARIA DEL CARMEN', 'ACEVEDO', 17891978, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(79, 'ANDREA ADELMA', 'ACEVEDO', 18315925, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(80, 'FELIX', 'ACEVEDO', 18686742, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(81, 'HECTOR OSVALDO', 'ACEVEDO', 21050023, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(82, 'MARIANA NOELIA', 'ACEVEDO', 23839796, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(83, 'SILVIO GUSTAVO', 'ACEVEDO', 23952438, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(84, 'MARTIN FACUNDO', 'ACEVEDO', 27230337, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(85, 'PAULA SOLEDAD', 'ACEVEDO', 30023204, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(86, 'GONZALO EXEQUIEL', 'ACEVEDO', 31469792, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(87, 'RODRIGO', 'ACEVEDO', 35962320, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(88, 'CLAUDIO SEBASTIAN', 'ACEVEDO', 36387685, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(89, 'DOMINGA DEL VALE', 'ACHAVAL', 6257493, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(90, 'FRANCISCO A', 'ACHAVAL', 7127445, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(91, 'NICOLAS EZEQUIEL', 'ACHDJIAN', 37933956, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(92, 'ALBERTO DAVID', 'ACHER', 4338573, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(93, 'ERNESTO', 'ACHES', 7086774, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(94, 'MARTA JUDITH', 'ACKENS', 4406214, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(95, 'ELISA ADELFA', 'ACORIA', 6428264, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(96, 'NELLY ROSA', 'ACOSTA', 1736624, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(97, 'BLACIDA', 'ACOSTA', 2428103, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(98, 'JUAN CARLOS', 'ACOSTA', 4166625, '', 0, '', 0, '', '0', '', 0, 12, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(99, 'ELENA', 'ACOSTA', 4846347, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(100, 'GRACIELA ELSA', 'ACOSTA', 5726680, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(101, 'TOMAS A', 'ACOSTA', 6085798, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(102, 'MARCELINO', 'ACOSTA', 7613170, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(103, 'ABEL ALCIDES DENIS', 'ACOSTA', 8420663, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(104, 'OSVALDO GABRIEL', 'ACOSTA', 8447479, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(105, 'NELIDA', 'ACOSTA', 11203935, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(106, 'JORGE ROBERTO', 'ACOSTA', 11330367, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(107, 'CARMEN HAYDEE', 'ACOSTA', 11436303, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(108, 'LILIANA GRACIELA', 'ACOSTA', 12489240, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(109, 'ANTONIA ARMINDA', 'ACOSTA', 12597478, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(110, 'ALDA GRICELDA', 'ACOSTA', 12687726, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(111, 'SUSANA BEATRIZ', 'ACOSTA', 12771943, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(112, 'NILDA CRISTINA', 'ACOSTA', 12945102, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(113, 'LUIS EDUARDO', 'ACOSTA', 13687382, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(114, 'CARLOS RUBEN', 'ACOSTA', 13755625, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(115, 'OSVALDO ENRIQUE', 'ACOSTA', 13914273, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(116, 'PATRICIA AMELIA', 'ACOSTA', 13919806, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(117, 'PERLA', 'ACOSTA', 14091085, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(118, 'ESTHER', 'ACOSTA', 14799544, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(119, 'MONICA PATRICIA', 'ACOSTA', 14931807, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(120, 'ELVIRA NOEMI', 'ACOSTA', 16000679, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(121, 'ORLANDO HECTOR', 'ACOSTA', 16227284, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(122, 'RAMON FLAUDIANO', 'ACOSTA', 16444781, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(123, 'ANA MARIA', 'ACOSTA', 17430479, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(124, 'CARLOS ALBERTO', 'ACOSTA', 17651603, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(125, 'MARCELO S', 'ACOSTA', 18389808, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(126, 'LAURA KARINA', 'ACOSTA', 20912853, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(127, 'GUSTAVO GABRIEL', 'ACOSTA', 21052231, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(128, 'MARIA LAURA', 'ACOSTA', 21820905, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(129, 'LEONARDO ADRIAN', 'ACOSTA', 22457641, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(130, 'MYRIAN KARINA', 'ACOSTA', 23191885, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(131, 'ERNESTO CAMILO', 'ACOSTA', 23524534, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(132, 'CECILIA MARIANA', 'ACOSTA', 23705403, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(133, 'DIEGO OMAR', 'ACOSTA', 23847832, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(134, 'MARIA DE LOS ANGELES', 'ACOSTA', 25119842, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(135, 'MARCELO CHRISTIAN', 'ACOSTA', 25414328, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(136, 'MARCELO GABRIEL', 'ACOSTA', 25551991, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(137, 'MARIELA ROSANA', 'ACOSTA', 26689701, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(138, 'AMELIA DEL CARMEN', 'ACOSTA', 27852849, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(139, 'MIGUEL ALEJANDRO', 'ACOSTA', 29854972, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(140, 'GLADYS SARA', 'ACOSTA', 30512261, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(141, 'RITA BELINDA', 'ACOSTA', 30534633, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(142, 'CINTHIA GISELLE', 'ACOSTA', 30591229, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(143, 'ROMINA VANESA', 'ACOSTA', 32112041, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(144, 'MARTHA ELOISA', 'ACOSTA GONZALEZ', 24636939, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(145, 'CARLOS ALBERTO', 'ACOSTA RODRIGUEZ', 27710787, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(146, 'SERGIO HERNAN', 'ACOSTA SUAREZ', 33498671, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(147, 'ALCIRA EROIDES', 'ACQUARONE', 5579668, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(148, 'ALEJANDRO JULIO', 'ACQUARONE', 12976964, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(149, 'SUSANA SILVIA', 'ACQUAVELLA', 4868944, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(150, 'LUCIANO DANIEL', 'ACROGLIANO', 29502549, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(151, 'CLEMENTINA FILOMENA', 'ACU?A', 3940741, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(152, 'DELINA DEL TRANSITO', 'ACU?A', 4677439, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(153, 'RAMON HILARIO', 'ACU?A', 7374738, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(154, 'CARLOS ALBERTO', 'ACU?A', 7592215, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(155, 'MARIA LIDIA', 'ACU?A', 10196505, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(156, 'ELSA INES', 'ACU?A', 16335598, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(157, 'GRACIELA MABEL', 'ACU?A', 20430897, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(158, 'DANIEL', 'ACU?A', 20840888, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(159, 'ALICIA', 'ACU?A', 22005191, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(160, 'GABRIEL ALEJANDRO', 'ACU?A', 24306146, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(161, 'SABINA EDITH', 'ACU?A', 27259310, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(162, 'ARNALDO VICTOR', 'ACU?A', 27681707, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(163, 'FLAVIA ROMINA', 'ACU?A', 32031552, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(164, 'MARIA TERESA', 'ACURSO', 14231586, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(165, 'DANIELA SOL', 'ACURSO', 31662282, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(166, 'MARIA', 'ADAD', 1360867, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(167, 'LILIANA ROSA', 'ADAMOLI', 12087010, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(168, 'JUAN ANTONIO', 'ADANO', 8316844, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(169, 'DIEGO SEBASTIAN', 'ADANO', 25478499, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(170, 'NILDA ESTER', 'ADARO', 10717117, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(171, 'MARIA VALERIA', 'ADARO', 24004770, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(172, 'MARCELO DANIEL', 'ADDUCI', 24014343, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(173, 'EDUARDO DOMINGO', 'ADER', 7616242, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(174, 'NELIDA ISABEL', 'ADET', 25769, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(175, 'MIGUEL ANGEL', 'ADHEMAR', 25999175, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(176, 'MARTA S', 'ADILARDI', 4461380, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(177, 'ROBERTO OSCAR', 'ADINOLFI', 4160909, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(178, 'ESTER', 'ADJIMANN', 3697650, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(179, 'ANDRES HERNANDO', 'ADORNO', 21477794, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(180, 'HUGO ALBERTO', 'ADRIANO', 4158410, '', 0, '', 0, '', '0', '', 0, 12, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(181, 'RICARDO', 'ADRIAZOLA', 12045820, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(182, 'GERARDO', 'ADROGUE', 17889464, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(183, 'ANGELICA ARACELI', 'AFFRANCHINO', 3568508, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(184, 'ARIEL MAXIMILIANO', 'AFONSO', 26553255, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(185, 'LIDIA MARIA', 'AGATI', 11104681, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(186, 'STELLA MARIS', 'AGGIO', 11726102, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(187, 'MARIANO', 'AGHAZARIAN', 34842218, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(188, 'MARCELO ALEJANDRO ARIEL', 'AGORRECA', 24448170, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(189, 'ELBA NOEMI', 'AGOSTINELLI', 2939422, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(190, 'HAYDEE', 'AGOZZINO', 2951524, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(191, 'EMILCE MARIA', 'AGRA', 3973337, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(192, 'ROSA VIOLETA', 'AGRANATI', 3756003, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(193, 'NATALI VANESA', 'AGREDA RAYMONDI', 29903866, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(194, 'NORBERTO MARIO', 'AGRO', 17499242, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(195, 'ERMINIA', 'AGUADE', 232207, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(196, 'EUSEBIO', 'AGUADO', 4511912, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(197, 'MARIA TERESA', 'AGUAYO', 10387004, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(198, 'JOAQUIN DANIEL', 'AGUDELO', 31873233, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(199, 'MAGDALENA DEL ROSARIO', 'AGUERO', 2434869, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(200, 'MANUEL', 'AGUERO', 5579932, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(201, 'NELLI MARTA', 'AGUERO', 6297807, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(202, 'EMILIO ISIDORO', 'AGUERO', 8493130, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(203, 'OLGA FRANCISCA', 'AGUERO', 10463369, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(204, 'NILDA ESTHER', 'AGUERO', 10999308, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(205, 'MARTA BEATRIZ', 'AGUERO', 13092496, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(206, 'EDGARDO MAURICIO', 'AGUERO', 13141800, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(207, 'ROBERTO DANIEL', 'AGUERO', 17228145, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(208, 'JUANA NORMA', 'AGUERO', 17618028, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(209, 'DANIEL OSCAR', 'AGUERO', 17873884, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(210, 'FABIAN FERNANDO', 'AGUERO', 21313092, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(211, 'JUANA BAUTISTA', 'AGUERO', 24427940, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(212, 'GUSTAVO RAUL', 'AGUERO', 24848543, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(213, 'JORGE MARIO', 'AGUERO', 27202715, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(214, 'NATALIA LORENA', 'AGUERRE', 24755215, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(215, 'DIEGO JAVIER', 'AGUERRE', 25866393, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(216, 'NOELIA SILVINA', 'AGUERRIBERRY', 27289431, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(217, 'BLANCA ARGENTINA', 'AGUERRIDO', 9882059, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(218, 'RAMON ALEJO', 'AGUIAR', 12496151, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(219, 'SOFIA', 'AGUIERREZ', 10005306, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(220, 'AIDA C', 'AGUILAR', 2636861, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(221, 'EMMA HILARIA', 'AGUILAR', 3469775, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(222, 'MARIA DEL VALLE', 'AGUILAR', 4845671, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(223, 'OFELIA BERNARDINA', 'AGUILAR', 5035292, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(224, 'DORA ROSA', 'AGUILAR', 10735753, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(225, 'EDMUNDO DAVID', 'AGUILAR', 11908454, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(226, 'JUAN PEDRO', 'AGUILAR', 13280267, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(227, 'ALDO NESTOR', 'AGUILAR', 14495040, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(228, 'PAMELA', 'AGUILAR', 17507916, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(229, 'RICARDO JAVIER', 'AGUILAR', 20187595, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(230, 'PATRICIA CLAUDIA', 'AGUILAR', 24228766, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(231, 'CARINA SOLEDAD', 'AGUILAR', 25652858, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(232, 'BEATRIZ PETRONA', 'AGUILAR', 26439497, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(233, 'VANESA CAROLINA', 'AGUILAR', 28751880, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(234, 'RAQUEL ANTONIA', 'AGUILAR', 28756362, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(235, 'ANALIA ROMINA', 'AGUILAR', 31525622, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(236, 'PABLO DANILO', 'AGUILERA', 7697072, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(237, 'DEOLINDA ANGELA', 'AGUILERA', 20131957, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(238, 'LAURA MARISA', 'AGUILERA', 26933079, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(239, 'MARIA INES', 'AGUILO', 16580637, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(240, 'CAROLINA NELIDA E', 'AGUILO', 22430074, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(241, 'GABRIELA JOSEFA', 'AGUIRRE', 503412, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(242, 'BLANCA AURORA', 'AGUIRRE', 2275067, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(243, 'ANA FELIPA', 'AGUIRRE', 3212701, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(244, 'AMANDA JUSTINA', 'AGUIRRE', 3404495, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(245, 'ADELA SATURNINA', 'AGUIRRE', 4776917, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(246, 'ANTONIA CELSA', 'AGUIRRE', 5087279, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(247, 'MARIA CONCEPCION', 'AGUIRRE', 6357759, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(248, 'RAUL VICENTE', 'AGUIRRE', 6379196, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(249, 'ELVECIA', 'AGUIRRE', 6477611, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(250, 'RAMON GREGORIO', 'AGUIRRE', 7377289, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(251, 'MARIO SEGUNDO', 'AGUIRRE', 7918983, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(252, 'JUAN JOSE', 'AGUIRRE', 12137714, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(253, 'MANUEL DOMINGO', 'AGUIRRE', 12784372, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(254, 'CELIA BEATRIZ', 'AGUIRRE', 13178348, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(255, 'OMAR OSCAR', 'AGUIRRE', 13229163, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(256, 'VENTURA JOSE LUIS', 'AGUIRRE', 13955891, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(257, 'MARIA INES', 'AGUIRRE', 16635094, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(258, 'EDUARDO OSCAR', 'AGUIRRE', 17331411, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(259, 'JULIA DEL CARMEN NOEMI', 'AGUIRRE', 21914499, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(260, 'MARIA LAURA', 'AGUIRRE', 22825800, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(261, 'JUANA PATRICIA', 'AGUIRRE', 24005751, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(262, 'MATILDE TERESA', 'AGUIRRE', 24469334, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(263, 'LEONOR BEATRIZ', 'AGUIRRE', 24804340, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(264, 'ROMINA JUANA', 'AGUIRRE', 27182881, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(265, 'WALTER DAVID', 'AGUIRRE', 28418296, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(266, 'OLGA BEATRIZ', 'AGUIRRE', 28555089, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(267, 'JORGE EDUARDO', 'AGUIRRE', 29956799, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(268, 'JULIA BEATRIZ', 'AGUIRRE', 30355979, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(269, 'CLAUDIA LORELEY', 'AGUIRRE', 32798605, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(270, 'ANAHI MELINA', 'AGUIRRE', 32798606, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(271, 'SOLEDAD', 'AGUIRRE', 33157631, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(272, 'ANDREA VIVIANA', 'AGUIRRE', 35376335, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(273, 'CLAIRE EDER', 'AGUIRREZABALA', 1086673, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(274, 'GABRIELA NATALIA', 'AGUSTINI', 23628842, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(275, 'MARIA ESTELA', 'AHIARELLI', 13480334, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(276, 'BLANCA SUSANA', 'AHMED', 4770198, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(277, 'LEONOR SERAFINA', 'AHUMADA', 9990283, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(278, 'AMALIA DIANA', 'AIASSA', 10831435, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(279, 'ESTHER FRANCISCA', 'AICARDI', 314980, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(280, 'AMERICA', 'AIELLO', 490630, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(281, 'BEATRIZ ELBA', 'AIELLO', 11056621, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(282, 'IVANNA FLORENCIA', 'AIELLO', 27942450, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(283, 'ADRIANA MIRIAM', 'AIMAN', 12945021, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(284, 'IRMA CLOTILDE', 'AIME', 6034196, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(285, 'LUIS ANTONIO', 'AIRA FERNANDEZ', 20372831, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(286, 'JAVIER WILFREDO', 'AIRE', 22252148, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(287, 'NANCY ESTER', 'AIRE', 36919076, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(288, 'BEATRIZ IRENE', 'AISEMBERG', 178699, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(289, 'LIBRADA RAMONA', 'AIVA', 20394863, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(290, 'ADRIANA MABEL', 'AIZMAN', 16891176, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(291, 'MIGUEL ANGEL', 'AIZPIRI', 27627408, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(292, 'GABRIEL HERNAN', 'AJA', 22046220, '', 0, '', 0, '', '0', '', 0, 10, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(293, 'MARTIN LUIS', 'AJA', 22878259, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(294, 'MARISA LIDIA', 'AJA', 25890454, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(295, 'CRISTIAN ARIEL', 'AJA', 29564695, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(296, 'CRISTIAN FRANCO', 'AJALLA', 28250853, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(297, 'JUAN MANUEL', 'AKEL', 23469956, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(298, 'ROSALIA ELSA', 'ALABASTER', 4627972, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(299, 'DAMIAN EZEQUIEL', 'ALADRO', 34705097, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(300, 'ROLANDO IRENEO', 'ALAGASTIN', 14203654, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(301, 'RAMON EDUARDO', 'ALAGASTINO', 13189006, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(302, 'JOSE EDUARDO', 'ALAGASTINO', 24967374, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(303, 'NOELIA VANESA', 'ALAGIA', 31289610, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(304, 'MATIAS ELIAS', 'ALALOUF', 4255618, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(305, 'LEON RICARDO', 'ALALOUF', 4271603, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(306, 'LUIS ENRIQUE', 'ALALUF', 17577841, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(307, 'GONZALO', 'ALAMA', 36608087, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(308, 'NILDA ROSA', 'ALAMEDA', 11481255, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(309, 'VILMA CECILIA', 'ALAMO', 4276522, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(310, 'NORMA ISABEL', 'ALAMO', 11651157, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(311, 'SERGIO VICENTE', 'ALANCAY', 25954653, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(312, 'ZENOBIA MARTHA', 'ALANIS', 4604461, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(313, 'MARCELO ANTONIO', 'ALANIS', 22878976, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(314, 'JOSE ALFREDO', 'ALANIZ', 12660697, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(315, 'MIRIAM ESTHER', 'ALANIZ', 16361109, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(316, 'DIANA INES', 'ALANIZ', 22581691, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(317, 'JONATHAN JAVIER', 'ALANIZ', 32636200, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(318, 'SARA', 'ALARCON', 289257, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(319, 'ROBUSTIANA LUISA', 'ALARCON', 2772388, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(320, 'TOMASA OLGA', 'ALARCON', 3542259, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(321, 'BLAS TOMAS', 'ALARCON', 4477370, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(322, 'GRACIELA MIRTA', 'ALARCON', 10840663, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(323, 'MARIO DAVID', 'ALARCON', 11992655, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(324, 'ELIZABETH SANDRA', 'ALARCON', 17657845, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(325, 'HECTOR JUAN', 'ALARCON', 18390562, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(326, 'VANINA FLORENCIA', 'ALARCON', 25070319, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(327, 'LORENA VANESA', 'ALARCON', 29523141, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(328, 'DAMIAN RICARDO', 'ALARCON ADUVIRI', 34358614, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(329, 'JOE FELIX', 'ALARCON JULCA', 18780143, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(330, 'SILVIA FLOR', 'ALAYO MELENDEZ', 18892186, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(331, 'SALOMON DAVID', 'ALAZRAKI', 7638036, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(332, 'ARIEL ALEJANDRO ANTONIO', 'ALBA', 10132257, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(333, 'TERESA', 'ALBA', 11675310, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(334, 'PABLO MARTIN', 'ALBANESE', 24963706, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(335, 'VICTORIA MARIA', 'ALBANO', 2141238, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(336, 'MABEL ELENA', 'ALBANO', 3770119, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(337, 'SERGIO', 'ALBANO', 11062553, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(338, 'FERNANDO CESAR', 'ALBANO', 24871907, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(339, 'ELBA CATALINA', 'ALBAREZ', 4839330, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(340, 'DANTE ISIDRO', 'ALBARRACIN', 7851617, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(341, 'ROSA CRISTINA', 'ALBARRACIN', 10300113, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(342, 'CARLOS JAVIER', 'ALBARRACIN', 22472845, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(343, 'IVANNA LILIANA', 'ALBARRACIN', 28905249, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(344, 'IRMA HAYDEE', 'ALBE', 3222193, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(345, 'MATIAS ERNESTO', 'ALBERGHINA', 25641365, '', 0, '', 0, '', '0', '', 0, 12, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(346, 'OSCAR EDUARDO', 'ALBERGO', 11958370, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(347, 'HAYDEE NELIDA', 'ALBERICI', 4455245, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(348, 'VERONICA CELIA', 'ALBERINI', 17448311, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(349, 'JUAN BALTASAR', 'ALBERINO', 7374151, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(350, 'ROMAN PEDRO', 'ALBERIO', 11987610, '', 0, '', 0, '', '0', '', 0, 12, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10);
INSERT INTO `affiliates` (`id_affiliate`, `names_affiliate`, `last_names_affiliate`, `dni_affiliate`, `street_affiliate`, `number_affiliate`, `dtto_affiliate`, `postal_code_affiliate`, `job_affiliate`, `studies_affiliate`, `denomination_affiliate`, `circuit_affiliate`, `commune_affiliate`, `gener_affiliate`, `age_affiliate`, `facebook_affiliate`, `twitter_affiliate`, `instagram_affiliate`, `email_affiliate`, `phone_1_affiliate`, `phone_2_affiliate`, `checked_phone`, `mobile_1_affiliate`, `mobile_2_affiliate`, `mobile_3_affiliate`, `mobile_4_affiliate`, `mobile_5_affiliate`, `mobile_6_affiliate`, `comments_affiliate`, `status_affiliate`) VALUES
(351, 'CARLOS DANIEL', 'ALBERIO', 13430021, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(352, 'TRISTAN ANIBAL', 'ALBERTI', 4417104, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(353, 'DANIEL', 'ALBERTO', 28905378, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(354, 'MARTA ESTHER', 'ALBERTOLLI', 1009310, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(355, 'DARIO ELOY', 'ALBERTONI', 28324899, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(356, 'JUAN PABLO', 'ALBERTOTTI', 24053385, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(357, 'ELVA DOLORES', 'ALBINO', 8781532, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(358, 'ALICIA SILVIA', 'ALBINO', 20226305, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(359, 'JULIO LUIS', 'ALBONICO', 5841072, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(360, 'HORACIO N', 'ALBORNOZ', 4539240, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(361, 'JOSEFA DEL VALLE', 'ALBORNOZ', 11454611, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(362, 'DANIEL CARLOS', 'ALBORNOZ', 12890100, '', 0, '', 0, '', '0', '', 0, 12, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(363, 'SILVIA LILIANA', 'ALBORNOZ', 14822486, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(364, 'PATRICIA CLAUDIA', 'ALBORNOZ', 14901083, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(365, 'AMALIA TERESA', 'ALBORNOZ', 16106582, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(366, 'ALEJANDRA LILIANA', 'ALBORNOZ', 17063837, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(367, 'MONICA LILIANA', 'ALBORNOZ', 21615614, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(368, 'CARLOS MARIO', 'ALBORNOZ', 23798300, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(369, 'FRANCO RAMON', 'ALBORNOZ', 27721439, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(370, 'MARIA CAROLINA', 'ALBORNOZ COSTOPULOS', 34438242, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(371, 'HIPOLITO JUAN', 'ALBRECHT', 4266085, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(372, 'CASTISINA ELENA', 'ALBRIGI', 4300877, '', 0, '', 0, '', '0', '', 0, 13, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(373, 'CANDIDA ESTHER', 'ALBUQUERQUE', 9976144, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(374, 'MARTIN IGNACIO', 'ALCAIDE IAFFEI', 23644121, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(375, 'MARIA INES', 'ALCAIZ', 16603908, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(376, 'JUANA ERNESTINA', 'ALCANTARA', 115676, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(377, 'ANIBAL FEDERICO JUAN', 'ALCA?IZ', 4485834, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(378, 'FELIPA ALCIRA', 'ALCARAZ', 4526928, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(379, 'EDUARDO NOLASCO', 'ALCARAZ', 5832862, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(380, 'JULIA ESTER', 'ALCARAZ', 11121929, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(381, 'ADELMA', 'ALCARAZ', 14029713, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(382, 'MARIA CRISTINA', 'ALCARAZ', 14070764, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(383, 'SILVANA VERONICA', 'ALCARAZ', 23428391, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(384, 'CAROLINA VERONICA', 'ALCARAZ', 24113005, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(385, 'ANA MARIA', 'ALCARAZ', 24582372, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(386, 'MARIA CONSTANZA', 'ALCARO', 22825959, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(387, 'ROSA PAULINA', 'ALCAYAGA', 2472154, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(388, 'ROSA MARIA', 'ALCAYAGA', 11247076, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(389, 'SILVIA MARIANA', 'ALCOCER', 23864025, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(390, 'CRISTIAN MARCELO', 'ALCOCER', 26898424, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(391, 'WENCESLAO', 'ALCOCER TORRES', 12572911, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(392, 'MARICEL', 'ALDABE', 13404756, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(393, 'ROBERTO', 'ALDAMA', 8389245, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(394, 'HERMENEGILDA', 'ALDANA', 1131584, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(395, 'MARIA ISABEL', 'ALDECOA', 3751369, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(396, 'NOEMI CELINA', 'ALDERETE', 4890668, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(397, 'STELLA MARIS', 'ALDERETE', 12269478, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(398, 'LORENZO YGNACIO', 'ALDERETE', 13391981, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(399, 'MANUEL DEL ROSARIO', 'ALDERETE', 20158296, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(400, 'MARIA PIA', 'ALECCI GALDOS', 25909175, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(401, 'HORTENCIA', 'ALEGRE', 3859923, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(402, 'ELENA', 'ALEGRE', 4444895, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(403, 'NIEVE ZUNILDA', 'ALEGRE', 4474280, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(404, 'ESTELA GLORIA', 'ALEGRE', 5000713, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(405, 'CARLOS FRANCISCO', 'ALEGRE', 11808706, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(406, 'AMADA ROSA', 'ALEGRE', 12263892, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(407, 'DOMINGA MABEL', 'ALEGRE', 14238656, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(408, 'MYRIAM EDITH', 'ALEGRE', 22781871, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(409, 'ROBERTO OSCAR', 'ALEGRE', 26653041, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(410, 'GLADYS NAZARENA', 'ALEGRE', 27288172, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(411, 'ANA MARIA', 'ALEGRO', 5001513, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(412, 'ROMINA ANDREA', 'ALEJANDRIA', 29592242, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(413, 'ANGELICA RAQUEL', 'ALEJO', 21323855, '', 0, '', 0, '', '0', '', 0, 13, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(414, 'ROSA ALEJANDRA', 'ALEJO', 23868896, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(415, 'NOEMI MARINA', 'ALEMAN', 4549556, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(416, 'MIRIAM BEATRIZ', 'ALEMAN', 13237508, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(417, 'MARIA AURELIA', 'ALENCZYK', 14380162, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(418, 'LAURA VALERIA', 'ALENNY', 22493429, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(419, 'CANDELARIO', 'ALFARO', 11334079, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(420, 'MARIA CRISTINA', 'ALFARO', 16300395, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(421, 'NORMA BEATRIZ', 'ALFARO', 17128817, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(422, 'MARIA ROSA', 'ALFARO', 20440036, '', 0, '', 0, '', '0', '', 0, 13, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(423, 'LORENA BEATRIZ', 'ALFARO', 34725560, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(424, 'EMILIO', 'ALFIE', 11824795, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(425, 'ELIDA DELMIRA', 'ALFONSO', 1712560, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(426, 'BLANCA BEATRIZ', 'ALFONSO', 6353852, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(427, 'MARTHA RAQUEL', 'ALFONZO', 3013971, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(428, 'ZULEMA MIRTA', 'ALFONZO', 18298079, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(429, 'FELIX', 'ALGARBE', 4216624, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(430, 'EDUARDO JOSE', 'ALGAZI', 13403096, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(431, 'MARTIN PEDRO', 'ALHINHO MARTINS DE OLIVEIRA', 25704866, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(432, 'NURA', 'ALI', 1297334, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(433, 'OMAR PEDRO', 'ALI', 14010553, '', 0, '', 0, '', '0', '', 0, 12, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(434, 'SANDRA FABIANA', 'ALI', 18005494, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(435, 'CLAUDIA ROXANA', 'ALI', 21951814, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(436, 'AZUCENA ESTER', 'ALIATA', 5138410, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(437, 'HORACIO FERNANDO', 'ALIATA', 8503796, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(438, 'VICTORIA SOFIA', 'ALICE', 4277989, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(439, 'ANA MARIA', 'ALIMENA', 11178768, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(440, 'JORGE LUIS', 'ALIO', 18152060, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(441, 'ANIBAL DARIO', 'ALIOTTA', 29250151, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(442, 'HILDA ANA', 'ALLEGRI', 23908184, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(443, 'ROSALIA GRISELDA', 'ALLEGRI', 27635269, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(444, 'AUGUSTO MARCELO', 'ALLEMAND', 16209891, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(445, 'ROCIO JAZMIN', 'ALLEMAND', 31013238, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(446, 'MAURO FEDERICO', 'ALLENAND', 30275407, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(447, 'JOSE LUIS', 'ALLENDE', 14033971, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(448, 'PATRICIA MABEL', 'ALLO', 14195953, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(449, 'LUIS ALBERTO', 'ALLOIS', 6036902, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(450, 'HECTOR JOSE', 'ALMADA', 4303442, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(451, 'MERCEDES ALICIA', 'ALMADA', 4635246, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(452, 'RUBENS', 'ALMADA', 6231180, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(453, 'DARIO LEONARDO', 'ALMADA', 7986787, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(454, 'LAURA', 'ALMADA', 13191626, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(455, 'ROMINA MERCEDES', 'ALMADA', 26194388, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(456, 'ILEANA BEATRIZ', 'ALMANDOZ', 2708802, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(457, 'LEONARDO MATIAS', 'ALMANZA', 26936155, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(458, 'ALICIA ESTER', 'ALMARAZ', 6293568, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(459, 'AMANDA DELIA', 'ALMARAZ', 10574423, '', 0, '', 0, '', '0', '', 0, 13, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(460, 'ALBERTO HECTOR', 'ALMARZA', 4977182, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 2),
(461, 'RICARDO GUSTAVO', 'ALMAZA', 25060181, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(462, 'LUCIA DEL CARMEN', 'ALMEIDA', 11429294, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(463, 'JORGE LUIS', 'ALMEIDA', 16281138, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(464, 'ISABEL LEONOR', 'ALMEIDA', 24459065, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(465, 'JUAN OSCAR', 'ALMENDRAS', 21815772, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(466, 'JOSE LUIS', 'ALMENDRAS', 22456989, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(467, 'NELIDA SUSANA', 'ALMIRON', 5984245, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(468, 'FRANCISCO LA CRUZ', 'ALMIRON', 13028789, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(469, 'LUISA', 'ALMIRON', 16270928, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(470, 'MONICA BEATRIZ', 'ALMIRON', 18195207, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(471, 'JUAN CARLOS', 'ALMIRON', 27712987, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(472, 'SEBASTIAN DANIEL', 'ALMIRON', 30711808, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(473, 'YOLANDA ELIZABETH', 'ALMIRON', 32340848, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(474, 'VANESA CAROL', 'ALOCCO', 21465100, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(475, 'MATIAS DAMIAN', 'ALOCCO', 23838380, '', 0, '', 0, '', '0', '', 0, 10, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(476, 'DIEGO GABRIEL', 'ALOISO', 24112682, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(477, 'FANNY NELIDA', 'ALONSO', 2686902, '', 0, '', 0, '', '0', '', 0, 13, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(478, 'CIRIACA CARMEN', 'ALONSO', 3044524, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(479, 'ROSA OLGA', 'ALONSO', 3185332, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(480, 'ALICIA', 'ALONSO', 3204707, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(481, 'OLGA E', 'ALONSO', 3211222, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(482, 'CARMEN ALICIA', 'ALONSO', 3972816, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(483, 'ALICIA BEATRIZ', 'ALONSO', 4074567, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(484, 'ALBERTO', 'ALONSO', 4155261, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(485, 'NORBERTO', 'ALONSO', 4293736, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(486, 'JULIO ALBERTO', 'ALONSO', 4499571, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(487, 'RUBEN DARIO', 'ALONSO', 5214420, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(488, 'MARIA HAYDEE', 'ALONSO', 5249994, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(489, 'MARTA SILVIA', 'ALONSO', 5327082, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(490, 'JORGE GUILLERMO', 'ALONSO', 12501693, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(491, 'MARIA ANTONIA', 'ALONSO', 12547003, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(492, 'BERNARDINA', 'ALONSO', 18712290, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(493, 'KARINA CINTHIA', 'ALONSO', 21465950, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(494, 'CLAUDIA ESTHER', 'ALONSO', 22813288, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(495, 'PABLO DANIEL', 'ALONSO', 22885997, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(496, 'GUADALUPE', 'ALONSO', 24821927, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(497, 'CAROLINA VALERIA', 'ALONSO', 27942590, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(498, 'MARIA FERNANDA', 'ALONSO', 28766105, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(499, 'FLORENCIA BELEN', 'ALONSO', 37246658, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(500, 'ELIANA BELEN', 'ALONSO', 38787074, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(501, 'MARIA CONSTANCIA E', 'ALONSO NAVARRO', 24876799, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(502, 'FERNANDO DANIEL', 'ALONSO VENIER', 25316819, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(503, 'MARIA ELENA', 'ALONZO', 12766862, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(504, 'ABEL', 'ALSIDES', 18803089, 'Principal', 23, 'Principal', 1201, 'desarrollador', 'Universitarios', '', 12, 78, 'Masculino', 27, 'hemm18', 'hemm_18_', 'hemm18', 'martinezhenry18@gmail.com', 4248559520, 4125816392, 0, 0, 0, 0, 0, 0, 0, '', 7),
(505, 'GUILLERMO ADRIAN', 'ALSINA', 17374655, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(506, 'NORBERTO PABLO', 'ALTADONNA', 4201173, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(507, 'JOSE LUIS', 'ALTAMIRA', 14492655, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(508, 'ANTONIA LIMBANIA', 'ALTAMIRANDA', 4167616, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(509, 'VICENTE', 'ALTAMIRANDA', 14285383, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(510, 'JOSE ANCELMO', 'ALTAMIRANDA', 20152009, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(511, 'JUANA GEORGINA', 'ALTAMIRANO', 256823, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(512, 'FLORA ANTONIA', 'ALTAMIRANO', 1438791, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(513, 'AZUCENA MARIA', 'ALTAMIRANO', 5854178, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(514, 'EDUARDO MANUEL', 'ALTAMIRANO', 11650021, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(515, 'EMILIA LUISA', 'ALTAMIRANO', 16780922, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(516, 'MARCELA OLGA', 'ALTAMIRANO', 21532624, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(517, 'PAULA BERENICE', 'ALTAYRAC', 26257508, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(518, 'GRACIELA ROSA', 'ALTERMAN', 1828515, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(519, 'EDGARDO RUBEN', 'ALTHABE', 11237158, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(520, 'SANDRA', 'ALTI', 22502146, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(521, 'NORMA CRISTINA', 'ALTIERI', 3971276, '', 0, '', 0, '', '0', '', 0, 13, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(522, 'CECILIA ANA', 'ALTMANN', 267596, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(523, 'CONCEPCION ANA', 'ALTOBELLI', 1023858, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(524, 'COSME ALFREDO', 'ALTOMARI', 5582087, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(525, 'GRACIELA MARTA', 'ALTOMONTE', 16497328, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(526, 'CELIA FRANCISCA', 'ALTUNA', 3017056, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(527, 'AURORA MABEL', 'ALTUNA', 11629921, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(528, 'DANIEL RODOLFO', 'ALVARADO', 26637028, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(529, 'CLAUDIO MARTIN', 'ALVARADO', 28440673, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(530, 'NELLY BEATRIZ', 'ALVAREDO', 507559, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(531, 'ELPIDIO MANUEL', 'ALVARENGA', 18046505, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(532, 'CARMEN SUSANA', 'ALVARENGA', 25747282, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(533, 'CATALINA MARGARITA', 'ALVAREZ', 39456, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(534, 'MARTA ELENA', 'ALVAREZ', 41737, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(535, 'MARIA ZULEMA ANGELICA', 'ALVAREZ', 50077, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(536, 'ELVIRA', 'ALVAREZ', 271736, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(537, 'LUCIA AIDA', 'ALVAREZ', 282148, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(538, 'ALICIA ERMINDA', 'ALVAREZ', 1302441, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(539, 'MARIA HAYDEE', 'ALVAREZ', 1797918, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(540, 'ELENA HAYDEE', 'ALVAREZ', 2319550, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(541, 'VIRGINIA LYDIA M', 'ALVAREZ', 2927658, '', 0, '', 0, '', '0', '', 0, 13, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(542, 'NELIDA LEONOR', 'ALVAREZ', 2951230, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(543, 'FELISA A', 'ALVAREZ', 3078407, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(544, 'HAYDEE TERESA', 'ALVAREZ', 3550472, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(545, 'RUBEN OMAR', 'ALVAREZ', 4126734, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(546, 'LITA MARGOT', 'ALVAREZ', 4228677, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(547, 'ALBERTO HORACIO', 'ALVAREZ', 4241660, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(548, 'EDUARDO NICOLAS', 'ALVAREZ', 4493530, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(549, 'JUANA OFELIA', 'ALVAREZ', 4678809, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(550, 'ALFREDO RAFAEL', 'ALVAREZ', 4704857, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(551, 'ANA MARIA ISABEL VICTORIA', 'ALVAREZ', 4951728, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(552, 'BERNARDINA', 'ALVAREZ', 5383127, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(553, 'FAUSTINO HECTOR', 'ALVAREZ', 5407462, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(554, 'OSCAR', 'ALVAREZ', 5587901, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(555, 'ELSA CLARA', 'ALVAREZ', 5724745, '', 0, '', 0, '', '0', '', 0, 13, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(556, 'ALICIA ESTELA', 'ALVAREZ', 5862032, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(557, 'GRACIELA SUSANA', 'ALVAREZ', 5868120, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(558, 'LETICIA NORMA', 'ALVAREZ', 6154258, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(559, 'MARIA CRISTINA', 'ALVAREZ', 6221416, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(560, 'MARIA CRISTINA', 'ALVAREZ', 6386826, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(561, 'MANGUDO NYLES ZULMA', 'ALVAREZ', 7322241, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(562, 'ABEL MARTIN', 'ALVAREZ', 7638536, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(563, 'MARIA DALINDA', 'ALVAREZ', 8781789, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(564, 'MARIA CRISTINA', 'ALVAREZ', 10473320, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(565, 'RAFAEL ALFREDO', 'ALVAREZ', 10593842, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(566, 'HAYDEE', 'ALVAREZ', 10996650, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(567, 'ESTELA BENIGNA', 'ALVAREZ', 11231005, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(568, 'MARIA TERESA', 'ALVAREZ', 11369113, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(569, 'CARLOS DANIEL', 'ALVAREZ', 11376814, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(570, 'DANIEL MATIAS', 'ALVAREZ', 11624924, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(571, 'PABLO ROLANDO', 'ALVAREZ', 12086436, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(572, 'IRMA GLADYS', 'ALVAREZ', 12125828, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(573, 'ARTURO ROBERTO', 'ALVAREZ', 12606526, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(574, 'MERCEDES DEL TRANSITO', 'ALVAREZ', 12921966, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(575, 'MARIA ERCILIA', 'ALVAREZ', 13133944, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(576, 'NOEMI BEATRIZ', 'ALVAREZ', 13622791, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(577, 'GUILLERMO HORACIO', 'ALVAREZ', 13808964, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(578, 'SILVIA GRACIELA', 'ALVAREZ', 13982321, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(579, 'ANTONIA MARGARITA', 'ALVAREZ', 14391423, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(580, 'MONICA LIDIA', 'ALVAREZ', 14578066, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(581, 'ADOLFO JOSE', 'ALVAREZ', 14820478, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(582, 'AMANDA SILVIA', 'ALVAREZ', 14888995, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(583, 'FABIAN SANTIAGO', 'ALVAREZ', 14959104, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(584, 'CLAUDIA ROSA', 'ALVAREZ', 16161552, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(585, 'ADRIANA GRACIELA', 'ALVAREZ', 16776453, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(586, 'JULIO CESAR', 'ALVAREZ', 16866916, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(587, 'FABIAN HUGO', 'ALVAREZ', 17577688, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(588, 'ANA SILVIA', 'ALVAREZ', 17739309, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(589, 'ALBERTA TERESA', 'ALVAREZ', 17782395, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(590, 'GLADYS CATALINA', 'ALVAREZ', 17816408, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(591, 'ALCIRA', 'ALVAREZ', 18296804, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(592, 'ALFREDO HECTOR', 'ALVAREZ', 18473016, '', 0, '', 0, '', '0', '', 0, 10, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(593, 'DANIELA FABIANA', 'ALVAREZ', 21058960, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(594, 'ANDREA FABIANA', 'ALVAREZ', 21485182, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(595, 'RAMONA AMALIA', 'ALVAREZ', 21934987, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(596, 'LORENA GABRIELA', 'ALVAREZ', 22570214, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(597, 'LUIS MARIO', 'ALVAREZ', 22631496, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(598, 'ANA MARIA', 'ALVAREZ', 22644527, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(599, 'SUSANA', 'ALVAREZ', 23222557, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(600, 'DIEGO ARIEL', 'ALVAREZ', 23833274, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(601, 'LORENA ELIZABETH', 'ALVAREZ', 24994304, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(602, 'ALEJO', 'ALVAREZ', 25100293, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(603, 'VERONICA FLORENCIA', 'ALVAREZ', 25435298, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(604, 'GONZALO JUAN', 'ALVAREZ', 25440981, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(605, 'GUADALUPE SUSANA', 'ALVAREZ', 26047771, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(606, 'CRISTIAN ERNESTO', 'ALVAREZ', 26257272, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(607, 'ARIEL OSCAR', 'ALVAREZ', 26647563, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(608, 'MARIA EUGENIA', 'ALVAREZ', 27202373, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(609, 'DANIEL DE DIOS', 'ALVAREZ', 27288717, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(610, 'MIGUEL ANGEL DARIO', 'ALVAREZ', 27479720, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(611, 'GUSTAVO JAVIER', 'ALVAREZ', 28120741, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(612, 'MARIANO ALEJANDRO', 'ALVAREZ', 28337062, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(613, 'LAURA MARIELA', 'ALVAREZ', 28418304, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(614, 'FRANCISCO JAVIER', 'ALVAREZ', 29193189, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(615, 'NADINA IRENE', 'ALVAREZ', 29544018, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(616, 'JOSE LUIS', 'ALVAREZ', 29911240, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(617, 'CHRISTIAN GASTON', 'ALVAREZ', 30774557, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(618, 'ELSA NOEMI', 'ALVAREZ', 31965839, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(619, 'ROBERTO ALEJANDRO', 'ALVAREZ', 33306160, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(620, 'MARIO MARTIN', 'ALVAREZ', 36594285, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(621, 'CLARA ROSA', 'ALVAREZ IBARRA', 49537, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(622, 'JULIO PABLO', 'ALVAREZ LENER', 4034085, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(623, 'HERNAN ANDRES', 'ALVAREZ PRIETO', 29521419, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(624, 'JOSE LUIS', 'ALVAREZ VARGAS', 30354286, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(625, 'JOSE DANIEL', 'ALVAREZ VERA', 31171315, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(626, 'ALEJANDRO GABRIEL', 'ALVARI?AS', 16336278, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(627, 'PATRICIO SEBASTIAN', 'ALVARI?AS', 22651464, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(628, 'PAULA ELIZABETH', 'ALVEAR', 37859087, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(629, 'TAMARA', 'ALVES DA SILVA', 34602058, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(630, 'JOSE CARLOS', 'ALVES MOREIRA', 31425007, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(631, 'ARMANDO MANUEL', 'ALVES ROLO', 10423169, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(632, 'GENOVEVA', 'ALVEZ', 9986539, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(633, 'JULIO EMILIO', 'ALVEZ', 29438562, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(634, 'ANTONINO MANUEL', 'ALVILARES LOVELLE', 16822952, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(635, 'AMNERIS ADHELMA', 'ALVIS', 2969954, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(636, 'JUSTINO DANIEL', 'ALVIS', 6151103, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(637, 'ROSA ATILIA', 'ALZAMORA', 12153652, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(638, 'MARIA DE LAS MERCEDES', 'ALZINA', 11220246, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(639, 'JULIO RUBEN', 'ALZOGARAY', 7005550, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(640, 'ANGEL RAUL', 'ALZOGARAY', 11175495, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(641, 'TIMOTEA DIONICIA', 'ALZOGARAY', 14031733, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(642, 'CESAR RAMON', 'ALZOGARAY', 14330734, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(643, 'CARLOS HUMBERTO', 'ALZOGARAY', 17902441, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(644, 'ANA MARIA', 'ALZUGARAY', 11383994, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(645, 'CARLOS ARIEL', 'AMADEO', 16939314, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(646, 'FERMINA ZULEMA', 'AMADO', 3721670, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(647, 'OSCAR RODOLFO', 'AMADO', 7605352, '', 0, '', 0, '', '0', '', 0, 10, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(648, 'MARINA VERONICA', 'AMADO', 23463703, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(649, 'SILVINA ADELA', 'AMADO', 29039977, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(650, 'SILVIA SUSANA', 'AMADORI', 6155270, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(651, 'DORA BEATRIZ', 'AMARAY', 4402222, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(652, 'TEODORA', 'AMARILLA', 14134243, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(653, 'MARICEL', 'AMARILLA', 20348150, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(654, 'HECTOR DANTE', 'AMARILLA', 22733333, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(655, 'MARIA LURDES', 'AMARILLA', 26238572, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(656, 'MIRTA SUSANA', 'AMARO', 14569653, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(657, 'NICOLAS', 'AMARO MONACO', 35537793, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(658, 'CARLOS EDUARDO', 'AMAT', 11643118, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(659, 'JOSE LUIS', 'AMATO', 4120010, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(660, 'ALBERTO', 'AMATO', 4515403, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(661, 'FERNANDO LUIS', 'AMAYA', 11726391, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(662, 'CASILDA MERICIA', 'AMAYA', 12394966, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(663, 'MARIA MERCEDES', 'AMAYA', 20818879, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(664, 'LUCILA VALERIA', 'AMAYA', 26474979, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(665, 'CARLOS MANUEL', 'AMAYA', 28554924, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(666, 'GILDA GABRIELA PATRICIA', 'AMAYA AKKAUY', 22667121, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(667, 'AILIN', 'AMBASCH', 34482912, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(668, 'PATRICIA PILAR', 'AMBRES', 17824987, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(669, 'CARLOS A', 'AMBROS', 4515418, '', 0, '', 0, '', '0', '', 0, 10, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(670, 'JUAN PABLO', 'AMBROS', 27050290, '', 0, '', 0, '', '0', '', 0, 10, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(671, 'RODOLFO PABLO', 'AMBROSANO', 7597096, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(672, 'CARLOS', 'AMBROSI', 6612055, '', 0, '', 0, '', '0', '', 0, 10, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(673, 'MARIA VICTORIA', 'AMDEN', 3194933, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(674, 'HUGO ALBERTO', 'AMEAL', 10464017, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(675, 'MABEL NORMA', 'AMEREY', 5267222, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(676, 'MARIA TERESA', 'AMERI', 3752151, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(677, 'JULIETA', 'AMESSA', 26932626, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(678, 'JORGE ARMANDO', 'AMICARELLI', 4442983, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(679, 'MARIA ISABEL', 'AMIEVA', 6627083, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(680, 'CAROLINA SOLEDAD', 'AMITRANO', 28875805, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(681, 'GRISELDA', 'AMMATURO', 12454352, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(682, 'HAYDEE', 'AMO', 3230991, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(683, 'PABLO CRISTIAN', 'AMODEI', 28508694, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(684, 'RAUL EDUARDO', 'AMORENA', 11848352, '', 0, '', 0, '', '0', '', 0, 10, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(685, 'GUILLERMO DIEGO', 'AMORIN', 16681573, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(686, 'MARIA ESTER', 'AMOROS', 13431040, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(687, 'JORGE EDUARDO', 'AMOROS', 20838539, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(688, 'MARIANA RAQUEL', 'AMOROSO', 20861592, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(689, 'MIRTA BEATRIZ', 'AMORUSO', 4093577, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(690, 'FELIPE', 'AMPUERO', 14873456, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(691, 'BLANCA YOLANDA', 'AMPUERO', 20177629, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(692, 'CARMEN DEL VALLE', 'AMPUERO', 29527509, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(693, 'ALEJANDRA MICAELA DE', 'ANA', 26217466, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(694, 'CARLOS ALBERTO', 'ANABIA', 10740276, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(695, 'ANTONIO PEDRO', 'ANANIA', 4183527, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(696, 'CARLOS ALBERTO', 'ANANIA', 10688306, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(697, 'JOSE LUIS', 'ANASTASIO', 4148528, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(698, 'LEONILDA MIRTA', 'ANASTASIO', 6347278, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10);
INSERT INTO `affiliates` (`id_affiliate`, `names_affiliate`, `last_names_affiliate`, `dni_affiliate`, `street_affiliate`, `number_affiliate`, `dtto_affiliate`, `postal_code_affiliate`, `job_affiliate`, `studies_affiliate`, `denomination_affiliate`, `circuit_affiliate`, `commune_affiliate`, `gener_affiliate`, `age_affiliate`, `facebook_affiliate`, `twitter_affiliate`, `instagram_affiliate`, `email_affiliate`, `phone_1_affiliate`, `phone_2_affiliate`, `checked_phone`, `mobile_1_affiliate`, `mobile_2_affiliate`, `mobile_3_affiliate`, `mobile_4_affiliate`, `mobile_5_affiliate`, `mobile_6_affiliate`, `comments_affiliate`, `status_affiliate`) VALUES
(699, 'ALICIA ELENA', 'ANASTASOPULOS', 5711909, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(700, 'FERNANDO ESTEBAN', 'ANCAROLA PSAROS', 33934903, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(701, 'RUBEN OSCAR', 'ANCHOREN', 23608519, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(702, 'ROMINA MARIANA', 'ANDELIQUE', 25156572, '', 0, '', 0, '', '0', '', 0, 13, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(703, 'CARMEN', 'ANDERSEN', 2754863, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(704, 'EDUARDO ENRIQUE', 'ANDERSON', 4884990, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(705, 'MARGARITA DORA', 'ANDIA', 13799343, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(706, 'MONICA BEATRIZ', 'ANDIA', 17035066, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(707, 'HERNAN RICARDO', 'ANDIA USTARES', 22791389, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(708, 'ANA MARIA', 'ANDOLCETTI', 10412877, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(709, 'NELIDA HAYDEE', 'ANDRADA', 2603638, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(710, 'BERTA ALICIA', 'ANDRADA', 3862617, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(711, 'HUGO', 'ANDRADA', 5070169, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(712, 'ANGELA DEL ROSARIO', 'ANDRADA', 12987731, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(713, 'SANDRA NOEMI', 'ANDRADA', 18589997, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(714, 'CLAUDIO SERGIO', 'ANDRADA', 26865121, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(715, 'CRISTIAN DAMIAN', 'ANDRADA', 29274411, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(716, 'RODOLFO MANUEL', 'ANDRADE', 4176089, '', 0, '', 0, '', '0', '', 0, 10, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(717, 'NELLY', 'ANDRADE', 13828302, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(718, 'SANDRA ELIZABETH', 'ANDRADE', 23413976, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(719, 'LUCIANA MARCIA', 'ANDRADE', 24910122, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(720, 'CLAUDIA ROMINA', 'ANDRADE', 27530191, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(721, 'MARIA FERNANDA', 'ANDRADE', 31465345, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(722, 'BRENDA MALEN', 'ANDRADE', 33366230, '', 0, '', 0, '', '0', '', 0, 13, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(723, 'JOSE LUIS', 'ANDRADE BORGES', 16924530, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(724, 'NORBERTO LUIS', 'ANDRADE DE FARIA', 14611355, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(725, 'ROSA ESTER', 'ANDRAL', 5414334, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(726, 'ADRIANA RAQUEL', 'ANDREETTA', 13515306, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(727, 'LEONOR NORMA', 'ANDRES', 10460347, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(728, 'JOSE', 'ANDREU', 4350023, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(729, 'ANA CAROLINA', 'ANDREU', 28419007, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(730, 'NELIDA SOLEDAD', 'ANDRICH', 28966126, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(731, 'ANA MARIA', 'ANDRIEUX', 3160086, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(732, 'DANIEL FERNANDO', 'ANDRONICO', 12088216, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(733, 'MARISA EDITH', 'ANDUEZA', 28178858, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(734, 'MAGDALENA', 'ANDUEZA', 34027401, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(735, 'TOMAS IGNACIO', 'ANELLO', 31640986, '', 0, '', 0, '', '0', '', 0, 14, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(736, 'RAFAEL EZEQUIEL', 'ANELLO', 33075193, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(737, 'CLAUDIA ALEJANDRA', 'ANESINI', 14996697, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(738, 'VIVIANA ISABEL', 'ANGEL', 23877385, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(739, 'PATRICIA TERESA', 'ANGELELLI', 11383910, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(740, 'MARTA SUSANA', 'ANGELETTI', 12544663, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(741, 'OFELIA VICTORIA', 'ANGELLA', 4523725, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(742, 'ANTONIA LUCIA', 'ANGELUCCI', 5769639, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(743, 'MARIA CARMEN', 'ANGILLETTA', 3735613, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(744, 'NORMA DORA', 'ANGIOLINI', 3290269, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(745, 'ALDO ROBERTO', 'ANGRIGIANI', 4063797, '', 0, '', 0, '', '0', '', 0, 12, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(746, 'ANDREA VERONICA', 'ANIBARRO', 20318813, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(747, 'PAULA GISELLE', 'ANICHINI', 30978542, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(748, 'MARINA', 'ANIDO', 22470476, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(749, 'MARTHA BEATRIZ', 'ANNECCHINI', 3737638, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(750, 'ROSA RAMONA', 'ANRIQUEZ', 3078651, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(751, 'LEONARDO EZEQUIEL', 'ANRIQUEZ', 27308839, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(752, 'DIEGO RICARDO', 'ANRIQUEZ', 30461484, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(753, 'CLEMENTINA', 'ANRRIQUEZ', 13301124, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(754, 'EVELYN DANAE', 'ANSALDO', 37989543, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(755, 'MARIA DE LAS MERCEDES', 'ANSALDO VARGAS', 24823862, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(756, 'MARTHA JULIETA', 'ANSELMI', 2729930, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(757, 'ELENA PURA', 'ANTELO', 13529895, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(758, 'NATALIA MARIA VICTORIA', 'ANTELO', 26337039, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(759, 'FLORENCIA', 'ANTINUCCI', 30218399, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(760, 'MARIA DE LOS ANGELES', 'ANTIQUEIRA', 27535956, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(761, 'BLANCA HILDA', 'ANTISTA', 15046, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(762, 'ALEJANDRA ANAHI', 'ANTIVERO', 37596201, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(763, 'SANTIAGO HERNAN', 'ANTOGNOLLI', 27287250, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(764, 'ARIEL EDGARDO', 'ANTOKOLEC', 18151459, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(765, 'EDELVIS FELIX', 'ANTONELLI', 4479553, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(766, 'VIOLETA MARINA', 'ANTONELLI', 6067384, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(767, 'HUGO NICOLAS', 'ANTONELLI', 12524764, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(768, 'PEDRO ADRIAN', 'ANTONELLI', 30378525, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(769, 'PEDRO JESUS', 'ANTONELLI', 30408988, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(770, 'LAURA', 'ANTONELLI VARELA', 16940247, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(771, 'LAURA IRENE', 'ANTONIAZZO', 4947415, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(772, 'SUSANA', 'ANTONIO', 143925, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(773, 'ANA MARIA', 'ANTONIOLLI', 5425314, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(774, 'ISABEL HERMINIA', 'ANTONUCCIO', 2767520, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(775, 'ERIKA MARA', 'ANTORIANO TAKAGAKY', 26338039, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(776, 'MARTHA ELVIRA ENRIQUETA DE', 'ANTUENO', 4774746, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(777, 'MARIA INES JOSEFINA', 'ANTUNES', 534052, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(778, 'CATALINA DEL CARMEN', 'ANTUNEZ', 4790848, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(779, 'MARIA ANGELICA', 'ANTUNEZ', 14764206, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(780, 'CARMEN', 'ANTU?A', 5214118, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(781, 'ALICIA ANGELA', 'ANTU?A', 9989450, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(782, 'ISABEL MARIA', 'ANUCH', 14884714, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(783, 'LILIANA NOEMI', 'ANZINI', 16975947, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(784, 'NATALIA LORENA', 'ANZIVINO', 26800248, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(785, 'ROBERTO', 'A?EZ PEREZ', 18727949, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(786, 'PAULA GABRIELA', 'AOUADH', 18404981, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(787, 'MARCELA EDITH', 'APA', 25317443, '', 0, '', 0, '', '0', '', 0, 13, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(788, 'MONICA FELICIDAD', 'APARICIO', 14009430, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(789, 'ALBERTO', 'APARICIO', 17657742, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(790, 'GABRIELA', 'APARICIO', 20624159, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(791, 'ROBERTO CARLOS', 'APARICIO', 23435467, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(792, 'ALCIDES REINALDO', 'APARICIO', 26554028, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(793, 'ROMULO OMAR', 'APARICIO', 32088111, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(794, 'CRISTIAN ROBERTO', 'APARO', 22941645, '', 0, '', 0, '', '0', '', 0, 6, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(795, 'DARIO ROBERTO', 'APARO', 22941646, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(796, 'NORMA GRACIELA', 'APAZ', 16497946, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(797, 'ISABEL', 'APAZA', 16468359, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(798, 'MARIA ESTHER', 'APAZA', 28050958, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(799, 'MALENA JUDITH', 'APAZA', 30009180, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(800, 'ANTONIA MARIA', 'APE', 2447618, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(801, 'ROXANA ELIZABETH', 'APE', 23553852, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(802, 'JUAN JOSE', 'APONTE', 10534614, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(803, 'LORENA GABRIELA', 'APONTE', 24823040, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(804, 'JUAN GASTON', 'APONTE', 26966054, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(805, 'BEATRIZ SUSANA', 'APREA', 888558, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(806, 'ELSA BEATRIZ', 'APREDA', 75606, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(807, 'ADRIANA BEATRIZ', 'APUZZO', 16764692, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '<ul><li>Se llamo al cliente y se estableció un primer contacto, quedando asi muy satisfecho con el trato recibido.</li></ul>', 5),
(808, 'LILIANA ARMANDA', 'AQUILA', 10126878, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(809, 'CARLOS', 'AQUILINA', 4214107, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(810, 'ELMA DE JESUS', 'AQUINO', 1465599, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(811, 'GLORIA ESTER', 'AQUINO', 5652221, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(812, 'JUAN DE LA CRUZ', 'AQUINO', 7632824, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(813, 'RAMON ESTEBAN', 'AQUINO', 8186722, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(814, 'ANA MARIA', 'AQUINO', 11080515, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(815, 'LUISA', 'AQUINO', 12827066, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(816, 'JUAN CARLOS', 'AQUINO', 13222514, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(817, 'MARIA FELISA', 'AQUINO', 14063460, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(818, 'MIRTA BEATRIZ', 'AQUINO', 14309717, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(819, 'DELFINA', 'AQUINO', 14995873, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(820, 'SUSANA GRACIELA', 'AQUINO', 16055974, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(821, 'LUIS ALBERTO', 'AQUINO', 16790333, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(822, 'MARY YTATI', 'AQUINO', 17077697, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(823, 'NORBERTO MARTIN', 'AQUINO', 22548794, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(824, 'MIRTA GRACIELA', 'AQUINO', 24025484, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(825, 'LUCIA GUADALUPE', 'AQUINO', 25477776, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(826, 'MARIA LAURA', 'AQUINO', 27577629, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(827, 'GRISELDA SOLEDAD', 'AQUINO', 28992676, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(828, 'PABLO EMANUEL', 'AQUINO', 30354269, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(829, 'DARIO ALEJANDRO', 'AQUINO BAEZ', 28911297, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(830, 'ALICIA SUSANA', 'ARAGONES', 4229077, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(831, 'EDUARDO JULIO', 'ARAMAYO GALEANO', 22269894, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(832, 'SABINA', 'ARAMBULO PAREDES', 18522258, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(833, 'MATIAS ABEL', 'ARAMONI', 33020512, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(834, 'MARIA CRISTINA', 'ARANA', 972148, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(835, 'SALVADOR SANTOS', 'ARANA', 4157113, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(836, 'DANIEL EUGENIO', 'ARANCIBIA', 8511492, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(837, 'GABRIELA VANESA', 'ARANCIBIA', 23584633, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(838, 'RENE WALTER', 'ARANCIBIA LEDESMA', 24882728, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(839, 'DIONICIO ABRAHAM', 'ARANDA', 4284987, '', 0, '', 0, '', '0', '', 0, 5, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(840, 'DELIA ELSA', 'ARANDA', 5619795, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(841, 'CECILIA MARIA', 'ARANDA', 10004693, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(842, 'MARIA LUISA', 'ARANDA', 10292519, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(843, 'BEATRIZ PETRONA', 'ARANDA', 11018622, '', 0, '', 0, '', '0', '', 0, 15, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(844, 'ALBA AIDA', 'ARANDA', 11474156, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(845, 'LUCIA ANGELINA', 'ARANDA', 11919980, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(846, 'ELVIRA ELOISA', 'ARANDA', 12166101, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(847, 'JULIO CESAR', 'ARANDA', 13697822, '', 0, '', 0, '', '0', '', 0, 12, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(848, 'SOFIA ELVIRA', 'ARANDA', 13856083, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(849, 'LUIS ANTONIO', 'ARANDA', 13892774, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(850, 'DEMETRIO DEL VALLE', 'ARANDA', 16884895, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(851, 'LUISA ELENA', 'ARANDA', 17008791, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(852, 'CARLOS SERGIO', 'ARANDA', 17448399, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(853, 'MICAELA DEL VALLE', 'ARANDA', 17457604, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(854, 'SANTOS FABIAN', 'ARANDA', 18582694, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(855, 'ROBERTO MARCELO', 'ARANDA', 21781376, '', 0, '', 0, '', '0', '', 0, 3, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(856, 'MARIA ALEJANDRA', 'ARANDA', 22046293, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(857, 'RAUL ALBERTO', 'ARANDA', 25162703, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(858, 'MIGUEL GUSTAVO', 'ARANDA', 25827862, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(859, 'MARIA DEL CARMEN', 'ARANDA', 26887248, '', 0, '', 0, '', '0', '', 0, 7, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(860, 'LIDIA ELIZABETH', 'ARANDA', 30451891, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(861, 'RITA LEONOR', 'ARANDA', 30977291, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(862, 'YESICA SABRINA', 'ARANDA', 34073248, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(863, 'FEDERICO RODRIGO', 'ARANDA', 34389788, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(864, 'RUBEN ALEJANDRO', 'ARANDA', 36320071, '', 0, '', 0, '', '0', '', 0, 1, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(865, 'LUIS LEONARDO', 'ARANEDA MEDEL', 14616066, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(866, 'VIVIANA PATRICIA', 'ARANGUIZ', 16496628, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(867, 'MARIA ANA', 'ARANIZ', 6689795, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(868, 'MARIA CELINA', 'ARANO', 3354090, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(869, 'CLAUDIA ADRIANA', 'ARANOVICH', 12889804, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(870, 'TITA DEL CARMEN', 'ARAOZ', 10641910, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(871, 'HORACIO ANTONIO', 'ARAOZ', 11960137, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(872, 'MARIA LAURA', 'ARAOZ', 14226560, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(873, 'TAMARA ERIKA', 'ARARIO', 34987927, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(874, 'SILVANA PATRICIA', 'ARATA', 12969217, '', 0, '', 0, '', '0', '', 0, 11, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(875, 'EZEQUIEL RAUL', 'ARATER', 31251515, '', 0, '', 0, '', '0', '', 0, 12, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(876, 'MARIA O', 'ARAUJO', 1664775, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(877, 'MARIA DE LAS MERCEDES', 'ARAUJO', 5290645, '', 0, '', 0, '', '0', '', 0, 12, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(878, 'OSCAR EDUARDO', 'ARAUJO', 16688569, '', 0, '', 0, '', '0', '', 0, 12, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(879, 'LILIANA BEATRIZ', 'ARAUJO', 24794341, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(880, 'JUAN FACUNDO', 'ARAUJO', 28988020, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(881, 'EVA', 'ARAVI', 5861136, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(882, 'SILVIA VIOLETA', 'ARAYA', 20360605, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(883, 'PEDRO', 'ARBIA', 4310415, '', 0, '', 0, '', '0', '', 0, 9, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(884, 'JOSE LUIS', 'ARBITMAN', 26583571, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(885, 'NORA MAGDALENA', 'ARBUES', 9952209, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(886, 'MARCELO C', 'ARCALA', 20440438, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(887, 'JUANA', 'ARCAS', 1304923, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(888, 'ANA MARIA E', 'ARCE', 3766180, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(889, 'REYNALDO ALBERTO', 'ARCE', 5811630, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(890, 'JORGE OSCAR', 'ARCE', 8247331, '', 0, '', 0, '', '0', '', 0, 10, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(891, 'MAXIMINA', 'ARCE', 9977422, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(892, 'GLADYS LILIANA', 'ARCE', 14473098, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(893, 'ELSA CAROLINA', 'ARCE', 22706313, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(894, 'JORGELINA MARCELA', 'ARCE', 22976011, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(895, 'JOSE LUIS', 'ARCE', 23354446, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(896, 'SILVIA MARIELA', 'ARCE', 24217371, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(897, 'JACQUELINE IVONNE', 'ARCE', 26136189, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(898, 'HERNAN ARIEL', 'ARCE', 29535989, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(899, 'NESTOR GABRIEL', 'ARCE', 29563681, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(900, 'PATRICIA MIRELLA', 'ARCE', 30642374, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(901, 'LAURA TAMARA', 'ARCE', 30911230, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(902, 'ERIKA ELIZABETH', 'ARCE', 32092834, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(903, 'JONATAN EZEQUIEL', 'ARCE', 32936291, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(904, 'JORGE DAVID', 'ARCE', 34523820, '', 0, '', 0, '', '0', '', 0, 8, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(905, 'GISELA SOLEDAD', 'ARCE RAMOS', 34531212, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(906, 'SANTIAGO ERNESTO', 'ARCEO', 30460357, '', 0, '', 0, '', '0', '', 0, 12, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(907, 'MARIA', 'ARCHETTI', 10390366, '', 0, '', 0, '', '0', '', 0, 1, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(908, 'DIEGO MARCELO', 'ARCHETTI', 31293488, '', 0, '', 0, '', '0', '', 0, 10, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(909, 'ANGELA TERESA', 'ARDANZABAL', 385078, '', 0, '', 0, '', '0', '', 0, 6, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(910, 'MIGUELINA LUCIA', 'ARDILES', 13386655, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(911, 'ANDREA GRACIELA', 'ARDILES', 24873113, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(912, 'CESAR FABIAN', 'ARDILES', 26997905, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(913, 'MARISOL ELIZABETH', 'ARDILES', 29431771, '', 0, '', 0, '', '0', '', 0, 8, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(914, 'NATALIA ELIZABETH', 'ARDILES', 33208286, '', 0, '', 0, '', '0', '', 0, 4, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(915, 'ERNESTO CARLOS', 'ARDITO', 22656488, '', 0, '', 0, '', '0', '', 0, 13, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(916, 'IRMA YOLANDA', 'ARDURA', 2671912, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(917, 'ENRIQUE LUIS', 'ARELLANO', 1018861, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(918, 'JOSE SALVADOR', 'ARELLANO', 4283153, '', 0, '', 0, '', '0', '', 0, 2, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(919, 'MIRTA RAQUEL', 'ARELLANO', 4899270, '', 0, '', 0, '', '0', '', 0, 9, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(920, 'MARIA CRISTINA', 'ARELLANO', 6356943, '', 0, '', 0, '', '0', '', 0, 3, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(921, 'CARLOS MOISES', 'ARELLANO', 7801813, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(922, 'MIGUEL ANGEL', 'ARELLANO', 10922319, '', 0, '', 0, '', '0', '', 0, 4, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(923, 'MARCELA', 'ARELLANO', 30409687, '', 0, '', 0, '', '0', '', 0, 5, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(924, 'OLGA LUISA', 'ARENA', 1773339, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(925, 'MARIA DEL CARMEN', 'ARENA', 2628155, '', 0, '', 0, '', '0', '', 0, 14, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(926, 'JORGE SEBASTIAN', 'ARENA', 4054751, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(927, 'EDUARDO DANIEL', 'ARENA', 4134983, '', 0, '', 0, '', '0', '', 0, 7, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(928, 'NELFY SUSANA', 'ARENA', 4265185, '', 0, '', 0, '', '0', '', 0, 2, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(929, 'JORGE LUIS', 'ARENA', 4981612, '', 0, '', 0, '', '0', '', 0, 15, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(930, 'LILIA BEATRIZ', 'ARENA', 11292932, '', 0, '', 0, '', '0', '', 0, 10, 'Femenino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 11),
(931, 'ERNESTO JOSE', 'ARENA', 14232167, '', 0, '', 0, '', '0', '', 0, 10, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(932, 'FABIAN MARCOS', 'ARENA', 22668943, '', 0, '', 0, '', '0', '', 0, 11, 'Masculino', 0, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `audio`
--

CREATE TABLE IF NOT EXISTS `audio` (
  `id_audio` int(11) NOT NULL,
  `title_audio` varchar(255) NOT NULL,
  `description_audio` varchar(255) NOT NULL,
  `folder_audio` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `check_users`
--

CREATE TABLE IF NOT EXISTS `check_users` (
  `id_check_users` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code_user` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `check_users`
--

INSERT INTO `check_users` (`id_check_users`, `user_id`, `code_user`) VALUES
(1, 1, 'RYoUDslnvg6yjiC5fOLywCGC04+7j+ZXHYkA+EnuH7+ZxDHyEH4f0oQ4MpmWv38DeaVwrmwLRfClITYyGTL+nA=='),
(2, 2, 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(3, 3, 'dFi0Afa8xGBsRtp3nDCqjYm2A9uP7rglC9CDhqhruTTATrmKS7kEKaUdv5cXnHcs6XPQGgpYPiFWEMgaCvh3Yg=='),
(4, 4, '7+r+SPuX6n7qpljGML1CRx70o9teDzA7xkdpJ97DodoAu/ARPCHND6S8RuF2+74PcIjlaSYps2cJ0V/AfObMEQ=='),
(5, 5, 'Op/fNcLLtNutCU5aKP4JrX6+gW7huomfyCarYh/kyR1PX1/8y5crRDVm81/lBaXMQyuKGaapHBMlSnv8JNU/Cg=='),
(6, 6, 'ewl9kahzYZm90UBwaM0HTvh9fFrQrkkJgTqG7+Ck9yAX3MI6YFTniA/K2ZTJ9gLaJDYnBcJSBVitqdFDS/87cA=='),
(7, 7, 'U4fOgjKOKOVcyPjiv7SN6jpWX8WxPx41Memq/XIGf/ieA9zwZoOW3JOme4pdlX5HRYu/JKzryOmqNUQao2aorQ==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts_affiliates`
--

CREATE TABLE IF NOT EXISTS `contacts_affiliates` (
  `id_contact_affliate` int(11) NOT NULL,
  `affiliate_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `date_contact_affiliate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacts_affiliates`
--

INSERT INTO `contacts_affiliates` (`id_contact_affliate`, `affiliate_id`, `contact_id`, `date_contact_affiliate`) VALUES
(1, 504, 3, '2018-03-04 19:53:43'),
(2, 807, 4, '2018-03-04 04:00:00'),
(3, 4, 3, '2018-03-06 04:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts_types`
--

CREATE TABLE IF NOT EXISTS `contacts_types` (
  `id_contacts_type` int(11) NOT NULL,
  `name_contacts_type` varchar(50) NOT NULL,
  `description_contacts_type` varchar(255) NOT NULL,
  `status_contacts_tye` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacts_types`
--

INSERT INTO `contacts_types` (`id_contacts_type`, `name_contacts_type`, `description_contacts_type`, `status_contacts_tye`) VALUES
(1, 'Visita', 'El Responsable le hace una visita personal a afiliado', 1),
(2, 'Email', 'Via correo electronico el responsable se comunica con el afiliado', 1),
(3, 'WhatsApp', '', 1),
(4, 'Llamada Telefonica', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `id_faq` int(11) NOT NULL,
  `title_faq` varchar(255) NOT NULL,
  `answer_faq` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `faq`
--

INSERT INTO `faq` (`id_faq`, `title_faq`, `answer_faq`) VALUES
(1, 'Por que no me llegan los Mails?', 'Debes contar con un correo electronico valido resgistrado, si no llegan es por que no esta correcto, para ello debes contactar a el administrador para que te ayude.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `general_rules`
--

CREATE TABLE IF NOT EXISTS `general_rules` (
  `id_general_rule` int(11) NOT NULL,
  `title_general_rule` varchar(255) NOT NULL,
  `description_general_rule` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `master`
--

CREATE TABLE IF NOT EXISTS `master` (
  `id_master` int(11) NOT NULL,
  `name_master` varchar(255) NOT NULL,
  `cargo_master` varchar(255) NOT NULL,
  `date_register_master` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `check_user` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `master`
--

INSERT INTO `master` (`id_master`, `name_master`, `cargo_master`, `date_register_master`, `check_user`) VALUES
(1, 'Henry Martinez', 'Presidente', '2018-02-22 13:20:54', 'RYoUDslnvg6yjiC5fOLywCGC04+7j+ZXHYkA+EnuH7+ZxDHyEH4f0oQ4MpmWv38DeaVwrmwLRfClITYyGTL+nA==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_messages`
--

CREATE TABLE IF NOT EXISTS `model_messages` (
  `id_model_message` int(11) NOT NULL,
  `title_model_message` varchar(255) NOT NULL,
  `content_model_message` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `model_messages`
--

INSERT INTO `model_messages` (`id_model_message`, `title_model_message`, `content_model_message`) VALUES
(1, 'Demo de mensaje', 'Buenas tardes el presente es para ofrecerle un paquete.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posible_answer`
--

CREATE TABLE IF NOT EXISTS `posible_answer` (
  `id_posible_answer` int(11) NOT NULL,
  `title_posible_answer` varchar(50) NOT NULL,
  `text_posible_answer` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `posible_answer`
--

INSERT INTO `posible_answer` (`id_posible_answer`, `title_posible_answer`, `text_posible_answer`) VALUES
(1, 'Como asisto a un evento?', 'Debes contactar con un&nbsp;<u>Afiliado</u>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rules`
--

CREATE TABLE IF NOT EXISTS `rules` (
  `id_rule` int(11) NOT NULL,
  `title_rule` varchar(50) NOT NULL,
  `text_rule` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rules`
--

INSERT INTO `rules` (`id_rule`, `title_rule`, `text_rule`) VALUES
(1, 'Norma de Conexion', '<b>Los Usuarios no pueden conectarse al mismo tiempo desde diferentes equipos.</b>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id_session` int(11) NOT NULL,
  `in_session` datetime NOT NULL,
  `out_session` datetime NOT NULL,
  `session_check_user` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sessions`
--

INSERT INTO `sessions` (`id_session`, `in_session`, `out_session`, `session_check_user`) VALUES
(3, '2018-03-05 18:09:29', '2018-03-05 07:39:19', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(8, '2018-03-05 09:14:05', '2018-03-05 09:14:27', 'ewl9kahzYZm90UBwaM0HTvh9fFrQrkkJgTqG7+Ck9yAX3MI6YFTniA/K2ZTJ9gLaJDYnBcJSBVitqdFDS/87cA==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sessions_hist`
--

CREATE TABLE IF NOT EXISTS `sessions_hist` (
  `id_session` int(11) NOT NULL,
  `in_session` datetime NOT NULL,
  `out_session` datetime NOT NULL,
  `session_check_user` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `sessions_hist`
--

INSERT INTO `sessions_hist` (`id_session`, `in_session`, `out_session`, `session_check_user`) VALUES
(3, '2017-03-05 06:11:27', '2018-03-05 07:06:56', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(4, '2018-03-05 07:06:33', '2018-03-05 07:06:56', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(5, '2018-03-05 07:08:49', '0000-00-00 00:00:00', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(6, '2018-03-05 07:20:00', '2018-03-05 07:20:29', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(7, '2018-03-05 07:22:52', '2018-03-05 07:24:33', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(8, '2018-03-05 07:24:38', '0000-00-00 00:00:00', 'ewl9kahzYZm90UBwaM0HTvh9fFrQrkkJgTqG7+Ck9yAX3MI6YFTniA/K2ZTJ9gLaJDYnBcJSBVitqdFDS/87cA=='),
(9, '2018-03-05 07:38:50', '0000-00-00 00:00:00', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(10, '2018-03-05 07:38:54', '0000-00-00 00:00:00', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(11, '2018-03-05 07:39:05', '2018-03-05 07:39:20', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(12, '2018-03-05 07:39:25', '0000-00-00 00:00:00', 'ewl9kahzYZm90UBwaM0HTvh9fFrQrkkJgTqG7+Ck9yAX3MI6YFTniA/K2ZTJ9gLaJDYnBcJSBVitqdFDS/87cA=='),
(13, '2018-03-05 09:14:05', '2018-03-05 09:14:27', 'ewl9kahzYZm90UBwaM0HTvh9fFrQrkkJgTqG7+Ck9yAX3MI6YFTniA/K2ZTJ9gLaJDYnBcJSBVitqdFDS/87cA=='),
(14, '2018-03-05 09:32:52', '0000-00-00 00:00:00', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(15, '2018-03-05 15:41:32', '0000-00-00 00:00:00', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(16, '2018-03-05 16:53:37', '0000-00-00 00:00:00', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(17, '2018-03-05 16:54:03', '0000-00-00 00:00:00', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(18, '2018-03-05 18:09:28', '0000-00-00 00:00:00', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow=='),
(19, '2018-03-05 18:09:29', '0000-00-00 00:00:00', 'UMOME+eYilMwcMH93yA6k7+GCEuGDvfz9dmNHYU8BwplfVUh+LqUKNiA6NVnw6QQA7StTOuVXdScWjU0sPj8Ow==');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `specialcheck`
--

CREATE TABLE IF NOT EXISTS `specialcheck` (
  `id_special_check` int(11) NOT NULL,
  `pass_special_check` varchar(255) NOT NULL,
  `created_special_check` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `specialcheck`
--

INSERT INTO `specialcheck` (`id_special_check`, `pass_special_check`, `created_special_check`, `status_id`) VALUES
(1, 'yOsu5Msvyr9QhnjAiN8Leq2bjMduPxllUtEEk02zKBOow2CNlDwwsxMtFFKUJ38oNt462h3QllOh/msqAA3aiQ==', '2018-02-22 11:43:12', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id_status` int(11) NOT NULL,
  `name_status` varchar(255) NOT NULL,
  `description_status` varchar(255) NOT NULL,
  `color_status` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id_status`, `name_status`, `description_status`, `color_status`) VALUES
(1, 'Activo', 'Status Activo para Todos los casos', '#3ecb5e'),
(2, 'Inactivo', 'Inactivo para todos los casos', '#fb9678'),
(3, 'Espera de Confirmacion', 'Espera de confirmacion a las solicitudes de vendedores', '#5bc0de'),
(4, 'Posible Voto', 'cliente que posiblemente registre', '#f39c12'),
(5, 'Posible Candidato', 'Posible Candidato', '#bd7c14'),
(6, 'Contactado', 'Contactado', '#0094D6'),
(7, 'Voto Confirmado', 'Voto Confirmado', '#9675ce'),
(8, 'Candidato Confirmado', 'Candidato Confirmado', '#00a65a'),
(9, 'Voto Contrario', 'Voto Contrario', '#dd4b39'),
(10, 'Compañero', 'Compañero', '#00a65a'),
(11, 'N/NA', 'N/NA', '#e6e6e6');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `types_users`
--

CREATE TABLE IF NOT EXISTS `types_users` (
  `id_type_user` int(11) NOT NULL,
  `name_type` varchar(255) NOT NULL,
  `description_type` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `types_users`
--

INSERT INTO `types_users` (`id_type_user`, `name_type`, `description_type`) VALUES
(1, 'Master', 'Usuario Principal del Sistema'),
(2, 'Administrador', 'Administrador encargado por la empresa.'),
(3, 'Vendedor', 'Usuario admitido o registrado por un administrador o el master.'),
(4, 'Comun', 'Se le asignan los modulos a los que puede entrar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL,
  `name_user` varchar(255) NOT NULL,
  `pass_user` varchar(255) NOT NULL,
  `type_user` int(11) NOT NULL,
  `status_user` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user`, `name_user`, `pass_user`, `type_user`, `status_user`) VALUES
(1, 'hemm18', 'WW40Qtqr//sy30c5viGrsLHNSPNTj1vRSoDqGMOIb7PlYvR4KiUcxIb4hlSLSlNFfl0Q7C2/opI6hR9D6M+H9g==', 1, 1),
(2, 'admin', 'iZh9ozN/BhGpBEsQyvWfT6MUxf6UYNjwd4XKhyOnGZzlbjO49F/BbGgrZD/wF3igALnp9ph4J2ufbwmMaE/K0w==', 2, 1),
(3, 'jperez', '23/tCAY1wDpOkk0fRu/mviJ1/DdqIhC5Z1OQ1VJjxs1wlgMbJyuY1hYo+bXZjc8cADv4BoiBGcLjYiJlR/U8Pw==', 2, 1),
(4, 'maper', '23/tCAY1wDpOkk0fRu/mviJ1/DdqIhC5Z1OQ1VJjxs1wlgMbJyuY1hYo+bXZjc8cADv4BoiBGcLjYiJlR/U8Pw==', 2, 1),
(5, 'tyrone', 'MuW4v12sBKulMzNNLCAwVi5ohu6H7lcUvdGyl10m0H/I+pNygVao1ucYpxjavRfwskm9py+W6qTcItUPykgWKg==', 2, 1),
(6, 'hola', 'G1DfVHMxEeaxM3jqKlOyOojWzy/ghK/ltUiybo4HeWLS+Xo1Gvs/YT4qHrVMsWAHjueZOyzH8biAd1LBjNzhSQ==', 2, 1),
(7, 'vendor@aout.com', 'uc/d0gUMrdgqrbt50ckji0Zg2wCBvqBJzGMMq+VcGLM66fpFFRN5VqGvynpimWCIVyg+TLqDRaUr6dO5KgqzBA==', 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendors`
--

CREATE TABLE IF NOT EXISTS `vendors` (
  `id_vendor` int(11) NOT NULL,
  `name_vendor` varchar(255) NOT NULL,
  `phone_vendor` varchar(255) NOT NULL,
  `direction_vendor` text NOT NULL,
  `email_vendor` varchar(255) NOT NULL,
  `sex_vendor` varchar(200) NOT NULL,
  `check_user_vendor` varchar(255) NOT NULL,
  `dni_vendor` varchar(50) NOT NULL,
  `age_vendor` int(11) NOT NULL,
  `date_register_vendor` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vendors`
--

INSERT INTO `vendors` (`id_vendor`, `name_vendor`, `phone_vendor`, `direction_vendor`, `email_vendor`, `sex_vendor`, `check_user_vendor`, `dni_vendor`, `age_vendor`, `date_register_vendor`, `status_id`) VALUES
(3, 'Mercedes Marighetti', '', '', 'mercedesmarighetti@gmail.com', '	Femenino', '', '35678820', 26, '2018-02-28 20:34:54', 1),
(4, 'steven munoz', '', '', 'steven.cabo18@gmail.com', '	Masculino', '', '1452', 25, '2018-03-05 08:45:52', 2),
(5, 'Federico Treguer', '4556788723', 'el centro', 'fstreguer@gmail.com', 'Masculino', '', '39207552', 22, '2018-03-05 08:59:38', 1),
(6, 'Luis Gotfryd', '', '', 'Lggot@yahoo.com.ar', '	Masculino', '', '14727777', 56, '2018-02-28 20:34:54', 1),
(7, 'Marcelo Pascal', '', '', 'marcelopascal@yahoo.com.ar', '	Masculino', '', '20369251', 49, '2018-02-28 20:34:54', 1),
(8, 'Hernan Martini', '', '', 'rrcc100@hotmail.com', '	Masculino', '', '33563445', 30, '2018-02-28 20:34:54', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `id_video` int(11) NOT NULL,
  `title_video` varchar(50) NOT NULL,
  `description_video` varchar(255) NOT NULL,
  `folder_video` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id_activity`);

--
-- Indices de la tabla `activities_affiliates`
--
ALTER TABLE `activities_affiliates`
  ADD PRIMARY KEY (`id_activity_affiliate`);

--
-- Indices de la tabla `administrators`
--
ALTER TABLE `administrators`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indices de la tabla `affiliate_vendor`
--
ALTER TABLE `affiliate_vendor`
  ADD PRIMARY KEY (`id_affiliate_vendor`);

--
-- Indices de la tabla `affiliates`
--
ALTER TABLE `affiliates`
  ADD PRIMARY KEY (`id_affiliate`);

--
-- Indices de la tabla `audio`
--
ALTER TABLE `audio`
  ADD PRIMARY KEY (`id_audio`);

--
-- Indices de la tabla `check_users`
--
ALTER TABLE `check_users`
  ADD PRIMARY KEY (`id_check_users`);

--
-- Indices de la tabla `contacts_affiliates`
--
ALTER TABLE `contacts_affiliates`
  ADD PRIMARY KEY (`id_contact_affliate`);

--
-- Indices de la tabla `contacts_types`
--
ALTER TABLE `contacts_types`
  ADD PRIMARY KEY (`id_contacts_type`);

--
-- Indices de la tabla `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id_faq`);

--
-- Indices de la tabla `general_rules`
--
ALTER TABLE `general_rules`
  ADD PRIMARY KEY (`id_general_rule`);

--
-- Indices de la tabla `master`
--
ALTER TABLE `master`
  ADD PRIMARY KEY (`id_master`);

--
-- Indices de la tabla `model_messages`
--
ALTER TABLE `model_messages`
  ADD PRIMARY KEY (`id_model_message`);

--
-- Indices de la tabla `posible_answer`
--
ALTER TABLE `posible_answer`
  ADD PRIMARY KEY (`id_posible_answer`);

--
-- Indices de la tabla `rules`
--
ALTER TABLE `rules`
  ADD PRIMARY KEY (`id_rule`);

--
-- Indices de la tabla `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id_session`), ADD UNIQUE KEY `session_check_user` (`session_check_user`);

--
-- Indices de la tabla `sessions_hist`
--
ALTER TABLE `sessions_hist`
  ADD PRIMARY KEY (`id_session`);

--
-- Indices de la tabla `specialcheck`
--
ALTER TABLE `specialcheck`
  ADD PRIMARY KEY (`id_special_check`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indices de la tabla `types_users`
--
ALTER TABLE `types_users`
  ADD PRIMARY KEY (`id_type_user`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indices de la tabla `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id_vendor`);

--
-- Indices de la tabla `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id_video`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `activities`
--
ALTER TABLE `activities`
  MODIFY `id_activity` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `activities_affiliates`
--
ALTER TABLE `activities_affiliates`
  MODIFY `id_activity_affiliate` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `administrators`
--
ALTER TABLE `administrators`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `affiliate_vendor`
--
ALTER TABLE `affiliate_vendor`
  MODIFY `id_affiliate_vendor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT de la tabla `affiliates`
--
ALTER TABLE `affiliates`
  MODIFY `id_affiliate` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=933;
--
-- AUTO_INCREMENT de la tabla `audio`
--
ALTER TABLE `audio`
  MODIFY `id_audio` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `check_users`
--
ALTER TABLE `check_users`
  MODIFY `id_check_users` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `contacts_affiliates`
--
ALTER TABLE `contacts_affiliates`
  MODIFY `id_contact_affliate` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `contacts_types`
--
ALTER TABLE `contacts_types`
  MODIFY `id_contacts_type` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `faq`
--
ALTER TABLE `faq`
  MODIFY `id_faq` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `general_rules`
--
ALTER TABLE `general_rules`
  MODIFY `id_general_rule` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `master`
--
ALTER TABLE `master`
  MODIFY `id_master` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `model_messages`
--
ALTER TABLE `model_messages`
  MODIFY `id_model_message` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `posible_answer`
--
ALTER TABLE `posible_answer`
  MODIFY `id_posible_answer` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `rules`
--
ALTER TABLE `rules`
  MODIFY `id_rule` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id_session` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `sessions_hist`
--
ALTER TABLE `sessions_hist`
  MODIFY `id_session` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `specialcheck`
--
ALTER TABLE `specialcheck`
  MODIFY `id_special_check` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `types_users`
--
ALTER TABLE `types_users`
  MODIFY `id_type_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id_vendor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `video`
--
ALTER TABLE `video`
  MODIFY `id_video` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
