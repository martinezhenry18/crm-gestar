-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-02-2018 a las 16:10:45
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crm_gestar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `affiliates`
--

DROP TABLE IF EXISTS `affiliates`;
CREATE TABLE `affiliates` (
  `id_affiliate` int(11) NOT NULL,
  `names_affiliate` varchar(255) NOT NULL,
  `last_names_affiliate` varchar(255) NOT NULL,
  `dni_affiliate` bigint(20) NOT NULL,
  `street_affiliate` varchar(255) NOT NULL,
  `number_affiliate` int(11) NOT NULL,
  `dtto_affiliate` varchar(255) NOT NULL,
  `postal_code_affiliate` int(11) NOT NULL,
  `job_affiliate` varchar(255) NOT NULL,
  `studies_affiliate` int(11) NOT NULL,
  `denomination_affiliate` varchar(255) NOT NULL,
  `circuit_affiliate` int(11) NOT NULL,
  `commune_affiliate` int(11) NOT NULL,
  `gener_affiliate` varchar(255) NOT NULL,
  `age_affiliate` int(11) NOT NULL,
  `facebook_affiliate` varchar(255) NOT NULL,
  `twitter_affiliate` varchar(255) NOT NULL,
  `instagram_affiliate` varchar(255) NOT NULL,
  `email_affiliate` varchar(255) NOT NULL,
  `phone_1_affiliate` bigint(20) NOT NULL,
  `phone_2_affiliate` bigint(20) NOT NULL,
  `checked_phone` int(11) NOT NULL DEFAULT '0',
  `mobile_1_affiliate` bigint(20) NOT NULL,
  `mobile_2_affiliate` bigint(20) NOT NULL,
  `mobile_3_affiliate` bigint(20) NOT NULL,
  `mobile_4_affiliate` bigint(20) NOT NULL,
  `mobile_5_affiliate` bigint(20) NOT NULL,
  `mobile_6_affiliate` bigint(20) NOT NULL,
  `comments_affiliate` text NOT NULL,
  `status_affiliate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `affiliates`
--

INSERT INTO `affiliates` (`id_affiliate`, `names_affiliate`, `last_names_affiliate`, `dni_affiliate`, `street_affiliate`, `number_affiliate`, `dtto_affiliate`, `postal_code_affiliate`, `job_affiliate`, `studies_affiliate`, `denomination_affiliate`, `circuit_affiliate`, `commune_affiliate`, `gener_affiliate`, `age_affiliate`, `facebook_affiliate`, `twitter_affiliate`, `instagram_affiliate`, `email_affiliate`, `phone_1_affiliate`, `phone_2_affiliate`, `checked_phone`, `mobile_1_affiliate`, `mobile_2_affiliate`, `mobile_3_affiliate`, `mobile_4_affiliate`, `mobile_5_affiliate`, `mobile_6_affiliate`, `comments_affiliate`, `status_affiliate`) VALUES
(1, 'Henry Eduardo', 'Martinez Marmole', 20823231, 'Primera calle', 123, 'Dtto. Capital', 1012, 'Desarrollador Web', 1, '', 12, 12, 'Masculino', 25, 'hemm', 'hemm_18_', 'hemm18', 'martinezhenry18@hotmail.com', 4248559520, 0, 0, 0, 0, 0, 0, 0, 0, '', 1),
(3, 'Pedro Rafael', 'Mendez Rodriguez', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(4, 'Sal ', 'Carson', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(5, 'Eulalia ', 'Howell', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(6, 'Fay ', 'Franks', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 7),
(7, 'Carey ', 'Stanton', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 8),
(8, 'Gayla ', 'Hurst', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 9),
(9, 'Noella ', 'Browne', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(10, 'Jesse ', 'Lyon', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(11, 'Dorthey ', 'Rich', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(12, 'Ha ', 'Appleton', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(13, 'Tara ', 'Plummer', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 5),
(14, 'Ernestine ', 'Boyle', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(15, 'Calvin ', 'Cameron', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(16, 'Dwayne ', 'Haines', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(17, 'Delicia ', 'Dupont', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 6),
(18, 'Laree ', 'Welsh', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(19, 'Cinthia ', 'Sheehan', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(20, 'Laurene ', 'Cole', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(21, 'Tamiko ', 'Mcneill', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(22, 'Nenita ', 'Edge', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(23, 'Ruby ', 'Thornton', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 10),
(24, 'Cordelia ', 'Brandrick', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(25, 'Suk ', 'Barnett', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(26, 'Daniele ', 'Waller', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(27, 'Morris ', 'Clayton', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(28, 'Han ', 'Sykes', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(29, 'Hui ', 'Bains', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(30, 'Hye ', 'Jones', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(31, 'Joya ', 'Quinn', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(32, 'Becki ', 'Flower', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(33, 'Tina ', 'Barry', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(34, 'Eleonore ', 'Hooper', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(35, 'Simonne ', 'Wagstaff', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(36, 'Julianna ', 'Dawson', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(37, 'Jean ', 'Horn', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(38, 'Myrl ', 'Lynch', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(39, 'Roseanna ', 'Kirkpatrick', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(40, 'Denae ', 'Green', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(41, 'Violette ', 'Clarkson', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(42, 'Paulene ', 'Day', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(43, 'Millie ', 'Moon', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(44, 'Hailey ', 'Clements', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(45, 'Latia ', 'Nicholson', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(46, 'Melody ', 'Tucker', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(47, 'Jannette ', 'Brookes', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(48, 'Taneka ', 'Coleman', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(49, 'Verlie ', 'Fox', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(50, 'Sharleen ', 'Corrigan', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(51, 'Lashawna ', 'Holder', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(52, 'Ezra ', 'Robins', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4),
(53, 'Silvana ', 'Collins', 10111222333, '', 0, '', 0, '', 0, '', 0, 0, 'Masculino', 25, '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id_status` int(11) NOT NULL,
  `name_status` varchar(255) NOT NULL,
  `description_status` varchar(255) NOT NULL,
  `color_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id_status`, `name_status`, `description_status`, `color_status`) VALUES
(1, 'Activo', 'Status Activo para Todos los casos', '#3ecb5e'),
(2, 'Inactivo', 'Inactivo para todos los casos', '#fb9678'),
(3, 'Espera de Confirmacion', 'Espera de confirmacion a las solicitudes de vendedores', '#5bc0de'),
(4, 'Posible Voto', 'cliente que posiblemente registre', '#f39c12'),
(5, 'Posible Candidato', 'Posible Candidato', '#bd7c14'),
(6, 'Contactado', 'Contactado', '#0094D6'),
(7, 'Voto Confirmado', 'Voto Confirmado', '#9675ce'),
(8, 'Candidato Confirmado', 'Candidato Confirmado', '#00a65a'),
(9, 'Voto Contrario', 'Voto Contrario', '#dd4b39'),
(10, 'Compañero', 'Compañero', '#00a65a'),
(11, 'N/NA', 'N/NA', '#e6e6e6');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `affiliates`
--
ALTER TABLE `affiliates`
  ADD PRIMARY KEY (`id_affiliate`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id_status`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `affiliates`
--
ALTER TABLE `affiliates`
  MODIFY `id_affiliate` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT de la tabla `status`
--
ALTER TABLE `status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
