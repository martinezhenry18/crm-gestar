<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	function afiliados()
	{
		$this->db->select('id_affiliate,commune_affiliate,gener_affiliate,dni_affiliate,names_affiliate,last_names_affiliate,status_affiliate,age_affiliate,name_status,color_status');
		$this->db->from('affiliates');
		$this->db->join('status', 'status.id_status = affiliates.status_affiliate', 'inner');
		$this->db->where('status_affiliate != ', 2);
		$query = $this->db->get();
		return $query->result();
	}

	public function solPend()
	{
		$this->db->select('*');
		$this->db->from('vendors');
		$this->db->where('status_id', 3);
		$query = $this->db->get();
		return $query->result();
	}

	function totales()
	{
		$this->db->select('COUNT(*) as PV');
		$this->db->where('status_affiliate', 4);
		$this->db->from('affiliates');
		$pv = $this->db->get()->row('PV');

		$this->db->select('COUNT(*) as PC');
		$this->db->where('status_affiliate', 5);
		$this->db->from('affiliates');
		$pc = $this->db->get()->row('PC');

		$this->db->select('COUNT(*) as VC');
		$this->db->where('status_affiliate', 7);
		$this->db->from('affiliates');
		$vc = $this->db->get()->row('VC');

		$this->db->select('COUNT(*) as CC');
		$this->db->where('status_affiliate', 8);
		$this->db->from('affiliates');
		$cc = $this->db->get()->row('CC');

		$this->db->select('COUNT(*) as VCO');
		$this->db->where('status_affiliate', 9);
		$this->db->from('affiliates');
		$vco = $this->db->get()->row('VCO');

		$this->db->select('COUNT(*) as CO');
		$this->db->where('status_affiliate', 10);
		$this->db->from('affiliates');
		$co = $this->db->get()->row('CO');

		$this->db->select('COUNT(*) as TOT');
		$this->db->from('affiliates');
		$tot = $this->db->get()->row('TOT');

		return array('PV' => $pv,
					'PC' => $pc,
					'VC' => $vc,
					'CC' => $cc,
					'VCO' => $vco,
					'CO' => $co,
					'TOT' => $tot);
	}

}

/* End of file Principal_Model.php */
/* Location: ./application/models/Principal_Model.php */