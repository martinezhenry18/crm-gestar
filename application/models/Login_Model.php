<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	function adminData($user)
	{
		$this->db->select('name_admin,phone_admin,date_register_admin,code_user,email_admin');
		$this->db->from('users');
		$this->db->join('check_users', 'users.id_user = check_users.user_id', 'inner');
		$this->db->join('administrators', 'administrators.check_user_admin = check_users.code_user', 'inner');
		$this->db->where('name_user', $user);
		$query = $this->db->get();
		return $query->row_array();
	}

	function masterData($user)
	{
		$this->db->select('name_master,cargo_master,date_register_master,code_user');
		$this->db->from('users');
		$this->db->join('check_users', 'users.id_user = check_users.user_id', 'inner');
		$this->db->join('master', 'master.check_user = check_users.code_user', 'inner');
		$this->db->where('name_user', $user);
		$query = $this->db->get();
		return $query->row_array();
	}

	function outSession()
	{
		$this->db->where('session_check_user', $this->session->userdata('code_user'));
		$this->db->update('sessions', array('out_session' => date("Y-m-d H:i:s")));
		$this->db->select('in_session');
		$this->db->from('sessions');
		$this->db->where('session_check_user', $this->session->userdata('code_user'));
		$query = $this->db->get()->row('in_session');

		$this->db->where('in_session', $query);
		$this->db->where('session_check_user', $this->session->userdata('code_user'));
		$this->db->update('sessions_hist', array('out_session' => date("Y-m-d H:i:s")));
	}

	function privilegio($user)
	{
		$this->db->select('type_user');
		$this->db->from('users');
		$this->db->where('name_user', $user);
		$query = $this->db->get();
		return $query->row('type_user');
	}

	function realpass($user)
	{
		$this->db->select('pass_user');
		$this->db->from('users');
		$this->db->where('name_user', $user);
		$query = $this->db->get();
		return $query->row('pass_user');
	}

	public function verificaruser($user)
	{
		$this->db->select('name_user,status_user');
		$this->db->where('name_user',$user);
		$this->db->from('users');
		$query = $this->db->get();

		if ($query->num_rows() == 1) 
		{
			if ($query->row('status_user') == 1) 
			{
				return "valid";
			} else {
				return "disabled";
			}
		}
		else{
			return "invalid";
		}
	}



}

/* End of file Login_Model.php */
/* Location: ./application/models/Login_Model.php */