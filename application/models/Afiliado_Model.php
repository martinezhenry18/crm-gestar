<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Afiliado_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	function addResponsable($datos)
	{
		$this->db->insert('affiliate_vendor', $datos);
	}

	function afiliado($id)
	{
		$this->db->select('*');
		$this->db->from('affiliates');
		$this->db->join('status', 'status.id_status = affiliates.status_affiliate', 'inner');
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate', 'left');
		$this->db->join('vendors', 'vendors.id_vendor = affiliate_vendor.vendor_id', 'left');
		$this->db->join('contacts_affiliates', 'contacts_affiliates.affiliate_id = affiliates.id_affiliate', 'left');
		$this->db->where('id_affiliate', $id);
		$query = $this->db->get();
		return $query->result();
	}

	function disableAfiliado($id)
	{
		$this->db->where('id_affiliate', $id);
		$this->db->update('affiliates', array('status_affiliate' => 2));
	}

	function insertContactAffiliate($datos)
	{
		$this->db->insert('contacts_affiliates', $datos);
	}

	function removeResponsable($datos)
	{
		$this->db->where($datos);
		$this->db->delete('affiliate_vendor');
	}

	function Responsables()
	{
		$this->db->select('*');
		$this->db->from('vendors');
		$this->db->where('status_id', 1);
		$query = $this->db->get();
		return $query->result();
	}

	function saveAfiliado($datos)
	{
		$this->db->insert('affiliates', $datos);
	}

	function status()
	{
		$this->db->select('id_status,name_status');
		$this->db->from('status');
		$this->db->join('affiliates', 'affiliates.status_affiliate = status.id_status', 'inner');
		$this->db->group_by('status_affiliate');
		$this->db->where('id_status != ', 2);
		$query = $this->db->get();
		return $query->result();
	}

	function tContacto()
	{
		$this->db->select('*');
		$this->db->from('contacts_types');
		$query = $this->db->get();
		return $query->result();
	}

	function updateDataAffiliate($id_affiliate,$datos)
	{
		$this->db->where('id_affiliate', $id_affiliate);
		$this->db->update('affiliates', $datos);
	}

	function viewResp($id)
	{
		$this->db->select('name_vendor,email_vendor,dni_vendor');
		$this->db->from('vendors');
		$this->db->where('id_vendor', $id);
		$query = $this->db->get();
		return $query->result();
	}

}

/* End of file Afiliado_Model.php */
/* Location: ./application/models/Afiliado_Model.php */