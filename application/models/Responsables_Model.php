<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Responsables_Model extends CI_Model {

	

	public function __construct()
	{
		parent::__construct();
	}

	public function afiliados_noAsignados()
	{
		$this->db->select('*');
		$this->db->from('affiliates');
		$this->db->join('affiliate_vendor', 'affiliate_vendor.affiliate_id = affiliates.id_affiliate', 'left');
		$this->db->where('affiliate_vendor.id_affiliate_vendor IS NULL');
		$query = $this->db->get();
		return $query->result();
	}

	function asignaAfiliado($datos)
	{
		$this->db->insert('affiliate_vendor', $datos);
	}

	public function asignados($id)
	{
		$this->db->select('*');
		$this->db->from('affiliate_vendor');
		$this->db->join('affiliates', 'affiliates.id_affiliate = affiliate_vendor.affiliate_id', 'inner');
		$this->db->where('vendor_id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	function disableResponsable($id)
	{
		$this->db->where('id_vendor', $id);
		$this->db->update('vendors', array('status_id' => 2));
	}

	function removeAfiliado($datos)
	{
		$this->db->where($datos);
		$this->db->delete('affiliate_vendor');
	}

	function updateVendor($id,$datos)
	{
		$this->db->where('id_vendor', $id);
		$this->db->update('vendors', $datos);
	}

	function vendedores()
	{
		$this->db->select('*');
		$this->db->from('vendors');
		$this->db->join('status', 'status.id_status = vendors.status_id', 'left');
		$this->db->join('sessions', 'sessions.session_check_user = vendors.check_user_vendor', 'left');
		$this->db->where('status_id !=', 3);
		$query = $this->db->get();
		return $query->result();
	}

	function vendedor($id)
	{
		$this->db->select('*');
		$this->db->from('vendors');
		$this->db->join('status', 'status.id_status = vendors.status_id', 'left');
		$this->db->join('sessions', 'sessions.session_check_user = vendors.check_user_vendor', 'left');
		$this->db->where('id_vendor', $id);
		$query = $this->db->get();
		return $query->result();
	}

}

/* End of file Responsables_Model.php */
/* Location: ./application/models/Responsables_Model.php */