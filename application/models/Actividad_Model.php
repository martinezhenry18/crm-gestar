<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actividad_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function getEvents()
	{
		$this->db->select('id_activity as id, name_activity as title, date_activity as start, date_activity as end, hour_activity as inicio, description_activity as description, name_status, color_status as color');
		$this->db->from('activities');
		$this->db->join('status', 'status.id_status = activities.status_activity', 'inner');
		$this->db->order_by('start', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	function listActivities()
	{
		$this->db->select('*');
		$this->db->from('activities');
		$query = $this->db->get();
		return $query->result();
	}

	function saveActividad($datos)
	{
		$this->db->insert('activities', $datos);
	}

}

/* End of file Actividad_Model.php */
/* Location: ./application/models/Actividad_Model.php */