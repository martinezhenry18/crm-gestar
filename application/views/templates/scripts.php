



















    <script src="<?= base_url() ?>js/fullcalendar/moment.js"></script>
    <!-- jQuery -->
    <script src="<?= base_url() ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url() ?>js/tether.min.js"></script>
    <script src="<?= base_url() ?>js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?= base_url() ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?= base_url() ?>js/jquery.slimscroll.js"></script>
    
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="<?= base_url() ?>plugins/bower_components/html5-editor/wysihtml5-0.3.0.js"></script>
    <script src="<?= base_url() ?>plugins/bower_components/html5-editor/bootstrap-wysihtml5.js"></script>

    
    <script src="<?= base_url() ?>plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?= base_url() ?>js/custom.js"></script>
    <!--Style Switcher -->
    <script src="<?= base_url() ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script src="<?= base_url() ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>

    <script>
        
        $(document).ready(function() {
            $('#homeTable').DataTable({
                "lengthMenu": [[50, 100, -1], [50, 100, "Todos"]]
            });
        });


        $('#slimtest1').slimScroll({
            height: '150px'
        });
        $('#slimtest2').slimScroll({
            height: '150px'
        });
        $('#slimtest3').slimScroll({
            height: '150px'
        });
        $('#slimtest4').slimScroll({
            height: '150px'
        });
        $('#slimtest5').slimScroll({
            height: '150px'
        });
        $('#slimtest6').slimScroll({
            height: '150px'
        });
        $('#slimtest7').slimScroll({
            height: '150px'
        });
        $('#slimtest8').slimScroll({
            height: '150px'
        });
        $('#slimtest9').slimScroll({
            height: '150px'
        });
        $('#slimtest10').slimScroll({
            height: '150px'
        });
        $('#slimtest11').slimScroll({
            height: '150px'
        });
        $('#slimtest12').slimScroll({
            height: '150px'
        });


    </script>

    <script>
        $(document).ready(function(){
            $("#tipo").change(function() {

                var tipo = $("#tipo").val();


                if (tipo == 1) 
                {
                    cargar($("#contentHelp"),'<?= base_url("audios") ?>');
                }

                if (tipo == 2) 
                {
                    cargar($("#contentHelp"),'<?= base_url("sms") ?>');
                }

                if (tipo == 3) 
                {
                    cargar($("#contentHelp"),'<?= base_url("normas") ?>');
                }

                if (tipo == 4) 
                {
                    cargar($("#contentHelp"),'<?= base_url("resp") ?>');
                }

                if (tipo == 5) 
                {
                    cargar($("#contentHelp"),'<?= base_url("faq") ?>');
                }

                if (tipo == 6) 
                {
                    cargar($("#contentHelp"),'<?= base_url("videos") ?>');
                }
            });
        });
    </script>


    <script>
       
    /*function saveAudio() 
    {
        var params = {'title_audio' : $("#titulo_audio") , 'file_audio' :  document.getElementById('file_audio').files[0].name}
        $.ajax({
          url: '<?= base_url("saveAudio") ?>',
          type: 'POST',
          data: params,
          success: function (data) {
                alert(data);
            }
        });
    }

    function saveFileAudio(ruta) 
    {
        var archivos = document.getElementById('file_audio');
        
        var archivo = archivos.files;
        var arch = new FormData();
        
        if (archivo.length > 0)
        {
            for (var i = 0; i < archivo.length; i++) 
            {
                arch.append('archivo'+i,archivo[i]);
            }

            $.ajax({
                url: ruta,
                type:'POST',
                contentType:false,
                data:arch,
                processData:false,
                cache:false
            });
        }
    }*/
   </script>


    <script>
        function cargar (div,page) 
        {
            $(div).load(page);
        }
    </script>