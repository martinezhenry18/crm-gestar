<script src="<?= base_url() ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>


<script src="<?= base_url() ?>plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
<script src="<?= base_url() ?>plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>
<script src="<?= base_url() ?>plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
<script>
	
	$(document).ready(function() {
        $('#myTable').DataTable({
            "lengthMenu": [[50, 100, -1], [50, 100, "Todos"]]
        });
	});


</script>