


<?php foreach ($administrador as $key): ?>
    

                        <h4 class="modal-title">Detalles de Usuario</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" onsubmit="return false;" id="form_update_admin" role="form">

                            <div class="form-group">
                                <label for="reg" class="control-label">Registrado desde</label>
                                <input type="text" class="form-control" id="nombre" name="reg_desde" required value="<?= $key->date_register_admin ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label for="nombre" class="control-label">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="name_admin" required value="<?= $key->name_admin ?>">
                            </div>
                            <div class="form-group">
                                <label for="telefono" class="control-label">Telefono</label>
                                <input type="text" class="form-control" id="telefono" name="phone_admin" required value="<?= $key->phone_admin ?>">
                            </div>
                            <div class="form-group">
                                <label for="direccion" class="control-label">Direccion</label>
                                <textarea name="direction_admin" id="direccion" rows="3" class="form-control" required><?= $key->direction_admin ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="email" class="control-label">Email</label>
                                <input type="email" class="form-control" id="email" name="email_admin" required value="<?= $key->email_admin ?>">
                            </div>
                            <div class="form-group">
                                <label for="genero" class="control-label">Genero</label>
                                <select name="sex_admin" id="genero" class="form-control" required>
                                    <option value="">Genero</option>
                                    <option value="Masculino" <?php if ($key->sex_admin == "Masculino"): ?>selected<?php endif ?>>Masculino</option>
                                    <option value="Femenino" <?php if ($key->sex_admin == "Femenino"): ?>selected<?php endif ?>>Femenino</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="username" class="control-label">Username</label>
                                <input type="text" class="form-control" id="username" name="name_user" required readonly value="<?= $key->name_user ?>">
                            </div>
                            <div class="form-group">
                                <label for="pass" class="control-label">Clave</label>
                                <input type="password" class="form-control" id="pass" name="pass_user" required onkeyup="validaPass();" value="<?= $this->encrypt->decode($key->pass_user) ?>">
                                <span id="msg" style="color:red;"></span>
                            </div>
                            <div class="form-group">
                                <label for="rpass" class="control-label">Repetir Clave</label>
                                <input type="password" class="form-control" id="rpass" onkeyup="validaPass();" required value="<?= $this->encrypt->decode($key->pass_user) ?>">
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                        <button type="button" id="but" class="btn btn-success waves-effect waves-light" onclick="actualizar_admin(<?= $key->id_admin ?>)">Acualizar</button>
                    </div>
                

<?php endforeach ?>

            <script>
                function actualizar_admin(admin) 
                {
                    $.ajax({
                      url: '<?= base_url("updateAdmin") ?>/'+admin,
                      type: 'POST',
                      data: $("#form_update_admin").serialize(),
                      success: function () {
                            swal({   
                                title: "Datos Modificados",     
                                showConfirmButton: true 
                            });
                            window.location.reload();
                        }
                    });
                }

                function  validaPass() 
                {
                  var pass = $("#pass").val();
                  var pass1 = $("#rpass").val();
                  if (pass1 != pass) 
                  {
                    $("#msg").html("Las Contraseñas no Coinciden");
                    document.getElementById('but').disabled = true;
                  } else {
                    $("#msg").html("");
                    document.getElementById('but').disabled = false;
                  }
                }
            </script>