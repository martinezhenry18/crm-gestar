
        
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Tablas</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Tablas</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="col-md-6 col-lg-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-20">Tablas Principales del Sistema</h3>
                            <!-- Nav tabs -->
                            <ul class="nav customtab2 nav-tabs" role="tablist">
                                <li role="presentation" class="nav-item"><a href="#home6" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Administradores</span></a></li>
                                <li role="presentation" class="nav-item"><a href="#home6" class="nav-link" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span><span class="hidden-xs"> Vendedores</span></a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="home6">
                                    <div class="col-md-12">
                                        <div class="white-box">
                                            <h3 class="box-title m-b-0">Listado de Usuarios</h3>
                                            <div class="table-responsive">
                                                <table id="myTable" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Email</th>
                                                            <th>Nombre</th>
                                                            <th>Telefono</th>
                                                            <th>Ver</th>
                                                            <th>Eliminar</th>
                                                            <th>Ultimo Login</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($administradores as $key): ?>
                                                            <tr>
                                                                <td><?= $key->email_admin ?></td>
                                                                <td><?= $key->name_admin ?></td>
                                                                <td><?= $key->phone_admin ?></td>
                                                                <td><a href="#"><i class="fa fa-search"></i></a></td>
                                                                <td><a href="#"><i class="fa fa-times text-danger"></i></a></td>
                                                                <td>Hoy</td>
                                                            </tr>
                                                        <?php endforeach ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#responsive-modal">Registrar Administrador</a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->
                
               <div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Registrar un Administrador</h4>
                            </div>
                            <div class="modal-body">
                                <form method="post" onsubmit="return false;" id="form_save_admin" role="form">
                                    <div class="form-group">
                                        <label for="nombre" class="control-label">Nombre</label>
                                        <input type="text" class="form-control" id="nombre" name="nombre" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="telefono" class="control-label">Telefono</label>
                                        <input type="text" class="form-control" id="telefono" name="telefono" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="direccion" class="control-label">Direccion</label>
                                        <textarea name="direccion" id="direccion" rows="3" class="form-control" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="control-label">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="genero" class="control-label">Genero</label>
                                        <select name="genero" id="genero" class="form-control" required>
                                            <option value="">Genero</option>
                                            <option value="Masculino">Masculino</option>
                                            <option value="Femenino">Femenino</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="username" class="control-label">Username</label>
                                        <input type="text" class="form-control" id="username" name="username" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="pass" class="control-label">Clave</label>
                                        <input type="password" class="form-control" id="pass" name="password" required>
                                        <span id="msg" style="color:red;"></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="rpass" class="control-label">Repetir Clave</label>
                                        <input type="password" class="form-control" id="rpass" onkeyup="validaPass();" required>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                <button type="button" id="but" class="btn btn-success waves-effect waves-light" onclick="save_admin()">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>



                <script>
                    function save_admin() 
                    {
                        $.ajax({
                          url: '<?= base_url("saveAdmin") ?>',
                          type: 'POST',
                          data: $("#form_save_admin").serialize(),
                          success: function () {
                                window.alert("Usuario Guardado");
                                //window.location.reload();
                            }
                        });
                    }

                    function  validaPass() 
                    {
                      var pass = $("#pass").val();
                      var pass1 = $("#rpass").val();
                      if (pass1 != pass) 
                      {
                        $("#msg").html("Las Contraseñas no Coinciden");
                        document.getElementById('but').disabled = true;
                      } else {
                        $("#msg").html("");
                        document.getElementById('but').disabled = false;
                      }
                    }
                </script>