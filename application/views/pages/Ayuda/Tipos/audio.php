

        <div class="panel panel-default col-lg-12">
            <div class="panel-body">
            	<form method="POST" onsubmit="return false;" id="data-audio">
            		<div class="form-group">
            			<label for="">Titulo de Audio</label>
            			<input type="text" class="form-control" required name="titulo_audio" id="titulo_audio">
            		</div>
	                <div class="form-group">
	                    <label for="">Seleccionar Audio</label>
	                    <input type="file" class="form-control"  required name="file_audio" id="file_audio" />
	                </div>
	                <div class="form-group">
	                    <button class="btn btn-success">Guardar</button>
	                </div>
	            </form>
			</div>
        </div>




				<script>
					function saveAudio() 
				    {
				        var params = {'title_audio' : $("#titulo_audio").val() , 'file_audio' :  'files/ayuda/audios/'+document.getElementById('file_audio').files[0].name}
				        $.ajax({
				          	url: '<?= base_url("saveAudio") ?>',
				          	type: 'POST',
				          	data: params,
				          	success: function () {
				                alert("Audio Guardado");
			            	}
				        });
				    }

				    function saveFileAudio(ruta) 
				    {
				        var archivos = document.getElementById('file_audio');
				        
				        var archivo = archivos.files;
				        var arch = new FormData();
				        
				        if (archivo.length > 0)
				        {
				            for (var i = 0; i < archivo.length; i++) 
				            {
				                arch.append('archivo'+i,archivo[i]);
				            }

				            $.ajax({
				                url: ruta,
				                type:'POST',
				                contentType:false,
				                data:arch,
				                processData:false,
				                cache:false
				            });
				        }
				    }
				</script>