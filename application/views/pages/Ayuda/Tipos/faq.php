






        <div class="panel panel-default col-lg-12">
            <div class="panel-body">
                <form action="#" onsubmit="return false;" id="data-faq">
                    <div class="form-group">
                        <label for="">Pregunta</label>
                        <input type="text" class="form-control" placeholder="Titulo" name="title_faq" />
                    </div>
                    <div class="form-group">
                        <label for="">Respuesta</label>
                        <textarea class="textarea_editor form-control" rows="15" placeholder="Ingrese Respuesta" name="answer_faq"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" onclick="saveFaq()">Guardar</button>
                    </div>
                </form>
            </div>
        </div>



       <script>
        $(document).ready(function() {

            $('.textarea_editor').wysihtml5();


        });


            function saveFaq() 
            {
                $.ajax({
                    url: '<?= base_url("saveFaq") ?>',
                    type: 'POST',
                    data: $("#data-faq").serialize(),
                    success: function () {
                        swal({   
                            title: "Pregunta Frecuente Almacenada",     
                            showConfirmButton: true 
                        });
                        window.location.reload();
                    }
                });
            }
        </script>