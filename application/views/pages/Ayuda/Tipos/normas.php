





        <div class="panel panel-default col-lg-12">
            <div class="panel-body">
    			<form action="#" onsubmit="return false;" id="data-rule">
                    <div class="form-group">
                        <label for="">Titulo</label>
                        <input type="text" class="form-control" placeholder="Titulo" name="title_rule" />
                    </div>
                    <div class="form-group">
                        <label for="">Definicion de la Norma</label>
                        <textarea class="textarea_editor form-control" rows="15" placeholder="Redacte las normas" name="text_rule"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" onclick="saveRule()">Guardar</button>
                    </div>
                </form>
            </div>
        </div>


        
        <script>
        $(document).ready(function() {

            $('.textarea_editor').wysihtml5();


        });



            function saveRule() 
            {
                $.ajax({
                    url: '<?= base_url("saveNorma") ?>',
                    type: 'POST',
                    data: $("#data-rule").serialize(),
                    success: function () {
                        swal({   
                            title: "Norma Creada",     
                            showConfirmButton: true 
                        });
                        window.location.reload();
                    }
                });
            }
        </script>