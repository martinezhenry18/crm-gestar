




        <div class="panel panel-default col-lg-12">
            <div class="panel-body">
                <form action="#" onsubmit="return false;" id="data-Pos-Res">
                    <div class="form-group">
                        <label for="">Pregunta</label>
                        <input type="text" class="form-control" placeholder="Titulo" name="title_posible_answer" />
                    </div>
                    <div class="form-group">
                        <label for="">Posible Respuesta</label>
                        <textarea class="textarea_editor form-control" rows="15" placeholder="Ingrese respuesta" name="text_posible_answer"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success" onclick="savePosRes()">Guardar</button>
                    </div>
                </form>
            </div>
        </div>



			
        <script>
        $(document).ready(function() {

            $('.textarea_editor').wysihtml5();


        });



            function savePosRes() 
            {
                $.ajax({
                    url: '<?= base_url("savePosResp") ?>',
                    type: 'POST',
                    data: $("#data-Pos-Res").serialize(),
                    success: function () {
                        swal({   
                            title: "Posible Respuesta Almacenada",     
                            showConfirmButton: true 
                        });
                        window.location.reload();
                    }
                });
            }
        </script>