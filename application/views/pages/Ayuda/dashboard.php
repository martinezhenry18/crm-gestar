


            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Ayuda</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Panel de Ayudas</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Ayudas Disponibles</h3>
                            <a href="#modalAyuda" data-toggle="modal" data-target="#modalAyuda" class="btn btn-success" style="margin-bottom: 45px;">Crear nueva Ayuda</a>
                            

                            <div class="row">
                                <div class="col-md-12">
                                 
                                    <div class="panel-group" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="font-bold collapsed">Audios de ejemplos de llamados </a> </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                <div class="panel-body"> 

                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <h4 class="panel-title"> <a class="collapsed font-bold" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Mensajes de texto de ejemplo</a> </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                <div class="panel-body"> 
                                                    <div class="comment-center col-md-12">
                                                        <div class="comment-body col-md-3">
                                                            <?php foreach ($sms as $text): ?>
                                                                <div class="mail-contnet">
                                                                    <h5><?= $text->title_model_message ?></h5> 
                                                                    <span class="mail-desc"><?= $text->content_model_message ?></span> 
                                                                </div>
                                                            <?php endforeach ?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingThree">
                                                <h4 class="panel-title"> <a class="font-bold collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> Normas generales </a> </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body"> 
                                                    <div class="comment-center col-md-12">
                                                        <div class="comment-body col-md-3">
                                                            <?php foreach ($normas as $nor): ?>
                                                                <div class="mail-contnet">
                                                                    <h5><?= $nor->title_rule ?></h5> 
                                                                    <span class="mail-desc"><?= $nor->text_rule ?></span> 
                                                                </div>
                                                            <?php endforeach ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingFour"> <a class="collapsed font-bold panel-title" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Posibles respuestas al afiliado </a> </div>
                                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                                <div class="panel-body">
                                                    <div class="comment-center col-md-12">
                                                        <div class="comment-body col-md-3">
                                                            <?php foreach ($resp as $re): ?>
                                                                <div class="mail-contnet">
                                                                    <h5><?= $re->title_posible_answer ?></h5> 
                                                                    <span class="mail-desc"><?= $re->text_posible_answer ?></span> 
                                                                </div>
                                                            <?php endforeach ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingFive"> <a class="collapsed font-bold panel-title" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Videos de Ejemplo </a> </div>
                                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                                <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, inable VHS. </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingSix"> <a class="collapsed font-bold panel-title" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">Preguntas Frecuentes</a> </div>
                                            <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                <div class="panel-body">
                                                    <div class="comment-center col-md-12">
                                                        <div class="comment-body col-md-3">
                                                            <?php foreach ($faq as $fa): ?>
                                                                <div class="mail-contnet">
                                                                    <h5><?= $fa->title_faq ?></h5> 
                                                                    <span class="mail-desc"><?= $fa->answer_faq ?></span> 
                                                                </div>
                                                            <?php endforeach ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>

                        </div>
                    </div>
                </div>
                <!-- .row -->
            
                <div id="content"></div>
            </div>


            <div class="modal fade" id="modalAyuda">
                    
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">×</button>
                            <h4 class="modal-title">Añadir Nuevo Elemento</h4>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="tipo" class="control-label">Tipo</label>
                                <select name="tipo" id="tipo" class="form-control" required>
                                    <option value="">Tipo</option>
                                    <option value="1">Audio</option>
                                    <option value="6">Video</option>
                                    <option value="2">Mensajes de Texto</option>
                                    <option value="3">Normas</option>
                                    <option value="4">Posibles Respuestas</option>
                                    <option value="5">Preguntas Frecuentes</option>
                                </select>
                            </div>

                            <div class="row" id="contentHelp">
                                
                            </div>
                        </div>
                        </div>
                    </div>
                </div>


