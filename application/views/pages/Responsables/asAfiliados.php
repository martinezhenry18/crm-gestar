


				<div class="table-responsive">
                    <table id="asignaTable" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre Afiliado</th>
                                <th>DNI</th>
                                <th>Comuna</th>
                                <th>Asignar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($afiliados as $key): ?>
                            <tr>
                                <td><?= $key->names_affiliate ?> <?= $key->last_names_affiliate ?></td>
                                <td><?= $key->dni_affiliate ?></td>
                                <td>Comuna <?= $key->commune_affiliate ?></td>
                                <td>
                                    <a href="#" title="Asignar" style="margin-right: 10px;" onclick="asignaAfiliado(<?= $key->id_affiliate ?>,<?= $vendedor ?>);">
                                        <i class="fa fa-check" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i>
                                    </a>
                                </td>
                            </tr>   
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>

                <script>
                		$(document).ready(function() {
							$('#asignaTable').DataTable({
								"lengthMenu": [[5,10,15,20,25, -1], [5,10,15,20,25, "Todos"]]
							});
						});


                        function asignaAfiliado(afiliado,responsable) 
                        {
                            var params = {'vendor_id' : responsable, 'affiliate_id' : afiliado};
                            $.ajax({
                                url: '<?= base_url("asignaAffiliate") ?>',
                                type: 'POST',
                                data: params,
                                success: function () {
                                    swal({   
                                        title: "Afiliado Asignado",
                                        timer: 2000,
                                        showConfirmButton: true 
                                    });
                                    cargar('#TableAff','<?= base_url('tAfiliados') ?>/'+responsable);
                                    cargar($("#contentAsignaAfiliados"),'<?= base_url("asignaAfiliados") ?>/'+responsable);
                                }  
                            }); 
                        }
                </script>