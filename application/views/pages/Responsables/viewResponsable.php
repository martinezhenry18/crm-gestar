


<?php foreach ($vendedor as $vd): ?>


                        <h4 class="modal-title">Vendedor <?= $vd->name_vendor ?></h4>
                    </div>
                    <div class="modal-body">
                    	<div class="col-md-12 col-xs-12">
	                        <div class="white-box">
	                            <ul class="nav customtab nav-tabs" role="tablist">
	                                <li role="presentation" class="nav-item"><a href="#home" class="nav-link active" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs"> Actividad</span></a></li>
	                                <li role="presentation" class="nav-item"><a href="#profile" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Perfil</span></a></li>
	                                <li role="presentation" class="nav-item"><a href="#messages" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Mensajes</span></a></li>
	                                <li role="presentation" class="nav-item"><a href="#asignados" class="nav-link" aria-controls="asignados" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-th-list"></i></span> <span class="hidden-xs">Asignados</span></a></li>
	                            </ul>
	                            <div class="tab-content">
	                                <div class="tab-pane active" id="home">
	                                    <div class="row row-in">
			                                <div class="col-lg-6 col-sm-6 row-in-br">
			                                    <div class="col-in row">
			                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-thumb-up"></i>
			                                            <h5 class="text-muted vb">Votos Logrados</h5> </div>
			                                        <div class="col-md-6 col-sm-6 col-xs-6">
			                                            <h3 class="counter text-right m-t-15 text-danger">23</h3> 
			                                        </div>
			                                    </div>
			                                </div>
			                                <div class="col-lg-6 col-sm-6 row-in-br  b-r-none">
			                                    <div class="col-in row">
			                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-pencil-alt"></i>
			                                            <h5 class="text-muted vb">Contactados</h5> </div>
			                                        <div class="col-md-6 col-sm-6 col-xs-6">
			                                            <h3 class="counter text-right m-t-15 text-info">169</h3> 
			                                        </div>
			                                    </div>
			                                </div>
			                                
			                            </div>
	                                </div>
	                                <div class="tab-pane" id="profile">
	                                    <div class="row">
	                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Nombre</strong>
	                                            <br>
	                                            <p class="text-muted"><?= $vd->name_vendor ?></p>
	                                        </div>
	                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Telefono</strong>
	                                            <br>
	                                            <p class="text-muted"><?= $vd->phone_vendor ?></p>
	                                        </div>
	                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
	                                            <br>
	                                            <p class="text-muted"><?= $vd->email_vendor ?></p>
	                                        </div>
	                                        <div class="col-md-3 col-xs-6"> <strong>Direccion</strong>
	                                            <br>
	                                            <p class="text-muted"><?= $vd->direction_vendor ?></p>
	                                        </div>
	                                    </div>
	                                    <hr>
	                                    <h4 class="font-bold m-t-30">Modificar Datos</h4>
	                                    <hr>
	                                    <form action="#" onsubmit="return false;" id="profile-vendor">
	                                    	<div class="row">
	                                    		<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Nombre</label>
		                                    			<input type="text" class="form-control" value="<?= $vd->name_vendor ?>" name="name_vendor">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">DNI</label>
		                                    			<input type="text" class="form-control" value="<?= $vd->dni_vendor ?>" name="dni_vendor">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Telefono</label>
		                                    			<input type="text" class="form-control" value="<?= $vd->phone_vendor ?>" name="phone_vendor">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Direccion</label>
		                                    			<input type="text" class="form-control" value="<?= $vd->direction_vendor ?>" name="direction_vendor">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Correo</label>
		                                    			<input type="text" class="form-control" value="<?= $vd->email_vendor ?>" name="email_vendor">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Sexo</label>
		                                    			<select name="sex_vendor" id="sex_vendor" class="form-control">
		                                    				<option value="">Sexo</option>
		                                    				<option value="Masculino" <?php if ($vd->sex_vendor == "Masculino"): ?>selected<?php endif ?>>Masculino</option>
		                                    				<option value="Femenino" <?php if ($vd->sex_vendor == "Femenino"): ?>selected<?php endif ?>>Femenino</option>
		                                    			</select>
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Edad</label>
		                                    			<input type="text" class="form-control" value="<?= $vd->age_vendor ?>" name="age_vendor">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for=""></label>
		                                    			<a href="#" class="btn btn-success" style="margin-top: 26px;" onclick="saveProfileVendor(<?= $vd->id_vendor ?>)">Actualizar Datos</a>
		                                    		</div>
		                                    	</div>
	                                    	</div>
	                                    </form>
	                                </div>
	                                <div class="tab-pane" id="messages">
                                    	<form action="#">
                                    		<div class="row">
                                    			<div class="col-md-6">
                                    				<label for="">Asunto</label>
                                    				<input type="text" placeholder="Asunto" class="form-control" required>
                                    			</div>
                                    			<div class="col-md-6">
                                    				<label for="">Mensaje</label>
                                    				<textarea name="mensaje" rows="3" class="form-control" required></textarea>
                                    			</div>
                                    			<div class="col-md-12">
                                    				<button class="btn btn-success">Enviar Mensaje</button>
                                    			</div>
                                    		</div>
                                    	</form>
	                                </div>
	                                <div class="tab-pane" id="asignados">
                                    	<div class="table-responsive" id="TableAff">
			                                <table id="asTable" class="table table-striped">
			                                    <thead>
			                                        <tr>
			                                            <th>Nombre Afiliado</th>
			                                            <th>DNI</th>
			                                            <th>Comuna</th>
			                                            <th>Acciones</th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                        <?php foreach ($asignados as $key): ?>
			                                        <tr>
			                                            <td><?= $key->names_affiliate ?> <?= $key->last_names_affiliate ?></td>
			                                            <td><?= $key->dni_affiliate ?></td>
			                                            <td>Comuna <?= $key->commune_affiliate ?></td>
			                                            <td>
			                                            	<a href="#" title="Quitar" style="margin-right: 10px;" onclick="removeAffiliate(<?= $key->id_affiliate ?>,<?= $vd->id_vendor ?>)">
			                                            		<i class="fa fa-times" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i>
			                                            	</a>
		                                            	</td>
			                                        </tr>   
			                                        <?php endforeach ?>
			                                    </tbody>
			                                </table>
			                            </div>

                            			<div class="col-md-12">
                            				<a href="#" class="btn btn-success" onclick="asignaAfiliados(<?= $key->vendor_id ?>);">Asignar Afiliados</a>
                            			</div>

                            			<div id="contentAsignaAfiliados" style="margin-top: 80px;">
                            				
                            			</div>
	                                </div>
	                                
	                            </div>
	                        </div>
	                    </div>
                    </div>
                    <div class="modal-footer">
                    	<a href="#" class="btn btn-facebook"><i class="fa fa-facebook"></i></a> 
                    	<a href="#" class="btn btn-twitter"><i class="fa fa-twitter"></i></a> 
                    	<a href="#" class="btn btn-instagram"><i class="fa fa-instagram"></i></a> 
                    	<a href="#" class="btn btn-github" style="margin-right: 80px;"><i class="fa fa-envelope"></i></a>
                    	
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cerrar Ventana</button>
                    </div>

<?php endforeach ?>

<script>
	
	$(document).ready(function() {
        $('#asTable').DataTable({
        	"lengthMenu": [[5,10,15,20,25, -1], [5,10,15,20,25, "Todos"]]
        });
	});

	function asignaAfiliados(id_vendor) 
	{
		cargar($("#contentAsignaAfiliados"),'<?= base_url("asignaAfiliados") ?>/'+id_vendor);	
	}

	function saveProfileVendor(id_vendor) 
	{
        $.ajax({
            url: '<?= base_url("updateProfileVendor") ?>/'+id_vendor,
            type: 'POST',
            data: $("#profile-vendor").serialize(),
            success: function () {
                swal({   
                    title: "Usuario Habilitado",
                    timer: 2000,
                    showConfirmButton: true 
                });
                window.location.reload();
            }  
        });   
	}

	function removeAffiliate(afiliado,responsable) 
	{
		var params = {'vendor_id' : responsable, 'affiliate_id' : afiliado};
		$.ajax({
            url: '<?= base_url("removeAffiliate") ?>',
            type: 'POST',
            data: params,
            success: function () {
                swal({   
                    title: "Afiliado Removido",
                    timer: 2000,
                    showConfirmButton: true 
                });
                cargar('#TableAff','<?= base_url('tAfiliados') ?>/'+responsable);
            }  
        });	
	}

</script>