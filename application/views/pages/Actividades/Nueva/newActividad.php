        
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Nueva Actividad</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gesta</a></li>
                            <li class="#">Actividades</li>
                            <li class="active">Nueva Actividad</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <form action="#" method="POST" onsubmit="saveActividad();return false;" id="form-save-actividad" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Titulo Actividad</label>
                                                        <input type="text" name="name_activity" class="form-control" placeholder="Titulo" required id="name_activity">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Descripcion Actividad</label>
                                                        <input type="text" name="description_activity" class="form-control" placeholder="Descripcion" required id="description_activity">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Fecha de Actividad</label>
                                                        <input type="date" class="form-control" required name="date_activity" id="date_activity">
                                                    </div>
                                                </div>
                                                <!--/span-->

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Hora de Actividad</label>
                                                        <input type="time" class="form-control" required name="hour_activity" id="hour_activity">
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label">Direccion de Actividad</label>
                                                        <textarea name="address_activity" rows="5" placeholder="Direccion" class="form-control" id="address_activity" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                            
                                            <h3 class="box-title m-t-40">Filtros Sugeridos</h3>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                        <label class="control-label" style="margin-bottom: 21px;">Edad</label>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="Hasta 30" id="age_activity_1">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Hasta 30</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 30 a 40" id="age_activity_2">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 30 a 40</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 40 a 50" id="age_activity_3">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 40 a 50</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for=""></label>
                                                            <div class="form-check" style="margin-top: 25px;">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 50 a 60" id="age_activity_4">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 50 a 60</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="De 60 en adelante" id="age_activity_5">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">De 60 en adelante</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="age_activity[]" value="Todas" id="age_activity_6">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">todas</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">Genero</label>
                                                        <select class="form-control" data-placeholder="Seleccionar un Genero" tabindex="1" name="genre_activity" id="genre_activity">
                                                            <option value="Masculino">Masculino</option>
                                                            <option value="Femenino">Femenino</option>
                                                            <option value="Indistinto">Indistinto</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-md-6">
                                                        <label class="control-label" style="margin-top: 30px; margin-bottom: 20px;">Categoria</label>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Voto Confirmado" id="category_activity_1">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Voto Confirmado</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Posible Voto" id="category_activity_2">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Posible Voto</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Posible Candidato" id="category_activity_3">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Posible Candidato</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Candidato Confirmado" id="category_activity_4">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Candidato Confirmado</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label for=""></label>
                                                            <div class="form-check bd-example-indeterminate" style="margin-top: 50px;">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Voto Contrario" id="category_activity_5">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Voto Contrario</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Compañero" id="category_activity_6">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Compañero</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="category_activity[]" value="Indistinto" id="category_activity_7">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Indistinto</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                        <label class="control-label" style="margin-bottom: 24px;">Comuna</label>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="1" id="commune_activity_1">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">1</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="2" id="commune_activity_2">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">2</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="3" id="commune_activity_3">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">3</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="4" id="commune_activity_4">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">4</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="margin-top: 45px;">
                                                            <div class="form-check bd-example-indeterminate">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="5" id="commune_activity_5">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">5</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="6" id="commune_activity_6">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">6</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="7" id="commune_activity_7">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">7</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="8" id="commune_activity_8">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">8</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="margin-top: 45px;">
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="9" id="commune_activity_9">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">9</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="10" id="commune_activity_10">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">10</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="11" id="commune_activity_11">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">11</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="12" id="commune_activity_12">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">12</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3" style="margin-top: 45px;">
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="13" id="commune_activity_13">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">13</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="14" id="commune_activity_14">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">14</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="15" id="commune_activity_15">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">15</span>
                                                                </label>
                                                            </div>
                                                            <div class="form-check">
                                                                <label class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input" name="commune_activity[]" value="Indistinto" id="commune_activity_16">
                                                                    <span class="custom-control-indicator"></span>
                                                                    <span class="custom-control-description">Indistinto</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6" style="    margin-top: 29px;">
                                                    <div class="form-group">
                                                        <label>Tipo de Actividad</label>
                                                        <select name="type_activity" class="form-control" id="type_activity">
                                                            <option value="">Seleccionar tipo</option>
                                                            <option value="1">Actividad Permanente</option>
                                                            <option value="2">Actividad Puntual</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6" style="    margin-top: 29px;">
                                                    <div class="form-group">
                                                        <label>Imagen de Actividad</label>
                                                        <input type="file" class="form-control" name="image_activity" id="image_activity" accept=".jpg, .jpeg, .png" required>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-success" id="but"> <i class="fa fa-check"></i> Guardar</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->

<script>



    function saveActividad() 
    {
        var params = {
            'name_activity' : $("#name_activity").val(),
            'description_activity' : $("#description_activity").val(),
            'date_activity' : $("#date_activity").val(),
            'hour_activity' : $("#hour_activity").val(),
            'address_activity' : $("#address_activity").val(),
            'age_activity[]' : [],
            'genre_activity' : $("#genre_activity").val(),
            'category_activity[]' : [],
            'commune_activity[]' : [],
            'type_activity' : $("#type_activity").val(),
            'image_activity' : 'files/actividad/imagenes/'+document.getElementById('image_activity').files[0].name
        };
        $('input[name="age_activity[]"]:checked').each(function() {
          params['age_activity[]'].push($(this).val());
        });
        $('input[name="category_activity[]"]:checked').each(function() {
          params['category_activity[]'].push($(this).val());
        });
        $('input[name="commune_activity[]"]:checked').each(function() {
          params['commune_activity[]'].push($(this).val());
        });

        

        $.ajax({
            url: '<?= base_url('saveActividad') ?>',
            type: 'POST',
            data: params,
            success: function () {
                saveFileAudio('<?= base_url('saveImageActivity') ?>');
                swal({   
                    title: "Actividad Almacenada",     
                    showConfirmButton: true 
                });
                window.location.reload();
            }  
        });
    }

    function saveFileAudio(ruta) 
    {
        var archivos = document.getElementById('image_activity');
        
        var archivo = archivos.files;
        var arch = new FormData();
        
        if (archivo.length > 0)
        {
            for (var i = 0; i < archivo.length; i++) 
            {
                arch.append('archivo'+i,archivo[i]);
            }

            $.ajax({
                url: ruta,
                type:'POST',
                contentType:false,
                data:arch,
                processData:false,
                cache:false
            });
        }
    }
</script>