




              <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Principal</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Panel Escritorio</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="calendar col-lg-12" id="calendar">

                    </div>
                </div>
              






 

  
            <div class="modal fade" id="modaldos">
              <div class="modal-dialog" style="padding-top: 150px;">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="modal-title"></h4>
                      </div>
                      <div class="modal-body">
                          <div class="error" id="error"></div>
                          <form class="form-horizontal" id="crud-form">
                          <input type="hidden" id="start" class="start">
                          <input type="hidden" id="end" class="end">
                              <div class="form-group">
                                  <label class="col-md-12 control-label" for="titledos">Titulo</label>
                                  <div class="col-md-12">
                                      <input id="titledos" name="title" type="text" class="form-control " readonly />
                                  </div>
                              </div>                            
                              <div class="form-group">
                                  <label class="col-md-12 control-label" for="descriptiondos">Descripcion</label>
                                  <div class="col-md-12">
                                      <textarea class="form-control" id="descriptiondos" name="descriptiondos" readonly></textarea>
                                  </div>
                              </div>
                              <div class="form-group">
                                <label for="findos" class="col-md-12 control-label">Fecha</label>
                                <div class="col-md-12">
                                  <input type="text" name="findos" id="fecha" class="form-control" readonly/>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="iniciodos" class="col-md-12 control-label">Hora Inicio</label>
                                <div class="col-md-12">
                                  <input type="time" name="iniciodos" id="iniciodos" class="form-control" readonly />
                                </div>
                              </div>

                          </form>
                      </div>
                      <div class="modal-footer" id="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                      </div>
                  </div>
              </div>
            </div>


            </div>