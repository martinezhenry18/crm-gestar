


				<div class="table-responsive">
                    <table id="asignaTable" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Nombre Responsable</th>
                                <th>DNI</th>
                                <th>Email</th>
                                <th>Asignar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($responsables as $key): ?>
                            <tr>
                                <td><?= $key->name_vendor ?></td>
                                <td><?= $key->dni_vendor ?></td>
                                <td><?= $key->email_vendor ?></td>
                                <td><a href="#" title="Asignar" style="margin-right: 10px;" onclick="asignaResponsable(<?= $afiliado ?>,<?= $key->id_vendor ?>);"><i class="fa fa-check" style="background: #01c0c8; padding: 12px;color: white !important; border-radius: 5px;"></i></a></td>
                            </tr>   
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>

                <script>
            		$(document).ready(function() {
						$('#asignaTable').DataTable({
							"lengthMenu": [[5,10,15,20,25, -1], [5,10,15,20,25, "Todos"]]
						});
					});

                    function asignaResponsable(afiliado,responsable) 
                    {
                        params = {'vendor_id' : responsable , 'affiliate_id' : afiliado};
                        $.ajax({
                            url: '<?= base_url("addResponsable") ?>',
                            type: 'POST',
                            data: params,
                            success: function (data) {
                                $("#but").attr("hidden","true");
                                swal({   
                                    title: "El Responsable Fue Añadido",     
                                    showConfirmButton: true 
                                });
                                addRowResponsables(afiliado,responsable,data);
                                $("#contentAsignaResponsable").html("");
                            }  
                        });
                    }

                    function addRowResponsables (afiliado,responsable,data) 
                    {
                        all = JSON.parse(data);
                        $("#asTable tr:last").remove();
                        var tds = '<tr>';
                        
                        tds += '<td>'+all[0].name_vendor+'</td>';
                        tds += '<td>'+all[0].dni_vendor+'</td>';
                        tds += '<td>'+all[0].email_vendor+'</td>';
                        
                        tds += '<td><a href="#" title="Eliminar/Inhabilitar" style="margin-right: 10px;" onclick="removeResp('+afiliado+','+responsable+');"><i class="fa fa-times text-danger" style="background: #01c0c8; padding: 12px; margin-top: -10px;color: white !important; border-radius: 5px;"></i></a></td>'
                        tds += '</tr>';
                        $("#asTable").append(tds);
                    }
                </script>