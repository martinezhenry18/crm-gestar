        
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Agregar Afiliado</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gesta</a></li>
                            <li class="active">Nuevo Afiliado</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Nuevo Afiliado</h3>
                            <small>Por Favor descargue el archivo ejemplo de importación (.CSV)</small>
                            <br>
                                <a href="<?= base_url('files/afiliado.xlsx') ?>" download="Afiliado" class="btn btn-primary" style="margin-top: 20px;">Descargar Excel Demo</a>
                            <br>
                            <br>
                            <div class="form-group">
                                <input type="file" id="input-file-now" class="dropify" />
                            </div>
                            <br>
                            <a href="#" class="btn btn-success" onclick="saveAfiliado();">Importar Excel Datos</a>
                            <br><br>
                            <small>Recordar que si no va a agregar valor a alguna columna, dejar por defecto (0), para asi evitar inconsistencias<br>Si existen filas en el documento iguales o similares a la base de datos, estas filas serán actualizadas para no perder la información</small>
                        </div>
                    </div>
                </div>
                <!-- .row -->
            
                <div id="content"></div>
            </div>    

                <script>
                    function saveAfiliado() 
                    {
                        var archivos = document.getElementById('input-file-now');
                        
                        var archivo = archivos.files;

                        var arch = new FormData();
                        
                        if (archivo.length > 0)
                        {
                            for (var i = 0; i < archivo.length; i++) 
                            {
                                arch.append('archivo'+i,archivo[i]);
                            }

                            $.ajax({
                                url: '<?= base_url("readAfiliado") ?>',
                                type:'POST',
                                contentType:false,
                                data:arch,
                                processData:false,
                                cache:false,
                                success: function (data) {
                                    window.alert("Archivo Leido Correctamente");
                                    $('#content').html(data);
                                }
                            });
                        } else {
                            alert("Debe Arrastrar o Seleccionar un Archivo");
                        }
                    }
                </script>

