





<?php foreach ($afiliado as $af): ?>


                        <h4 class="modal-title">Afiliado <?= $af->names_affiliate ?> - <span class="label label-table" style="background-color: <?= $af->color_status ?>"><?= $af->name_status ?></span></h4>
                    </div>
                    <div class="modal-body">
                    	<div class="col-md-12 col-xs-12">
	                        <div class="white-box">
	                            <ul class="nav customtab nav-tabs" role="tablist">
	                                <li role="presentation" class="nav-item">
	                                	<a href="#actividad" class="nav-link active" aria-controls="actividad" role="tab" data-toggle="tab" aria-expanded="true">
	                                		<span class="visible-xs">
	                                			<i class="fa fa-home"></i>
	                                		</span>
	                                		<span class="hidden-xs"> Historial</span>
	                                	</a>
	                                </li>
	                                <li role="presentation" class="nav-item">
	                                	<a href="#profile" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true">
	                                		<span class="visible-xs">
	                                			<i class="fa fa-home"></i>
	                                		</span>
	                                		<span class="hidden-xs"> Datos Generales</span>
	                                	</a>
	                                </li>
	                                <li role="presentation" class="nav-item">
	                                	<a href="#home" class="nav-link" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">
	                                		<span class="visible-xs">
	                                			<i class="fa fa-user"></i>
	                                		</span> 
	                                		<span class="hidden-xs">Datos de Contacto</span>
	                                	</a>
	                                </li>
	                                <li role="presentation" class="nav-item">
	                                	<a href="#messages" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false">
	                                		<span class="visible-xs">
	                                			<i class="fa fa-envelope-o"></i>
	                                		</span> 
	                                		<span class="hidden-xs">Categorizar</span>
	                                	</a>
	                                </li>
	                                <li role="presentation" class="nav-item">
	                                	<a href="#asignados" class="nav-link" aria-controls="asignados" role="tab" data-toggle="tab" aria-expanded="false">
	                                		<span class="visible-xs">
	                                			<i class="fa fa-th-list"></i>
	                                		</span> 
	                                		<span class="hidden-xs">Responsable</span>
	                                	</a>
	                                </li>
	                            </ul>
	                            <div class="tab-content">
	                            	<div class="tab-pane active" id="actividad">
	                            		<div class="white-box">
				                            <h3 class="box-title">Contactos con el Afiliado</h3>
				                            <div class="steamline">
				                                <div class="sl-item">
				                                    <div class="sl-left"> </div>
				                                    <div class="sl-right">
				                                        <div><a href="#"><?= date('d-m-Y') ?></a> </div>
				                                        <p>Via: WhatsApp</p>
				                                    </div>
				                                </div>
				                                <div class="sl-item">
				                                    <div class="sl-left"> </div>
				                                    <div class="sl-right">
				                                        <div><a href="#"><?= date('d-m-Y') ?></a> </div>
				                                        <p>Via: Visita</p>
				                                    </div>
				                                </div>
				                                <div class="sl-item">
				                                    <div class="sl-left"> </div>
				                                    <div class="sl-right">
				                                        <div><a href="#"> <?= date('d-m-Y') ?></a> </div>
				                                        <p>Via: Mensajes de Texto</p>
				                                    </div>
				                                </div>
				                                <div class="sl-item">
				                                    <div class="sl-left"> </div>
				                                    <div class="sl-right">
				                                        <div><a href="#"><?= date('d-m-Y') ?></a> </div>
				                                        <p>Via: Llamada</p>
				                                    </div>
				                                </div>
				                                <div class="sl-item">
				                                    <div class="sl-left"> </div>
				                                    <div class="sl-right">
				                                        <div><a href="#"><?= date('d-m-Y') ?></a> </div>
				                                        <p>Via: Llamada</p>
				                                    </div>
				                                </div>
				                            </div>
				                        </div>
	                            	</div>
	                                <div class="tab-pane" id="profile">
	                                    <hr>
	                                    <form id="general-data-affiliate" onsubmit="return false;">
	                                    	<div class="row">
	                                    		<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Nombres</label>
		                                    			<input type="text" class="form-control" value="<?= $af->names_affiliate ?>"  name="names_affiliate">
		                                    		</div>
		                                    	</div>
	                                    		<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Apellidos</label>
		                                    			<input type="text" class="form-control" value="<?= $af->last_names_affiliate ?>"  name="last_names_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">DNI</label>
		                                    			<input type="text" class="form-control" value="<?= $af->dni_affiliate ?>"  name="dni_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Sexo</label>
		                                    			<select name="gener_affiliate" id="gener_affiliate" class="form-control">
		                                    				<option value="">Sexo</option>
		                                    				<option value="Masculino" <?php if ($af->gener_affiliate == "Masculino"): ?>selected<?php endif ?>>Masculino</option>
		                                    				<option value="Femenino" <?php if ($af->gener_affiliate == "Femenino"): ?>selected<?php endif ?>>Femenino</option>
		                                    			</select>
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Edad</label>
		                                    			<input type="text" class="form-control" value="<?= $af->age_affiliate ?>"  name="age_affiliate">
		                                    		</div>
		                                    	</div>
	                                    		<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Numero</label>
		                                    			<input type="text" class="form-control" value="<?= $af->number_affiliate ?>"  name="number_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Calle</label>
		                                    			<input type="text" class="form-control" value="<?= $af->street_affiliate ?>"  name="street_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Distrito</label>
		                                    			<input type="text" class="form-control" value="<?= $af->dtto_affiliate ?>"  name="dtto_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Codigo Postal</label>
		                                    			<input type="text" class="form-control" value="<?= $af->postal_code_affiliate ?>"  name="postal_code_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Estudios</label>
		                                    			<input type="text" class="form-control" value="<?= $af->studies_affiliate ?>"  name="studies_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Trabajo</label>
		                                    			<input type="text" class="form-control" value="<?= $af->job_affiliate ?>"  name="job_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Circuito</label>
		                                    			<input type="text" class="form-control" value="<?= $af->circuit_affiliate ?>"  name="circuit_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Comuna</label>
		                                    			<input type="text" class="form-control" value="<?= $af->commune_affiliate ?>"  name="commune_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for=""></label>
		                                    			<a href="#" class="btn btn-success" style="margin-top: 26px;" onclick="saveGeneralData(<?= $af->id_affiliate ?>);">Actualizar Datos</a>
		                                    		</div>
		                                    	</div>
	                                    	</div>
	                                    </form>
	                                </div>
	                                <div class="tab-pane" id="home">
	                                    <form onsubmit="return false;" id="contact-data-affiliate">
	                                    	<div class="row">
	                                    		<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Telefono 1</label>
		                                    			<input type="text" class="form-control" value="<?= $af->phone_1_affiliate ?>"  name="phone_1_affiliate">
		                                    		</div>
		                                    	</div>
	                                    		<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Telefono 2</label>
		                                    			<input type="text" class="form-control" value="<?= $af->phone_2_affiliate ?>"  name="phone_2_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Comprobado</label>
		                                    			<input type="text" class="form-control" value="<?= $af->checked_phone ?>"  name="checked_phone">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Movil 1</label>
		                                    			<input type="text" class="form-control" value="<?= $af->mobile_1_affiliate ?>"  name="mobile_1_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Movil 2</label>
		                                    			<input type="text" class="form-control" value="<?= $af->mobile_2_affiliate ?>"  name="mobile_2_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Movil 3</label>
		                                    			<input type="text" class="form-control" value="<?= $af->mobile_3_affiliate ?>"  name="mobile_3_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Movil 4</label>
		                                    			<input type="text" class="form-control" value="<?= $af->mobile_4_affiliate ?>"  name="mobile_4_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Movil 5</label>
		                                    			<input type="text" class="form-control" value="<?= $af->mobile_5_affiliate ?>"  name="mobile_5_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Movil 6</label>
		                                    			<input type="text" class="form-control" value="<?= $af->mobile_6_affiliate ?>"  name="mobile_6_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Facebook</label>
		                                    			<input type="text" class="form-control" value="<?= $af->facebook_affiliate ?>"  name="facebook_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Twitter</label>
		                                    			<input type="text" class="form-control" value="<?= $af->twitter_affiliate ?>"  name="twitter_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Instagram</label>
		                                    			<input type="text" class="form-control" value="<?= $af->instagram_affiliate ?>"  name="instagram_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for="">Email</label>
		                                    			<input type="text" class="form-control" value="<?= $af->email_affiliate ?>"  name="email_affiliate">
		                                    		</div>
		                                    	</div>
		                                    	<div class="col-md-3">
		                                    		<div class="form-group">
		                                    			<label for=""></label>
		                                    			<a href="#" class="btn btn-success" style="margin-top: 26px;" onclick="saveContactData(<?= $af->id_affiliate ?>)">Actualizar Datos</a>
		                                    		</div>
		                                    	</div>
	                                    	</div>
	                                    </form>
	                                </div>
	                                <div class="tab-pane" id="messages">
                                    	<form onsubmit="return false;" id="categorizacion-data-affiliate">
                                    		<div class="row">
                                    			<div class="col-md-6">
                                    				<label for="">Fecha de Contacto</label>
                                    				<input type="date" class="form-control" required value="<?= date_format(date_create($af->date_contact_affiliate),'Y-m-d') ?>" name="date_contact_affiliate">
                                    			</div>
                                    			<div class="col-md-6">
                                    				<label for="">Canal de Contacto</label>
                                    				<select name="contact_id" id="contact_id" class="form-control">
                                    					<option value="">Seleccionar</option>
                                    					<?php foreach ($tipo_contacto as $tp): ?>
                                    					<option value="<?= $tp->id_contacts_type ?>" <?php if ($af->contact_id == $tp->id_contacts_type): ?>selected<?php endif ?>><?= $tp->name_contacts_type ?></option>
                                    					<?php endforeach ?>
                                    				</select>
                                    			</div>
                                    			<div class="col-md-6">
                                    				<label for="">Estatus Afiliafo</label>
                                    				<select name="status_affiliate" id="status_affiliate" class="form-control">
                                    					<option value="">Seleccionar</option>
                                    					<?php foreach ($status as $st): ?>
                                    						<option value="<?= $st->id_status ?>" <?php if ($af->status_affiliate == $st->id_status): ?>selected<?php endif ?>><?= $st->name_status ?></option>
                                    					<?php endforeach ?>
                                    				</select>
                                    			</div>
                                    			<div class="col-md-12">
                                    				<label for="">Comentarios</label>
                                    				<textarea name="comments_affiliate" rows="3" class="form-control coments" required><?= $af->comments_affiliate ?></textarea>
                                    			</div>
                                    			<div class="col-md-12">
                                    				<button class="btn btn-success" style="margin-top: 60px;" onclick="saveCategorizacionData(<?= $af->id_affiliate ?>)">Guardar</button>
                                    			</div>
                                    		</div>
                                    	</form>
	                                </div>
	                                <div class="tab-pane" id="asignados">
                                    	<div class="table-responsive">
			                                <table id="asTable" class="table table-striped">
			                                    <thead>
			                                        <tr>
			                                            <th>Nombre de Responsable</th>
			                                            <th>DNI Responsable</th>
			                                            <th>Email Responsable</th>
			                                            <th>Quitar</th>
			                                        </tr>
			                                    </thead>
			                                    <tbody>
			                                        <tr>
			                                        	<td><?= $af->name_vendor ?></td>
			                                        	<td><?= $af->dni_vendor ?></td>
			                                        	<td><?= $af->email_vendor ?></td>
			                                        	<td><a href="#" title="Eliminar/Inhabilitar" style="margin-right: 10px;" onclick="removeResp(<?= $af->id_vendor ?>,<?= $af->id_affiliate ?>);"><i class="fa fa-times text-danger" style="background: #01c0c8; padding: 12px; margin-top: -10px;color: white !important; border-radius: 5px;"></i></a></td>
			                                        </tr>
			                                    </tbody>
			                                </table>
			                            </div>

                            			<div class="col-md-12">
                            				<a href="#" class="btn btn-success" onclick="asignaResponsable(<?= $af->id_affiliate ?>)" <?php if ($af->id_vendor != null): ?>hidden<?php endif ?> id="but">Asignar a un Responsable</a>
                            			</div>

                            			<div id="contentAsignaResponsable" style="margin-top: 80px;">
                            				
                            			</div>
	                                </div>
	                                
	                            </div>
	                        </div>
	                    </div>
                    </div>
                    <div class="modal-footer">
                    	<a href="http://facebook.com/<?= $af->facebook_affiliate ?>" class="btn btn-facebook" target="_blank"><i class="fa fa-facebook"></i></a> 
                    	<a href="http://twitter.com/<?= $af->twitter_affiliate ?>" class="btn btn-twitter" target="_blank"><i class="fa fa-twitter"></i></a> 
                    	<a href="http://instagram.com/<?= $af->instagram_affiliate ?>" class="btn btn-instagram" target="_blank"><i class="fa fa-instagram"></i></a> 
                    	<a href="#" class="btn btn-github" style="margin-right: 80px;" target="_blank"><i class="fa fa-envelope"></i></a>
                    	
                        <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cerrar Ventana</button>
                    </div>

<?php endforeach ?>

<script>
	
	function removeResp(vendor, afiliado) 
	{
		params = {'vendor_id' : vendor , 'affiliate_id' : afiliado};
		$.ajax({
            url: '<?= base_url("removeResponsable") ?>',
            type: 'POST',
            data: params,
            success: function () {
                swal({   
                    title: "El Responsable Fue Removido",     
                    showConfirmButton: true 
                });
                $("#asTable tr:last").remove();
				$("#but").removeAttr("hidden");
            }  
        });
	}

	function asignaResponsable(id_afiliado) 
	{
		cargar($("#contentAsignaResponsable"),'<?= base_url("asignaResponsable") ?>/'+id_afiliado);	
	}

	$(document).ready(function() {

        $('.coments').wysihtml5();


    });


    function saveGeneralData(affiliate) 
    {
    	$.ajax({
            url: '<?= base_url("saveGeneralData") ?>/'+affiliate,
            type: 'POST',
            data: $("#general-data-affiliate").serialize(),
            success: function () {
                swal({   
                    title: "Datos generales Actualizados",     
                    showConfirmButton: true 
                });
            }  
        });
    }


    function saveContactData(affiliate) 
    {
    	$.ajax({
            url: '<?= base_url("saveContactData") ?>/'+affiliate,
            type: 'POST',
            data: $("#contact-data-affiliate").serialize(),
            success: function () {
                swal({   
                    title: "Datos de Contacto Actualizados",    
                    showConfirmButton: true 
                });
            }  
        });
    }

    function saveCategorizacionData(affiliate) 
    {
    	$.ajax({
            url: '<?= base_url("saveCategorizationData") ?>/'+affiliate,
            type: 'POST',
            data: $("#categorizacion-data-affiliate").serialize(),
            success: function () {
                swal({   
                    title: "Datos de Categorizacion Actualizados",    
                    showConfirmButton: true 
                });
            }  
        });
    }

</script>

