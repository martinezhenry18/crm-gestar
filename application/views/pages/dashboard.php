
        
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Principal</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                        <ol class="breadcrumb">
                            <li><a href="<?= base_url() ?>">Crm - Gestar</a></li>
                            <li class="active">Panel Escritorio</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
            
                <div class="row">

                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="white-box">
                            <div class="row row-in">
                                <div class="col-lg-2 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-user"></i>
                                            <h5 class="text-muted vb">Posible Voto</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15 text-danger"><?= $totales['PV'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?= ($totales['PV']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['PV']*100)/$totales['TOT'] ?>%"> <span class="sr-only">40% Complete (success)</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-6 row-in-br  b-r-none">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-pencil-alt"></i>
                                            <h5 class="text-muted vb">Voto Confirmado</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15 text-info"><?= $totales['VC'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?= ($totales['VC']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['VC']*100)/$totales['TOT'] ?>%">  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-mouse-alt"></i>
                                            <h5 class="text-muted vb">Posible Candidato</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15 text-success"><?= $totales['PC'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= ($totales['PC']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['PC']*100)/$totales['TOT'] ?>%">  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-6  row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-receipt"></i>
                                            <h5 class="text-muted vb">Candidato Confirmado</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15 text-warning"><?= $totales['CC'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?= ($totales['CC']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['CC']*100)/$totales['TOT'] ?>%">  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-6 row-in-br">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-mouse-alt"></i>
                                            <h5 class="text-muted vb">Voto Contrario</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15 text-success"><?= $totales['VCO'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= ($totales['VCO']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['VCO']*100)/$totales['TOT'] ?>%">  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2 col-sm-6  b-0">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="ti-receipt"></i>
                                            <h5 class="text-muted vb">Compañero</h5> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h4 class="counter text-right m-t-15 text-warning"><?= $totales['CO'] ?></h4> </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?= ($totales['CO']*100)/$totales['TOT'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?= ($totales['CO']*100)/$totales['TOT'] ?>%">  </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--row -->
                <div class="row">
                    <h4></h4>
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Listado de Afiliados </h3>
                            <div class="table-responsive">
                                <table id="homeTable" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th> Nombre </th>
                                            <th> Apellido </th>
                                            <th> Edad </th>
                                            <th> DNI </th>
                                            <th> Sexo </th>
                                            <th> Comuna </th>
                                            <th> Categorizado </th>
                                            <th> Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($afiliados as $key): ?>
                                        <tr>
                                            <td><?= $key->names_affiliate ?></td>
                                            <td><?= $key->last_names_affiliate ?></td>
                                            <td>Edad <?= $key->age_affiliate ?></td>
                                            <td><?= $key->dni_affiliate ?></td>
                                            <td><?= $key->gener_affiliate ?></td>
                                            <td>Comuna <?= $key->commune_affiliate ?></td>
                                            <td><span class="label label-table" style="background-color: <?= $key->color_status ?>"><?= $key->name_status ?></span></td>
                                            <td>
                                                <a href="#respModal" data-toggle="modal" data-target="#respModal" onclick="cargar('#viewResp','<?= base_url('viewAfiliado/'.$key->id_affiliate) ?>');" title="Ver" style="margin-right: 10px;">
                                                    <i class="fa fa-search" style="background: #01c0c8; padding: 12px; margin-top: -10px;color: white !important; border-radius: 5px;"></i>
                                                </a> 
                                                
                                                <a href="#" title="Eliminar/Inhabilitar" style="margin-right: 10px;" onclick="disabledAffiliate(<?= $key->id_affiliate ?>);">
                                                    <i class="fa fa-times text-danger" style="background: #01c0c8; padding: 12px; margin-top: -10px;color: white !important; border-radius: 5px;"></i>
                                                </a>
                                            </td>
                                        </tr>   
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- .row -->
                
               <div class="modal fade" id="respModal" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar Ventana">×</button>

                                <div id="viewResp"></div>
                        </div>
                        <!-- /.modal-content -->
                    </div>  
                </div>

                <script>
                    function disabledAffiliate(afiliado) 
                    {
                        params = {'id_affiliate' : afiliado};
                        $.ajax({
                            url: '<?= base_url("disabledAffiliate") ?>',
                            type: 'POST',
                            data: params,
                            success: function () {
                                swal({   
                                    title: "Afiliado Eliminado",     
                                    showConfirmButton: true 
                                });
                                window.location.reload();
                            }  
                        });    
                    }
                </script>