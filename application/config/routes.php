<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'confirma_session';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['reporteDudas'] = 'principal/reportarDudas';

//Registro Especial
$route['registroEspecial'] = "registro/especial";
$route['validaCredencial'] = "registro/validaCheck";
$route['completeRegistroEspecial'] = "registro/completeRegistroEspecial";
$route['registroMaster'] = "registro/masterReg";


//Inicio de Sesion
$route['login'] = "login";
$route['principal'] = "principal";
$route['administrador'] = "administrador";
$route['vendedor'] = "vendedor";
$route['inSession'] = "login/inSession";
$route['outSession'] = "login/outSession";
$route['registroVendedor'] = "registro/registroVendedor";



//Afiliado
$route['newAfiliado'] = "afiliado";
$route['readAfiliado'] = "afiliado/newAfiliado";
$route['confirmaSaveAfiliado'] = "afiliado/saveAfiliado";
$route['viewAfiliado/(.+)'] = "afiliado/verAfiliado/$1";
$route['saveGeneralData/(.+)'] = "afiliado/updateData/$1";
$route['saveContactData/(.+)'] = "afiliado/updateData/$1";
$route['saveCategorizationData/(.+)'] = "afiliado/updateCatData/$1";
$route['asignaResponsable/(.+)'] = "afiliado/asignaResponsable/$1";
$route['removeResponsable'] = "afiliado/removeResponsable";
$route['addResponsable'] = "afiliado/addResponsable";
$route['disabledAffiliate'] = "afiliado/disable";


//Responsables
$route['listResponsables'] = "responsables";
$route['viewResponsable/(.+)'] = "responsables/verResponsable/$1";
$route['asignaAfiliados/(.+)'] = "responsables/asignaAfiliados/$1";
$route['disabledResponsable'] = "responsables/disable";
$route['updateProfileVendor/(.+)'] = "responsables/updateVendor/$1";
$route['removeAffiliate'] = "responsables/remove";
$route['tAfiliados/(.+)'] = "responsables/tAfiliados/$1";
$route['asignaAffiliate'] = "responsables/asigna";


//Actividades
$route['newActividad'] = "actividades/newActividad";
$route['saveActividad'] = "actividades/saveActividad";
$route['saveImageActivity'] = "actividades/saveImageActivity";
$route['allActivities'] = "actividades/verActividades";
$route['getActivities'] = "actividades/verActividadesCalendario";
$route['calendarActivities'] = "actividades/verCalendarioActividades";

//Tablas
$route['tablasPrincipales'] = "tablas";
$route['newAdmin'] = "tablas/newAdmin";
$route['viewAdmin/(.+)'] = "tablas/viewAdmin/$1";
$route['saveAdmin'] = "tablas/saveAdmin";
$route['updateAdmin/(.+)'] = "tablas/updateAdmin/$1";
$route['disableAdmin'] = "tablas/disableAdmin";
$route['enableAdmin'] = "tablas/enableAdmin";

$route['newStatus'] = "tablas/newStatus";
$route['saveStatus'] = "tablas/saveStatus";

//Ayuda
$route['helpSystem'] = "ayuda";
$route['audios'] = "ayuda/viewAudio";
$route['sms'] = "ayuda/viewSms";
$route['normas'] = "ayuda/viewNormas";
$route['resp'] = "ayuda/viewResp";
$route['faq'] = "ayuda/viewFaq";
$route['videos'] = "ayuda/viewVideo";
$route['saveAudio'] = "ayuda/saveAudio";
$route['saveFileAudio'] = "ayuda/saveFileAudio";
$route['saveFaq'] = "ayuda/saveFaq";
$route['saveNorma'] = "ayuda/saveNorma";
$route['savePosResp'] = "ayuda/savePosResp";
$route['saveMensaje'] = "ayuda/saveMensaje";


//Mailer
$route['mailer'] = "mailer";
$route['composeMail'] = "mailer/compose";
$route['viewMail'] = "mailer/view";



//General 
$route['viewResp'] = "general/viewResp";
$route['refuseResp'] = "general/refuseResp";
$route['acceptResp'] = "general/acceptResp";
$route['solicitudesPendientes'] = "general/solicitudesPendientes";

$route['savePass'] = "general/savePass";