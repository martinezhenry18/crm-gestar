<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Afiliado extends SuperController {


	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Afiliado_Model');
		$this->load->model('Principal_Model');
	}

	function addResponsable()
	{
		$this->Afiliado_Model->addResponsable($this->input->post());
		print_r(json_encode($this->Afiliado_Model->viewResp($this->input->post('vendor_id'))));
	}

	public function asignaResponsable($afiliado)
	{
		$data['responsables'] = $this->Afiliado_Model->Responsables();
		$data['afiliado'] = $afiliado;
		$this->load->view('pages/Afiliados/asResponsable', $data);
	}

	function disable()
	{
		$afiliado = $this->input->post('id_affiliate');
		$this->Afiliado_Model->disableAfiliado($afiliado);
	}

	public function index()
	{
		$data['title_page'] = "Agregar Afiliado";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Afiliados/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Afiliados/nuevoAfiliado');
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('pages/Afiliados/scripts');
		$this->load->view('templates/endHtml');
	}

	function newAfiliado()
	{
		foreach ($_FILES as $key) 
		{
			$fallo = 1;
			$file = $key['tmp_name'];

			//load the excel library
			$this->load->library('excel');
			 
			//read file from path
			$objPHPExcel = PHPExcel_IOFactory::load($file);
			 
			//get only the Cell Collection
			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
			 
			foreach ($objPHPExcel->getActiveSheet()->getRowIterator() as $row) {
				 
				$cellIterator = $row->getCellIterator();
				$cellIterator->setIterateOnlyExistingCells(false);
				foreach ($cellIterator as $cell) {
					if ($cell->getRow() > 1 ) 
					{
						if (!$cell->getValue()) 
						{
							$fallo = 0;
							echo 'Inconsistencia en el Archivo a Importar';
							break;								
						} 
					}
					
				}
			}
			
			if ($fallo == 1) 
			{
				//extract to a PHP readable array format
				foreach ($cell_collection as $cell) {
				    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
				    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
				    $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
				 
				    //The header will/should be in row 1 only. of course, this can be modified to suit your need.
				    if ($row == 1) {
				        $header[$row][$column] = $data_value;
				    } else {
				        $arr_data[$row][$column] = $data_value;
				    }
				}
				//send the data in an array format
				$data['header'] = $header;
				$data['values'] = $arr_data;
				echo $this->load->view('pages/Afiliados/successAfiliado', $data, TRUE);
			}
		}
	}

	function removeResponsable()
	{
		$this->Afiliado_Model->removeResponsable($this->input->post());
	}

	function saveAfiliado()
	{
		$this->Afiliado_Model->saveAfiliado($this->input->post());
	}

	function updateData($affiliate)
	{
		$this->Afiliado_Model->updateDataAffiliate($affiliate,$this->input->post());
	}

	function updateCatData($affiliate)
	{
		$datos = [
		    'status_affiliate' => $this->input->post('status_affiliate'),
		    'comments_affiliate' => $this->input->post('comments_affiliate')
		];
		$contactDatos = [
		    'date_contact_affiliate' => $this->input->post('date_contact_affiliate'),
		    'affiliate_id' => $affiliate,
		    'contact_id' => $this->input->post('contact_id')
		];

		$this->Afiliado_Model->updateDataAffiliate($affiliate,$datos);
		$this->Afiliado_Model->insertContactAffiliate($contactDatos);
	}

	function verAfiliado($id)
	{
		$data['afiliado'] = $this->Afiliado_Model->afiliado($id);
		$data['tipo_contacto'] = $this->Afiliado_Model->tContacto();
		$data['status'] = $this->Afiliado_Model->status();
		$this->load->view('pages/Afiliados/viewAfiliado', $data);
	}

}

/* End of file Afiliado.php */
/* Location: ./application/controllers/Afiliado.php */