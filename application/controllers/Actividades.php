<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Actividades extends SuperController {


	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Principal_Model');
		$this->load->model('Actividad_Model');
	}

	public function newActividad()
	{
		$data['title_page'] = "Nueva Actividad";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Actividades/Nueva/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Actividades/Nueva/newActividad');
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('pages/Actividades/Nueva/scripts');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	function saveActividad()
	{
		$datos = [
		    'name_activity' => $this->input->post('name_activity'),
			'description_activity' => $this->input->post('description_activity'),
			'date_activity' => $this->input->post('date_activity'),
			'hour_activity' => $this->input->post('hour_activity'),
			'address_activity' => $this->input->post('address_activity'),
			'age_activity ' => json_encode($this->input->post('age_activity')),
			'genre_activity' => $this->input->post('genre_activity'),
			'category_activity' => json_encode($this->input->post('category_activity')),
			'commune_activity' => json_encode($this->input->post('commune_activity')),
			'type_activity' => $this->input->post('type_activity'),
			'image_activity' => $this->input->post('image_activity')
		];
		$this->Actividad_Model->saveActividad($datos);
	}

	function saveImageActivity()
	{	
		foreach ($_FILES as $key) 
	 	{
	 		$name = $key['name'];
			$temp = $key['tmp_name']; 
			$local = "././files/actividad/imagenes/".$name;	
			
			move_uploaded_file($temp, $local);
	 	}
	}

	public function verActividades()
	{
		$data['title_page'] = "Todas las Actividades";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$list['allActivities'] = $this->Actividad_Model->listActivities();
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Actividades/Todas/viewTodas',$list);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	public function verActividadesCalendario()
	{
		$result=$this->Actividad_Model->getEvents();
		echo json_encode($result);
	}

	function verCalendarioActividades()
	{
		$data['title_page'] = "Calendario de Actividades";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Actividades/Calendario/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Actividades/Calendario/viewCalendario');
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('pages/Actividades/Calendario/scripts');
		$this->load->view('templates/endHtml');
	}

}

/* End of file Actividades.php */
/* Location: ./application/controllers/Actividades.php */