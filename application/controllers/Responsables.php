<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Responsables extends SuperController {


	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Responsables_Model');
		$this->load->model('Principal_Model');
	}

	function asigna()
	{
		$this->Responsables_Model->asignaAfiliado($this->input->post());
	}

	public function asignaAfiliados($vendor)
	{
		$data['afiliados'] = $this->Responsables_Model->afiliados_noAsignados();
		$data['vendedor'] = $vendor;
		$this->load->view('pages/Responsables/asAfiliados', $data);
	}

	function disable()
	{
		$vendor = $this->input->post('id_vendor');
		$this->Responsables_Model->disableResponsable($vendor);
	}


	public function index()
	{
		$data['title_page'] = "Responsables";
		$list['vendedores'] = $this->Responsables_Model->vendedores();
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Responsables/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Responsables/listResponsables',$list);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('pages/Responsables/scripts');
		$this->load->view('templates/endHtml');
	}

	function remove()
	{
		$this->Responsables_Model->removeAfiliado($this->input->post());
	}

	function tAfiliados($id)
	{
		$data['asignados'] = $this->Responsables_Model->asignados($id);
		$this->load->view('pages/Responsables/viewAsignados', $data);
	}

	function updateVendor($id)
	{
		$this->Responsables_Model->updateVendor($id,$this->input->post());
	}

	function verResponsable($id)
	{
		$data['vendedor'] = $this->Responsables_Model->vendedor($id);
		$data['asignados'] = $this->Responsables_Model->asignados($id);
		$this->load->view('pages/Responsables/viewResponsable', $data);
	}

}

/* End of file Responsables.php */
/* Location: ./application/controllers/Responsables.php */