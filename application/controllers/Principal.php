<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends SuperController {

	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
	}


	public function index()
	{
		$data['title_page'] = "Principal";
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/dashboard');
		$this->load->view('templates/raside');
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

	public function reportarDudas()
	{
		$this->load->view('pages/reporteDudas');
	}

}

/* End of file Principal.php */
/* Location: ./application/controllers/Principal.php */