<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends SuperController {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Login_Model');
	}


	public function index()
	{
		$this->session->sess_destroy();
		$data['title_page'] = "Iniciar Sesion";
		$data['mensaje'] = "";
		$data['user'] = "";
		$this->load->view('login',$data);
	}

	public function inSession()
	{
		$user = $this->input->post('user');
		$pass = $this->input->post('pass');

		$realuser = $this->Login_Model->verificaruser($user);

		if ($realuser == "valid") 
		{

			$realpass = $this->encrypt->decode($this->Login_Model->realpass($user));
			if ($pass != $realpass) 
			{
				$data['title_page'] = "Iniciar Sesion";
				$data['mensaje'] = "Contraseña Invalida";
				$data['user'] = $user;
				$this->load->view('login',$data);
			} else {
				$privilegio = $this->Login_Model->privilegio($user);


				if ($privilegio == 1) 
				{
					$dataMaster = $this->Login_Model->masterData($user);
					$array = array(
						'username' => $user,
						'code_user' => $dataMaster['code_user'],
						'nombre' => $dataMaster['name_master'],
						'cargo' => $dataMaster['cargo_master'],
						'registro' => $dataMaster['date_register_master'],
						'type_user' => $privilegio
					);
					
					$this->session->set_userdata( $array );
					redirect(base_url('confirma_session'));
				}
				if ($privilegio == 2) 
				{
					$dataAdmin = $this->Login_Model->adminData($user);
					$array = array(
						'username' => $user,
						'code_user' => $dataAdmin['code_user'],
						'nombre' => $dataAdmin['name_admin'],
						'phone' => $dataAdmin['phone_admin'],
						'email' => $dataAdmin['email_admin'],
						'registro' => $dataAdmin['date_register_admin'],
						'type_user' => $privilegio
					);
					
					$this->session->set_userdata( $array );
					redirect('confirma_session');
				}
				if ($privilegio == 3) 
				{
					
				}
			}

		} else {
			if ($realuser == "disabled") 
			{
				$data['title_page'] = "Iniciar Sesion";
				$data['mensaje'] = "Usuario Inactivo comunicate con el dpto de soporte";
				$data['user'] = "";
				$this->load->view('login',$data);
			}
			if ($realuser == "invalid") 
			{
				$data['title_page'] = "Iniciar Sesion";
				$data['mensaje'] = "Usuario Invalido";
				$data['user'] = "";
				$this->load->view('login',$data);
			}
			
		}
	}

	public function outSession()
	{
		$this->removeCache();
		$this->Login_Model->outSession();
		$this->session->sess_destroy();
		redirect('confirma_session');
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */