<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ayuda extends SuperController {

	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Principal_Model');
		$this->load->model('Ayuda_Model');
	}

	public function index()
	{
		$data['title_page'] = "Ayuda";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$datos['normas'] = $this->Ayuda_Model->normas();
		$datos['faq'] = $this->Ayuda_Model->faq();
		$datos['sms'] = $this->Ayuda_Model->sms();
		$datos['resp'] = $this->Ayuda_Model->resp();
		$this->load->view('templates/styles', $data);
		$this->load->view('pages/Ayuda/styles');
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/Ayuda/dashboard',$datos);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('pages/Ayuda/scripts');
		$this->load->view('templates/endHtml');
	}

	function saveAudio()
	{
		$datos = [
		    'title_audio' => $this->input->post('title_audio'),
		    'folder_audio' => $this->input->post('file_audio')
		];
		$this->Ayuda_Model->saveAudio($datos);
	}

	function saveFileAudio()
	{
		foreach ($_FILES as $key) 
	 	{
	 		$name = $key['name'];
			$temp = $key['tmp_name']; 
			$local = "././files/ayuda/audios/".$name;	
			
			move_uploaded_file($temp, $local);
	 	}
	}

	function saveFaq()
	{
		$datos = array('title_faq' => $this->input->post('title_faq'), 'answer_faq' => $this->input->post('answer_faq'));
		$this->Ayuda_Model->saveFaq($datos);
	}

	function saveNorma()
	{
		$datos = array('title_rule' => $this->input->post('title_rule'), 'text_rule' => $this->input->post('text_rule'));
		$this->Ayuda_Model->saveNorma($datos);
	}

	function savePosResp()
	{
		$datos = array('title_posible_answer' => $this->input->post('title_posible_answer'), 'text_posible_answer' => $this->input->post('text_posible_answer'));
		$this->Ayuda_Model->savePosResp($datos);
	}

	function saveMensaje()
	{
		$datos = array('title_model_message' => $this->input->post('title_model_message'), 'content_model_message' => $this->input->post('content_model_message'));
		$this->Ayuda_Model->saveMensaje($datos);
	}

	public function viewAudio()
	{
		$this->load->view('pages/Ayuda/Tipos/audio');
	}

	public function viewVideo()
	{
		$this->load->view('pages/Ayuda/Tipos/video');
	}

	public function viewSms()
	{
		$this->load->view('pages/Ayuda/Tipos/mensajes_text');
	}

	public function viewNormas()
	{
		$this->load->view('pages/Ayuda/Tipos/normas');
	}

	public function viewResp()
	{
		$this->load->view('pages/Ayuda/Tipos/posibles_resp');
	}

	public function viewFaq()
	{
		$this->load->view('pages/Ayuda/Tipos/faq');
	}

}

/* End of file Ayuda.php */
/* Location: ./application/controllers/Ayuda.php */