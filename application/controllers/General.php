<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends SuperController {

	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('General_Model');
		$this->load->model('Principal_Model');
	}

	public function viewResp()
	{
		$id = $this->input->post('id_resp');
		print_r(json_encode($this->General_Model->readResp($id)));
	}

	public function refuseResp()
	{
		$this->General_Model->refuseResp($this->input->post('id_resp'));
	}

	public function acceptResp()
	{
		$this->General_Model->acceptResp($this->input->post('id_resp'));
	}

	public function solicitudesPendientes()
	{
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$this->load->view('generalViews/dashboard/solicitudesPendientes', $raside);
	}

	public function savePass()
	{
		$user = $this->input->post('user');
		$datos = array('pass_user' => $this->encrypt->encode($this->input->post('password')));
		$this->General_Model->savePass($user,$datos);
	}


}

/* End of file General.php */
/* Location: ./application/controllers/General.php */