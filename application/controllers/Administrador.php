<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends SuperController {

	public function __construct()
	{
		parent::__construct();
		$this->removeCache();
		if (!$this->session->userdata('username'))
		{
			$this->session->sess_destroy();
			redirect(base_url());
		}
		$this->load->model('Principal_Model');
	}

	public function index()
	{
		$data['title_page'] = "Principal";
		$raside['solicitudes'] = $this->Principal_Model->solPend();
		$dashboard['afiliados'] = $this->Principal_Model->afiliados();
		$dashboard['totales'] = $this->Principal_Model->totales();
		$this->load->view('templates/styles', $data);
		$this->load->view('templates/header');
		$this->load->view('templates/aside');
		$this->load->view('pages/dashboard',$dashboard);
		$this->load->view('templates/raside',$raside);
		$this->load->view('templates/footer');
		$this->load->view('templates/scripts');
		$this->load->view('templates/endHtml');
	}

}

/* End of file Administrador.php */
/* Location: ./application/controllers/Administrador.php */